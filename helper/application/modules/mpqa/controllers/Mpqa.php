<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /**
  * @modified by ahmadluky
  * php 
  */

class Mpqa extends CI_Controller {

	public $delimiter=";";
	public $postag=false;

    public function __construct()
    {
		parent::__construct();
		$this->load->model('M_mpqa', 'mpqa');
	}

	public function index(){}

	public function cli(){
		echo "Save Data to MYSQL\n";
		$handle = fopen(RESOURCE_DIR."MPQA_id.txt","r");
		if ($handle) 
		{
		    while ( ($line = fgets($handle) ) !== false) 
		    {
		        echo $line;
		        $str=explode($this->delimiter, $line);
		        $word=trim($str[0]);
		        $score=trim($str[2]);
		        $tag=trim($str[1]);
		        if ( count($str)>1 )
		        	$this->mpqa->saveFile($word, $score, $tag);	
		        else
		        	trigger_error("Value is not Valid", E_USER_ERROR);
		    }
		    fclose($handle);
		} else {
		    echo "error opening the file";
		}
	}    
}
?>