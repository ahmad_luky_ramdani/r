<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mpqa extends CI_Model{	
	
	function saveFile($word, $score, $pos)
	{
		$data=array('word' =>$word , 'score'=>(int)$score, 'pos'=>$pos);
		return $this->db->insert('lexicon__mpqa_subjclueslen', $data);
	}

}