<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_afinn extends CI_Model{	
	
	function saveFile($word, $score)
	{
		$data=array('word' =>$word , 'score'=>(int)$score);
		return $this->db->insert('lexicon__afinn_eng', $data);
	}

}