<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /**
  * @modified by ahmadluky
  * php 
  */

class Opini extends CI_Controller {

	public $delimiter=";";
	public $postag=false;

    public function __construct()
    {
		parent::__construct();
		$this->load->model('M_opini', 'opini');
	}

	public function index(){}

	public function cli(){
		echo "Save Data to MYSQL\n";
		$path = "C:\\Users\\DIDSI-IPB\\workspace\\r\\resources\\datasets\\2016\\opinion\\data-new.log";
		if ( !file_exists($path)) {
			trigger_error("file ".$path, E_USER_ERROR);
		}
		$handle = fopen($path,"r");
		if ($handle) 
		{
		    while ( ($line = fgets($handle) ) !== false) 
		    {
		        echo $line;
		        $str=explode($this->delimiter, $line);
		        var_dump($str);
		        if ( count($str)>1 )
		        	$this->opini->saveFile($str[0], $str[1]);	
		        else
		        	trigger_error("Value is not Valid", E_USER_ERROR);
		    }
		    fclose($handle);
		} else {
		    echo "error opening the file";
		}
	}    
}
?>