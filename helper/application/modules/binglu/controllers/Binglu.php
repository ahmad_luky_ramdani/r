<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /**
  * @modified by ahmadluky
  * php 
  */

class Binglu extends CI_Controller {

	public $delimiter=";";
	public $postag=false;

    public function __construct()
    {
		parent::__construct();
		$this->load->model('M_binglu', 'binglu');
	}

	public function index(){}

	public function cli(){
		echo "Save Data to MYSQL\n";

		if ( !file_exists(RESOURCE_DIR."Bing_Liu.txt")) {
			trigger_error("file ".RESOURCE_DIR."Bing_Liu.txt not exsist", E_USER_ERROR);
		}

		$handle = fopen(RESOURCE_DIR."Bing_Liu.txt","r");
		if ($handle) 
		{
		    while ( ($line = fgets($handle) ) !== false) 
		    {
		        echo $line;
		        $str=explode($this->delimiter, $line);
		        $word=trim($str[0]);
		        $score=trim($str[1]);

		        if ( count($str)>1 )
		        	$this->binglu->saveFile($word, $score);	
		        else
		        	trigger_error("Value is not Valid", E_USER_ERROR);
		    }
		    fclose($handle);
		} else {
		    echo "error opening the file";
		}
	}    
}
?>