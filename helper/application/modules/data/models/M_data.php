<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model{	
	
	function saveFile(	$tweet_id, $nickname, $tweet_content, 
						$kelas, $favs, $rTs, $followers, $following, 
						$listed, $tweet_Url)
	{
		$data=array('tweet_id' =>$tweet_id , 'nickname'=>$nickname, 'tweet_content'=>$tweet_content, 
					'kelas'=>$kelas, 'favs'=>(int)$favs, 'rTs'=>(int)$rTs, 'followers'=>(int)$followers,
					'following'=>(int)$following, 'listed'=>(int)$listed, 'tweet_Url'=>$tweet_Url);
		return $this->db->insert('data', $data);
	}

}