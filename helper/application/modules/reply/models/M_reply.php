<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_reply extends CI_Model{	
	
	function saveFile($id_tweet, $id, $nickname, $tweet_content)
	{
		$data=array('id_tweet' =>$id_tweet , 'id_tweet_reply'=>$id, 'nickname'=>$nickname, 'tweet_content'=>$tweet_content);
		return $this->db->insert('data_reply', $data);
	}

}