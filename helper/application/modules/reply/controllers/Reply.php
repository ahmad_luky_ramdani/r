<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /**
  * @modified by ahmadluky
  * php 
  */

class Reply extends CI_Controller {

	public $delimiter="||";
	public $postag=false;

    public function __construct()
    {
		parent::__construct();
		$this->load->model('M_reply', 'reply');
	}

	public function index(){}


	public function cli(){
		echo "Save Data to MYSQL\n";
		$path = "C:\\Users\\DIDSI-IPB\\workspace\\r\\resources\\datasets\\2016\\logTwitter\\data_latih_id_tweet_reply_ALL-8.log";
		if ( !file_exists($path)) {
			trigger_error("file ".$path, E_USER_ERROR);
		}
		$handle = fopen($path,"r");
		if ($handle) 
		{
		    while ( ($line = fgets($handle) ) !== false) 
		    {
		        echo $line;
		        $str=explode($this->delimiter, $line);
		        $id_tweet = $str[0];
		        $reply=$str[1];
		        
		        $reply_arr=explode("//",$reply);
		        $id=$reply_arr[0];
		        $nickname=$reply_arr[1];
		        $tweet_content=$reply_arr[2];
		        
		        $this->reply->saveFile($id_tweet, $id, $nickname, $tweet_content);	
		        
		    }
		    fclose($handle);
		} else {
		    echo "error opening the file";
		}
	}       
}
?>