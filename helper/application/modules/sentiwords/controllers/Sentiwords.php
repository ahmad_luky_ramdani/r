<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /**
  * @modified by ahmadluky
  * php 
  */

class Sentiwords extends CI_Controller {

	public $delimiter=";";
	public $postag=false;

    public function __construct()
    {
		parent::__construct();
		$this->load->model('M_sentiwords', 'sentiwords');
	}

	public function index(){}

	public function cli(){
		echo "Save Data to MYSQL\n";
		$path = "E:\\lexicon\\fix\\SentiWords_compire.csv";
// 		$path = "E:\\lexicon\\fix\\SentiWords_1.0_.txt";
// 		if ( !file_exists(RESOURCE_DIR."SentiWords_1.0_ID_3.txt")) {
// 			trigger_error("file ".RESOURCE_DIR."SentiWords_1.0_ID_3.txt not exsist", E_USER_ERROR);
// 		}
		
		if ( !file_exists($path)) {
			trigger_error("file ".$path, E_USER_ERROR);
		}
		
		
		$handle = fopen($path,"r");
		if ($handle) 
		{
		    while ( ($line = fgets($handle) ) !== false) 
		    {
		        echo $line;
		        $str=explode($this->delimiter, $line);
		        if ( count($str)>1 )
		        	$this->sentiwords->saveFile($str[0], $str[1], $str[2], $str[3]);	
		        else
		        	trigger_error("Value is not Valid", E_USER_ERROR);
		    }
		    fclose($handle);
		} else {
		    echo "error opening the file";
		}
	}    
}
?>