<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sentiwords extends CI_Model{	
	
	function saveFile($word1, $word2, $score, $t)
	{
		$data=array('word1' =>$word1 ,'word2' =>$word2, 'score'=>(float)$score, 'terjemahkan'=>$t);
		return $this->db->insert('lexicon__sentiwords_compire', $data);
	}

}