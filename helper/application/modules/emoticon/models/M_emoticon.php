<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_emoticon extends CI_Model{	
	
	function saveFile($word, $score)
	{
		$data=array('word' =>$word , 'score'=>(int)$score);
		return $this->db->insert('lexicon__emoticon', $data);
	}

}