<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /**
  * @modified by ahmadluky
  * php 
  */

class Pengolahandata extends CI_Controller {

    public function __construct()
    {
		parent::__construct();
		$this->load->model('M_pengolahandata', 'm_pengolahandata');
	}

	public function index(){}

	public function cli(){
		$rst = $this->m_pengolahandata->getSelect();
		foreach ($rst -> result() as $row) {
			if (substr($row->text, 0, 2) == "RT") {
				$retweet = explode(":", $row->text);
				$retweet_from = str_replace(array("RT ","\""), "", $retweet[0]);
				$this->m_pengolahandata->getUpdateRetweet("TRUE", $retweet_from, $row->id_);
				// echo $row->id_ . "/". $row->text."\n"; 
			}
		}
	}    

	public function cli2(){
		$rst = $this->m_pengolahandata->getSelectTweet();
		foreach ($rst -> result() as $row) {
			if ( (substr($row->text, 0, 2) != "RT")  and $row->is_reply=="FALSE") {
				$this->m_pengolahandata->getUpdateTweet("TRUE", $row->id_);
			} else {
				$this->m_pengolahandata->getUpdateTweet("FALSE", $row->id_);
			}
		}
	}    
}
?>