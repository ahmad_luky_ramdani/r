package com.naufal.senti.commons.featurevector;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.naufal.senti.commons.Configuration;
/**
 * @author ahmadluky
 */
public class POSFeatureVectorGeneratorBahasa extends FeatureVectorGeneratorBahasa {
	private static final Logger LOG = LoggerFactory.getLogger(POSFeatureVectorGeneratorBahasa.class);
	private static final boolean LOGGING = Configuration.get("commons.featurevectorgenerator.pos.logging", false);
	private int m_vectorStartId;
	private final boolean m_normalize;
	private final int m_vectorSize;
	
	public POSFeatureVectorGeneratorBahasa(boolean normalize) {
	      m_normalize = normalize;
	      m_vectorStartId = 1;
	      m_vectorSize = 8;
	      LOG.info("VectorSize: " + m_vectorSize);
	}
    public POSFeatureVectorGeneratorBahasa(boolean normalize, int vectorStartId) {
    	this(normalize);
    	this.m_vectorStartId = vectorStartId;
    }
    @Override
    public int getFeatureVectorSize() {
    	return m_vectorSize;
    }
    @Override
    public Map<Integer, Double> generateFeatureVector(List<String> taggedTokens) {
        Map<Integer, Double> resultFeatureVector = new TreeMap<>();
        double[] posTags = countPOSTagsFromTaggedTokensBahasa(taggedTokens, m_normalize);
        if (posTags != null) {
	            if (posTags[0] != 0) // nouns
	                resultFeatureVector.put(m_vectorStartId, posTags[0]);
	            if (posTags[1] != 0) // verb
	                resultFeatureVector.put(m_vectorStartId + 1, posTags[1]);
	            if (posTags[2] != 0) // adjective
	                resultFeatureVector.put(m_vectorStartId + 2, posTags[2]);
	            if (posTags[3] != 0) // adverb
	                resultFeatureVector.put(m_vectorStartId + 3, posTags[3]);
	            if (posTags[4] != 0) // nagation
	                resultFeatureVector.put(m_vectorStartId + 4, posTags[6]);
	            if (posTags[5] != 0) // emoticon
	                resultFeatureVector.put(m_vectorStartId + 5, posTags[7]);
        }
        if (LOGGING) {
            	LOG.info("POStags: " + Arrays.toString(posTags));
        }
        return resultFeatureVector;
    }
    /**
    private double[] countPOSTagsFromTaggedTokens(List<TaggedToken> taggedTokens,boolean normalize) {
        // 8 = [NOUN, VERB, ADJECTIVE, ADVERB, INTERJECTION, PUNCTUATION, HASHTAG, EMOTICON]
        double[] posTags = new double[] { 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d };
        int wordCount = 0;
        for (TaggedToken word : taggedTokens) {
            wordCount++;
            String arkTag = word.tag;
            
            // http://www.ark.cs.cmu.edu/TweetNLP/annot_guidelines.pdf
            if (arkTag.equals("N") || arkTag.equals("O") || arkTag.equals("^") || arkTag.equals("Z")) {
                posTags[0]++;
            } else if (arkTag.equals("V") || arkTag.equals("T")) {
                posTags[1]++;
            } else if (arkTag.equals("A")) {
                posTags[2]++;
            } else if (arkTag.equals("R")) {
                posTags[3]++;
            } else if (arkTag.equals("!")) {
                posTags[4]++;
            } else if (arkTag.equals(",")) {
                posTags[5]++;
            } else if (arkTag.equals("#")) {
                posTags[6]++;
            } else if (arkTag.equals("E")) {
                posTags[7]++;
            }
        }
        if (normalize) {
            for (int i = 0; i < posTags.length; i++) {
                posTags[i] /= wordCount;
            }
        }
        return posTags;
    }
    **/
    private double[] countPOSTagsFromTaggedTokensBahasa(List<String> taggedTokens,boolean normalize) {
        // 8 = [NOUN, VERB, ADJECTIVE, ADVERB, NEGATION, EMOTICON]
        double[] posTags = new double[] { 0d, 0d, 0d, 0d, 0d, 0d };
        int wordCount = 0;
        for (String word : taggedTokens) {
            wordCount++;
            String Tag = word;
            if (Tag.equals("n")) {
                posTags[0]++;
            } else if (Tag.equals("v")) {
                posTags[1]++;
            } else if (Tag.equals("r")) {
                posTags[2]++;
            } else if (Tag.equals("a")) {
                posTags[3]++;
            } else if (Tag.equals("neg")) {
                posTags[6]++;
            } else if (Tag.equals("e")) {
                posTags[7]++;
            }
        }
        if (normalize) {
            for (int i = 0; i < posTags.length; i++) {
                posTags[i] /= wordCount;
            }
        }
        return posTags;
    }
}