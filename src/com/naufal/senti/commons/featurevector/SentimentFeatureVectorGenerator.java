package com.naufal.senti.commons.featurevector;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.dict.SentimentDictionary;
import com.naufal.senti.commons.dict.SentimentResult;

import cmu.arktweetnlp.Tagger.TaggedToken;

public class SentimentFeatureVectorGenerator extends FeatureVectorGenerator {
  
    private static final Logger LOG = LoggerFactory.getLogger(SentimentFeatureVectorGenerator.class);
    private static final boolean LOGGING = Configuration.get("commons.featurevectorgenerator.sentiment.logging", false);
    private static final int VECTOR_SIZE = 7;
    private SentimentDictionary m_sentimentDict;
    private int m_vectorStartId = 1;

    public SentimentFeatureVectorGenerator() {
        this.m_sentimentDict = SentimentDictionary.getInstance();
        LOG.info("VectorSize: " + getFeatureVectorSize());
    }

    public SentimentFeatureVectorGenerator(int vectorStartId) {
        this();
        this.m_vectorStartId = vectorStartId;
    }

    public SentimentDictionary getSentimentDictionary() {
        return m_sentimentDict;
    }

    @Override
    public final int getFeatureVectorSize() {
        // VECTOR_SIZE = {PosCount, NeutralCount, NegCount, Sum, Count, MaxPos, MaxNeg}
        return VECTOR_SIZE * m_sentimentDict.getSentimentWordListCount();
    }

//    @Override
//    public Map<Integer, Double> generateFeatureVector(List<TaggedToken> taggedTokens) {
//        Map<Integer, SentimentResult> tweetSentiments = m_sentimentDict.getSentenceSentiment(taggedTokens);
//        return generateFeatureVector(tweetSentiments);
//    }

    @SuppressWarnings("unused")
	private Map<Integer, Double> generateFeatureVector(Map<Integer, SentimentResult> tweetSentiments) {
        Map<Integer, Double> featureVector = new TreeMap<>();
        if (tweetSentiments != null) {
            for (Map.Entry<Integer, SentimentResult> tweetSentiment : tweetSentiments.entrySet()) {
              int key = tweetSentiment.getKey();
              SentimentResult sentimentResult = tweetSentiment.getValue();
              // LOG.info("TweetSentiment: " + sentimentResult);

                if (sentimentResult.getPosCount() != 0) {
                      featureVector.put(m_vectorStartId + (key * VECTOR_SIZE), (double) sentimentResult.getPosCount());
                }
                if (sentimentResult.getNeutralCount() != 0) {
                      featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 1, (double) sentimentResult.getNeutralCount());
                }
                if (sentimentResult.getNegCount() != 0) {
                  featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 2, (double) sentimentResult.getNegCount());
                }
                if (sentimentResult.getSum() != 0) {
                  featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 3, sentimentResult.getSum());
                }
                if (sentimentResult.getCount() != 0) {
                  featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 4,(double) sentimentResult.getCount());
                }
                if (sentimentResult.getMaxPos() != null) {
                  featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 5, sentimentResult.getMaxPos());
                }
                if (sentimentResult.getMaxNeg() != null) {
                  featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 6, sentimentResult.getMaxNeg());
                }

                if (LOGGING) {
                  LOG.info("TweetSentiment: " + sentimentResult);
                }
            }
        }
    return featureVector;
    }

    @Override
    public Map<Integer, Double> generateFeatureVector(List<TaggedToken> tweet) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}