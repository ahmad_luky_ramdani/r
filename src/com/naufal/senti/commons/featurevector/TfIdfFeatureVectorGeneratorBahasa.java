package com.naufal.senti.commons.featurevector;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.dict.SentimentDictionary;
import com.naufal.senti.commons.tfidf.TweetTfIdf;

public final class TfIdfFeatureVectorGeneratorBahasa extends FeatureVectorGeneratorBahasa {
    private static final Logger LOG = LoggerFactory.getLogger(TfIdfFeatureVectorGeneratorBahasa.class);
    private static final boolean LOGGING = Configuration.get("commons.featurevectorgenerator.tfidf.logging", false);
    private TweetTfIdf m_tweetTfIdf = null;
    private SentimentDictionary m_sentimentDict;
    private int m_vectorStartId = 1;

    public TfIdfFeatureVectorGeneratorBahasa(TweetTfIdf tweetTfIdf) {
        this.m_tweetTfIdf = tweetTfIdf;
        this.m_sentimentDict = SentimentDictionary.getInstance();
        LOG.info("VectorSize: " + getFeatureVectorSize());
    }
    public TfIdfFeatureVectorGeneratorBahasa(TweetTfIdf tweetTfIdf, int vectorStartId) {
        this(tweetTfIdf);
        this.m_vectorStartId = vectorStartId;
    }
    public SentimentDictionary getSentimentDictionary() {
        return m_sentimentDict;
    }
    @Override
    public int getFeatureVectorSize() {
        return m_tweetTfIdf.getInverseDocFreq().size();
    }
    @Override
    public Map<Integer, Double> generateFeatureVector(List<String> tweet) {
        return generateFeatureVector(m_tweetTfIdf.tfIdfFromTaggedTokens(tweet));
    }
    public Map<Integer, Double> generateFeatureVector(Map<String, Double> tfIdf) {
        Map<Integer, Double> resultFeatureVector = new TreeMap<>();
        if (m_tweetTfIdf != null) {
            // Map<String, Double> idf = m_tweetTfIdf.getInverseDocFreq();
            Map<String, Integer> termIds = m_tweetTfIdf.getTermIds();
            for (Map.Entry<String, Double> element : tfIdf.entrySet()) {
                String key = element.getKey();
                if (termIds.containsKey(key)) {
                    int vectorId = m_vectorStartId + termIds.get(key);
                    resultFeatureVector.put(vectorId, element.getValue());
                }
            }
        }
        if (LOGGING) {
            LOG.info("TfIdsFeatureVector: " + resultFeatureVector);
        }
        return resultFeatureVector;
    }

}