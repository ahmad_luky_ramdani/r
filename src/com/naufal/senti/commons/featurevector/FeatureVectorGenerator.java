package com.naufal.senti.commons.featurevector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cmu.arktweetnlp.Tagger.TaggedToken;

public abstract class FeatureVectorGenerator {
	
    private static final Logger LOG = LoggerFactory.getLogger(FeatureVectorGenerator.class);
    public abstract int getFeatureVectorSize();
    public abstract Map<Integer, Double> generateFeatureVector(List<TaggedToken> tweet);
    public List<Map<Integer, Double>> generateFeatureVectors(List<List<TaggedToken>> tweets) {
        return generateFeatureVectors(tweets, false);
    }
    public List<Map<Integer, Double>> generateFeatureVectors(List<List<TaggedToken>> taggedTweets, boolean logging) {
        List<Map<Integer, Double>> featuredVectors = new ArrayList<>();
        for (List<TaggedToken> tweet : taggedTweets) {
            Map<Integer, Double> featureVector = generateFeatureVector(tweet);
            if (logging) {
                LOG.info("Tweet: " + tweet);
                LOG.info("FeatureVector: " + featureVector);
            }
            featuredVectors.add(featureVector);
        }
        return featuredVectors;
    }

}