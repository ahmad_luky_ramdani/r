package com.naufal.senti.commons.featurevector;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.dict.SentimentDictionary;
import com.naufal.senti.commons.dict.SentimentResult;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author ahmadluky
 */
public class SentimentFeatureVectorGeneratorBahasa extends FeatureVectorGeneratorBahasa{
    
    private static final Logger LOG = LoggerFactory.getLogger(SentimentFeatureVectorGeneratorBahasa.class);
    private static final boolean LOGGING = Configuration.get("commons.SentimentFeatureVectorGeneratorBahasa.sentiment.logging", false);
    private static final int VECTOR_SIZE = 7;
    private SentimentDictionary m_sentimentDict;
    private int m_vectorStartId = 0;
    public SentimentFeatureVectorGeneratorBahasa() {
        this.m_sentimentDict = SentimentDictionary.getInstance();
        LOG.info("VectorSize: " + getFeatureVectorSize());
    }
    public SentimentFeatureVectorGeneratorBahasa(int vectorStartId) {
        this();
        this.m_vectorStartId = vectorStartId;
    }
    public SentimentDictionary getSentimentDictionary() {
        return m_sentimentDict;
    }
    @Override
    public final int getFeatureVectorSize() {
        // VECTOR_SIZE = [PosCount, NeutralCount, NegCount, Sum, Count, MaxPos, MinPos, MaxNeg, MinNeg]
        return VECTOR_SIZE * m_sentimentDict.getSentimentWordListCount();
    }
    @Override
    public Map<Integer, Double> generateFeatureVector(List<String> taggedTokens) {
        Map<Integer, SentimentResult> tweetSentiments = m_sentimentDict.getSentenceSentiment(taggedTokens);
        return generateFeatureVector(tweetSentiments);
    }
    private Map<Integer, Double> generateFeatureVector(Map<Integer, SentimentResult> tweetSentiments) {
    	// VECTOR_SIZE = [PosCount, NeutralCount, NegCount, Sum, Count, MaxPos, MinPos, MaxNeg, MinNeg]
        Map<Integer, Double> featureVector = new TreeMap<>();
        if (tweetSentiments != null) {
        	for(Map.Entry<Integer, SentimentResult> tweetSentiment : tweetSentiments.entrySet()){
                // key is index m_wordLists - lexicon from load configuration
                // int key = tweetSentiment.getKey();
                int key = 0;
                SentimentResult sentimentResult = tweetSentiment.getValue();
                // 1. Jumlah Kata Positif
                if (sentimentResult.getPosCount() != 0) {
	                //LOG.info(m_vectorStartId + (key * VECTOR_SIZE)+" : "+ (double) sentimentResult.getPosCount());
	                featureVector.put(m_vectorStartId + (key * VECTOR_SIZE), (double) sentimentResult.getPosCount());
                }else{
                    featureVector.put(m_vectorStartId + (key * VECTOR_SIZE), Double.NaN);
                }
                // 2. Jumlah Kata Netral
                if (sentimentResult.getNeutralCount() != 0) {
	                //LOG.info(m_vectorStartId + (key * VECTOR_SIZE) + 1 +" : "+ (double) sentimentResult.getNeutralCount());
	                featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 1, (double) sentimentResult.getNeutralCount());
                }else{
                    featureVector.put(m_vectorStartId + (key * VECTOR_SIZE)+ 1, Double.NaN);
                }
                // 3. Jumlah Kata Negatif
                if (sentimentResult.getNegCount() != 0) {
	                //LOG.info(m_vectorStartId + (key * VECTOR_SIZE) + 2 +" : "+ (double) sentimentResult.getNegCount());
	                featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 2, (double) sentimentResult.getNegCount());
                }else{
                    featureVector.put(m_vectorStartId + (key * VECTOR_SIZE)+ 2, Double.NaN);
                }
                // 4. Banyak kata sentimen
                if (sentimentResult.getCount() != 0) {
	                //LOG.info(m_vectorStartId + (key * VECTOR_SIZE) + 5 +" : "+ (double) sentimentResult.getCount());
	                featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 3,(double) sentimentResult.getCount());
                }else{
                    featureVector.put(m_vectorStartId + (key * VECTOR_SIZE)+ 3, Double.NaN);
                }
                // 5. Jumlah nilai sentimen pada kata
                if (sentimentResult.getSum() != 0) {
	                //LOG.info(m_vectorStartId + (key * VECTOR_SIZE) + 3 +" : "+ (double) sentimentResult.getSum());
	                featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 4, sentimentResult.getSum());
                }else{
                    featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 4, Double.NaN);
                }
                //6. Maximum positif
                if (sentimentResult.getMaxPos() != null) {
	                //LOG.info(m_vectorStartId + (key * VECTOR_SIZE) + 6 +" : "+ (double) sentimentResult.getMaxPos());
	                featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 5, sentimentResult.getMaxPos());
                }else{
                    featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 5, Double.NaN);
                }
                //7. Maximal negatif
                if (sentimentResult.getMaxNeg()!= null) {
	                //LOG.info(m_vectorStartId + (key * VECTOR_SIZE) + 7 +" : "+ (double) sentimentResult.getMinPos());
	                featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 6, sentimentResult.getMaxNeg());
                }else{
                    featureVector.put(m_vectorStartId + (key * VECTOR_SIZE) + 6, Double.NaN);
                }
                if (LOGGING) {
                    LOG.info("Key: " + key);
                    LOG.info("featureVector: " + featureVector);
                }
            };
        }
        return featureVector;
    }
}
