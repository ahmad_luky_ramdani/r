package com.naufal.senti.commons.featurevector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class FeatureVectorGeneratorBahasa {
    private static final Logger LOG = LoggerFactory.getLogger(FeatureVectorGeneratorBahasa.class);
    public abstract int getFeatureVectorSize();
    public abstract Map<Integer, Double> generateFeatureVector(List<String> tweet);
    public List<Map<Integer, Double>> generateFeatureVectors(List<List<String>> tweets) {
        return generateFeatureVectors(tweets, false);
    }
    public List<Map<Integer, Double>> generateFeatureVectors(List<List<String>> taggedTweets, boolean logging) {
        List<Map<Integer, Double>> featuredVectors = new ArrayList<>();
        for (List<String> tweet : taggedTweets) {
            Map<Integer, Double> featureVector = generateFeatureVector(tweet);
            if (logging) {
                LOG.info("Tweet: " + tweet);
                LOG.info("FeatureVector: " + featureVector);
            }
            featuredVectors.add(featureVector);
        }
        return featuredVectors;
    }
}
