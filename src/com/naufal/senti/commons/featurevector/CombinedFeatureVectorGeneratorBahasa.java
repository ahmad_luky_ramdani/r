package com.naufal.senti.commons.featurevector;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.Dataset;
import com.naufal.senti.commons.Tweet;
//import com.naufal.senti.commons.tfidf.TfIdfNormalization;
//import com.naufal.senti.commons.tfidf.TfType;
//import com.naufal.senti.commons.tfidf.TweetTfIdf;
import static com.naufal.senti.commons.util.io.FileUtils.writeCsvFile;
import com.naufal.senti.components.POSTagHMM;
import com.naufal.senti.components.PreprocessorBahasa;
import com.naufal.senti.components.Tokenizer;
import com.naufal.senti.cs.ir.kbbi.IOException_Exception;
import com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author @ahmadluky
 */
public final class CombinedFeatureVectorGeneratorBahasa extends FeatureVectorGeneratorBahasa {
    private static final Logger LOG = LoggerFactory.getLogger(CombinedFeatureVectorGeneratorBahasa.class);
    private SentimentFeatureVectorGeneratorBahasa m_sentimentFeatureVectorGeneratorBahasa = null;
    //private TfIdfFeatureVectorGeneratorBahasa m_tfidfFeatureVectorGenerator = null;
    //private POSFeatureVectorGeneratorBahasa m_POSFeatureVectorGenerator = null;
    public CombinedFeatureVectorGeneratorBahasa() {
        m_sentimentFeatureVectorGeneratorBahasa = new SentimentFeatureVectorGeneratorBahasa(0);
        //m_POSFeatureVectorGenerator = new POSFeatureVectorGeneratorBahasa(normalizePOSCounts, m_sentimentFeatureVectorGeneratorBahasa.getFeatureVectorSize() + 1);
        //m_tfidfFeatureVectorGenerator = new TfIdfFeatureVectorGeneratorBahasa(tweetTfIdf,m_sentimentFeatureVectorGeneratorBahasa.getFeatureVectorSize()+ m_POSFeatureVectorGenerator.getFeatureVectorSize() + 1);
        LOG.info("VectorSize: " + getFeatureVectorSize());
    }
    @Override
    public int getFeatureVectorSize() {
        return m_sentimentFeatureVectorGeneratorBahasa.getFeatureVectorSize();
    }
    @Override
    public Map<Integer, Double> generateFeatureVector(List<String> tweet) {
        Map<Integer, Double> featureVector = m_sentimentFeatureVectorGeneratorBahasa.generateFeatureVector(tweet);
    	//featureVector.putAll(m_POSFeatureVectorGenerator.generateFeatureVector(tweet));
      	//featureVector.putAll(m_tfidfFeatureVectorGenerator.generateFeatureVector(tweet));
        return featureVector;
    }
    public static void main(String[] args) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException, SQLException {
        // Use POS tags in terms
        //boolean usePOSTags = false; 
        PreprocessorBahasa preprocessor = PreprocessorBahasa.getInstance();
        POSTagHMM pOSTagHMM = POSTagHMM.getInstance();
        Dataset dataset = Configuration.getDataSet();
        // Load data training tweets
        List<Tweet> tweet = dataset.getTrainTweets();
        LOG.info("Read train tweets from " + dataset.getTrainDataFile());
        Dataset.printTweetStats(tweet);
        // Tokenize
        LOG.info("Tokenize ....");
        List<List<String>> tokenizedTweets = Tokenizer.tokenizeTweets(tweet);
        // Preprocess
        LOG.info("Praprosesing ....");
        List<List<String>> preprocessedTweets = preprocessor.preprocessTweets(tokenizedTweets);
        // POS Tagging
        LOG.info("Postagging ....");
        List<List<String>> taggedTweets = pOSTagHMM.tagTweets(preprocessedTweets);
        // Generate CombinedFeatureVectorGeneratorBahasa
        LOG.info("Feature Vector ....");
        //TweetTfIdf tweetTfIdf = TweetTfIdf.createFromTaggedTokens(taggedTweets, TfType.LOG, TfIdfNormalization.COS, usePOSTags);
        CombinedFeatureVectorGeneratorBahasa cfvg = new CombinedFeatureVectorGeneratorBahasa();
        // Combined Feature Vector Generation
        for (int i = 0; i < taggedTweets.size(); i++) {
            List<String> taggedTokens = taggedTweets.get(i);
            Map<Integer, Double> combinedFeatureVector = cfvg.generateFeatureVector(taggedTokens);
            // Generate feature vector string
            String featureVectorStr = "";
            List<Double> valueOnly = new ArrayList<Double>();
            for (Map.Entry<Integer, Double> feature : combinedFeatureVector.entrySet()) {
                featureVectorStr += " " + feature.getKey() + ":" + feature.getValue();    
                valueOnly.add(feature.getValue());
            }
            // Save vektor Ciri by score WORD
            valueOnly.add(tweet.get(i).getScore());
            writeCsvFile(valueOnly, "./resources/datasets/2016/csv_format/Unigram-SE-SW.csv");
            // Log Resume
            LOG.info("Tweet: '" + taggedTokens + "'");
            LOG.info("FeatureVector: " + featureVectorStr);
            LOG.info("Class: "+tweet.get(i).getScore());
        }
        System.exit(0);
    }
}
