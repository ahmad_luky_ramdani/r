package com.naufal.senti.commons.util;

import backtype.storm.utils.Time;

public class TimeUtils {

    public static void sleepMillis(long millis) {
        try {
            Time.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void sleepNanos(long nanos) {
        long start = System.nanoTime();
        long end = 0;
        do {
            end = System.nanoTime();
        } while (start + nanos >= end);
        System.out.println("waited time: " + (end - start) + " ns");
    }

}
