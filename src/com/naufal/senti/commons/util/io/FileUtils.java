package com.naufal.senti.commons.util.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Dataset;
import com.naufal.senti.commons.Tweet;
import com.naufal.senti.commons.dict.WordListMap;
import java.util.Arrays;
/**
 * @author ahmadluky
 */
public class FileUtils {
    private static final Logger LOG = LoggerFactory.getLogger(FileUtils.class);

    public static List<Tweet> readTweets(String file, Dataset property) {
        return readTweets(IOUtils.getInputStream(file), property);
    }

    public static List<Tweet> readTweets(InputStream is, Dataset dataset) 
    {
        List<String> negativeLabels = dataset.getNegativeLabels();
        List<String> neutralLabels = dataset.getNeutralLabels();
        List<String> positiveLabels = dataset.getPositiveLabels();
        List<Tweet> tweets = new ArrayList<>();
        InputStreamReader isr = null;
        BufferedReader br = null;
        if (is == null) {
          LOG.error("InputStream is null! File could not be found!");
          return null;
        }
        try {
            isr = new InputStreamReader(is, "UTF-8");
            br = new BufferedReader(isr);
            String line = "";

            while ((line = br.readLine()) != null) {
                String[] values = line.split(dataset.getDelimiter());
                long id = 0;
                try {
                    id = Long.parseLong(values[dataset.getIdIndex()]);
                } catch (NumberFormatException e) {
                    id = 0;
                }
                String text = values[dataset.getTextIndex()];
                String label = values[dataset.getLabelIndex()];
                double score = -1;
                if (negativeLabels.contains(label)) {
                    score = dataset.getNegativeValue();
                } else if (neutralLabels.contains(label)) {
                    score = dataset.getNeutralValue();
                } else if (positiveLabels.contains(label)) {
                    score = dataset.getPositiveValue();
                } else {
                    LOG.info("Label: '" + label + "' does not match!");
                }
                tweets.add(new Tweet(id, text, score));
            }

        } catch (IOException e) {
            LOG.error("IOException: " + e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignore) {}
            }
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException ignore) {}
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignore) {}
            }
        }
        LOG.info("Loaded total " + tweets.size() + " tweets");
        return tweets;
    }

    public static Map<String, String> readFile(String file, String splitRegex) {
        return readFile(IOUtils.getInputStream(file), splitRegex, false);
    }

    public static Map<String, String> readFile(InputStream is, String splitRegex, boolean logging) {
        Map<String, String> table = new HashMap<>();
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            isr = new InputStreamReader(is, "UTF-8");
            br = new BufferedReader(isr);
            String line = "";
            while ((line = br.readLine()) != null) {
                if (line.trim().length() == 0) {
                    continue;
                }
                String[] values = line.split(splitRegex, 2);
                table.put(values[0].trim(), values[1].trim());
                if (logging) {
                    LOG.info("Add entry key: '" + values[0].trim() + "' value: '"
                      + values[1].trim() + "'");
                }
            }
        } catch (IOException e) {
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignore) {}
            }
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException ignore) {}
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignore) {}
            }
        }
        LOG.info("Loaded total " + table.size() + " entries");
        return table;
    }
    
    public static Map<String, Double> readFile(String file, String splitRegex,boolean containsPOSTags, boolean featureScaling, double minValue, double maxValue) {
        return readFile(IOUtils.getInputStream(file), splitRegex, containsPOSTags,featureScaling, minValue, maxValue, false);
    }

    public static Map<String, Double> readFile(String file, String splitRegex,boolean containsPOSTags, boolean featureScaling, double minValue, double maxValue, boolean logging) {
        return readFile(IOUtils.getInputStream(file), splitRegex, containsPOSTags,featureScaling, minValue, maxValue, logging);
    }

    public static Map<String, Double> readFile(	InputStream is, 
                                                String splitRegex,
                                                boolean containsPOSTags, 
                                                boolean featureScaling, 
                                                double minValue,
                                                double maxValue, 
                                                boolean logging) {
    Map<String, Double> map = new HashMap<>();
    InputStreamReader isr = null;
    BufferedReader br = null;
    double actualMaxValue = Double.MIN_VALUE;
    double actualMinValue = Double.MAX_VALUE;
    try {
        isr = new InputStreamReader(is, "UTF-8");
        br = new BufferedReader(isr);
        String line = "";
        while ((line = br.readLine()) != null) {
            if (line.trim().length() == 0) {
                continue;
            }
            String[] values = line.split(splitRegex);
            String key = values[0].trim();
            if (containsPOSTags) {
                String[] posToken = key.split("#");
                key = posToken[0];
            }
            LOG.info(Arrays.toString(values));
            double value = Double.parseDouble(values[1].trim());
            if (featureScaling) {
                // Feature scaling
                double normalizedValue = (value - minValue) / (maxValue - minValue);
                if (logging) {
                    LOG.info("Add entry key: '" + key + "' value: '" + value+ "' normalizedValue: '" + normalizedValue + "'");
                }
                // check min and max values
                if (value > actualMaxValue) {
                    actualMaxValue = value;
                }
                if (value < actualMinValue) {
                    actualMinValue = value;
                }
                value = normalizedValue;
            }

            map.put(key, value);
            if ((logging) && (!featureScaling)) {
                LOG.info("Add entry key: '" + key + "' value: '" + value + "'");
            }
        }
    } catch (IOException e) {
        LOG.error(e.getMessage());
    } finally {
        if (br != null) {
            try {
              br.close();
            } catch (IOException ignore) {}
        }
        if (isr != null) {
            try {
              isr.close();
            } catch (IOException ignore) {}
        }
        if (is != null) {
            try {
              is.close();
            } catch (IOException ignore) {}
        }
    }
    LOG.info("Loaded total " + map.size() + " items [minValue: " + actualMinValue + ", maxValue: " + actualMaxValue + "]");

    if (featureScaling) {
        if (minValue != actualMinValue) {
            LOG.error("minValue is incorrect! actual minValue: " + actualMinValue + " given minValue: " + minValue + " (normalized values might be wrong!)");
        }
        if (maxValue != actualMaxValue) {
            LOG.error("maxValue is incorrect! actual maxValue: " + actualMaxValue  + " given maxValue: " + maxValue  + " (normalized values might be wrong!)");
        }
    }
    return map;
  }

  public static WordListMap<Double> readWordListMap(String file, String splitRegex, boolean containsPOSTags, boolean featureScaling, double minValue, double maxValue) {
    return readWordListMap(IOUtils.getInputStream(file), splitRegex,containsPOSTags, featureScaling, minValue, maxValue, false);
  }

  public static WordListMap<Double> readWordListMap(String file, String splitRegex, boolean containsPOSTags, boolean featureScaling, double minValue, double maxValue, boolean logging) {
    return readWordListMap(IOUtils.getInputStream(file), splitRegex, containsPOSTags, featureScaling, minValue, maxValue, logging);
  }

  public static WordListMap<Double> readWordListMap(InputStream is, String splitRegex, boolean containsPOSTags, boolean featureScaling,double minValue, double maxValue, boolean logging) {
    WordListMap<Double> wordListMap = new WordListMap<Double>();
    InputStreamReader isr = null;
    BufferedReader br = null;
    double actualMaxValue = Double.MIN_VALUE;
    double actualMinValue = Double.MAX_VALUE;
    try {
        isr = new InputStreamReader(is, "UTF-8");
        br = new BufferedReader(isr);
        String line = "";
        while ((line = br.readLine()) != null) {
            if (line.trim().length() == 0) {
                continue;
            }
            String[] values = line.split(splitRegex);
            String key = values[0].trim();
            double value = Double.parseDouble(values[1].trim());

            if (featureScaling) {
                // Feature scaling
                double normalizedValue = (value - minValue) / (maxValue - minValue);

                if (logging) {
                    LOG.info("Add Key: '" + key + "' Value: '" + value
                      + "' normalizedValue: '" + normalizedValue + "'");
                }

                // check min and max values
                if (value > actualMaxValue) {
                    actualMaxValue = value;
                }
                if (value < actualMinValue) {
                    actualMinValue = value;
                }
                value = normalizedValue;
            }
            wordListMap.put(key, value);
        }
        LOG.info("Loaded " + wordListMap.size() + " items [minValue: "
            + actualMinValue + ", maxValue: " + actualMaxValue + "]");

        if (featureScaling) {
          if (minValue != actualMinValue) {
            LOG.error("minValue is incorrect! actual minValue: " + actualMinValue
                + " given minValue: " + minValue
                + " (normalized values might be wrong!)");
          }
          if (maxValue != actualMaxValue) {
            LOG.error("maxValue is incorrect! actual maxValue: " + actualMaxValue
                + " given maxValue: " + maxValue
                + " (normalized values might be wrong!)");
          }
        }

        return wordListMap;

    } catch (IOException e) {
      LOG.error(e.getMessage());
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException ignore) {
        }
      }
      if (isr != null) {
        try {
          isr.close();
        } catch (IOException ignore) {
        }
      }
      if (is != null) {
        try {
          is.close();
        } catch (IOException ignore) {
        }
      }
    }
    return null;
  }

  public static Set<String> readFile(String file) {
    return readFile(IOUtils.getInputStream(file), false, false);
  }

  public static Set<String> readFile(String file, boolean toLowerCase) {
    return readFile(IOUtils.getInputStream(file), toLowerCase, false);
  }

  public static Set<String> readFile(InputStream is, boolean toLowerCase, boolean logging) {
    Set<String> set = new HashSet<String>();
    InputStreamReader isr = null;
    BufferedReader br = null;
    try {
      isr = new InputStreamReader(is, "UTF-8");
      br = new BufferedReader(isr);
      String line = "";
      while ((line = br.readLine()) != null) {
        line = line.trim();
        if (line.length() == 0) {
          continue;
        }
        if (toLowerCase) {
          line = line.toLowerCase();
        }
        set.add(line);
        if (logging) {
          LOG.info("Add entry: '" + line + "'");
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException ignore) {
        }
      }
      if (isr != null) {
        try {
          isr.close();
        } catch (IOException ignore) {
        }
      }
      if (is != null) {
        try {
          is.close();
        } catch (IOException ignore) {
        }
      }
    }
    LOG.info("Loaded total " + set.size() + " entries");
    return set;
  }

    public static Map<String, Double>  redFileSWN(String file, String splitRegex) {
        return redFileSWN(IOUtils.getInputStream(file), splitRegex);
    }
  
    public static Map<String, Double> redFileSWN(InputStream in, String splitRegex){
        Map<String, Double> map = new HashMap<>();
        InputStreamReader isr = null;
        BufferedReader br = null;
        
        try {
            isr = new InputStreamReader(in, "UTF-8");
            br = new BufferedReader(isr);
            String line = "";
            while ((line = br.readLine()) != null) {
                if (line.trim().length() == 0) {
                    continue;
                }
                String[] values = line.split(splitRegex);
                String tag = values[0].trim();
                String wordnet = values[5].trim();
                
                double  POS = Double.parseDouble(values[2].trim());
                double  NEG = Double.parseDouble(values[3].trim());
                double  OBJ = 1-(POS+NEG);

                if (map.containsKey(wordnet+"/"+tag+"/POS")){
                    map.put(""+wordnet+"/"+tag+"/POS", POS + map.get(wordnet+"/"+tag+"/POS"));
                } else {	
                    map.put(""+wordnet+"/"+tag+"/POS", POS);
                }
                
                if (map.containsKey(wordnet+"/"+tag+"/NEG")){
                    map.put(""+wordnet+"/"+tag+"/NEG", NEG + map.get(wordnet+"/"+tag+"/NEG"));	
                } else {
                    map.put(""+wordnet+"/"+tag+"/NEG", NEG);
                }
                
                if (map.containsKey(wordnet+"/"+tag+"/OBJ")) {
                    map.put(""+wordnet+"/"+tag+"/OBJ", OBJ + map.get(wordnet+"/"+tag+"/OBJ"));	
                } else {
                    map.put(""+wordnet+"/"+tag+"/OBJ", OBJ);
                }
                

            }


        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignore) {}
            }
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException ignore) {}
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignore) {}
            }
        }
        return map;
    }
  
  

    public static void writeCsvFile(List<Double> data, String file_name) throws IOException {
        //Delimiter used in CSV file
        String DELIMITER = ";";
        String NEW_LINE_SEPARATOR = "\n";

        File file  = new File(file_name);
        if (!file.exists()) {
            file.createNewFile();
        }
        
        if(data.size()>1){
            System.out.println(data.toString());

            FileWriter fileWriter = null;
            try {
                fileWriter = new FileWriter(file, true);
                for (int i = 0; i < data.size(); i++) {
                    if (i== (data.size()-1)) {
                        fileWriter.append(Double.toString(data.get(i)) );
                        fileWriter.append(NEW_LINE_SEPARATOR);
                    } else {
                        fileWriter.append(Double.toString(data.get(i)) );
                        fileWriter.append(DELIMITER);
                    }
                }
            } catch (Exception e) {
              System.out.println("Error in CsvFileWriter !!!");
              e.printStackTrace();
            } finally {
                  try {
                      fileWriter.flush();
                      fileWriter.close();
                  } catch (IOException e) {
                      System.out.println("Error while flushing/closing fileWriter !!!");
                      e.printStackTrace();
                  }
            }
        }
    }
	
	
    public static void writeClassPlarity(String data, String f) throws IOException {
        //Delimiter used in CSV file
        String NEW_LINE_SEPARATOR = "\n";

        File file = new File(f);
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file, true);
            fileWriter.append(data);
            fileWriter.append(NEW_LINE_SEPARATOR);
        } catch (Exception e) {
          System.out.println("Error in CsvFileWriter !!!");
          e.printStackTrace();
        } finally {
              try {
                  fileWriter.flush();
                  fileWriter.close();
              } catch (IOException e) {
                  System.out.println("Error while flushing/closing fileWriter !!!");
                  e.printStackTrace();
              }
        }
    }

}
