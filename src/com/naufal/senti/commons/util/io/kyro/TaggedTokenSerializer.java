package com.naufal.senti.commons.util.io.kyro;

import cmu.arktweetnlp.Tagger.TaggedToken;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class TaggedTokenSerializer extends Serializer<TaggedToken> {

  @Override
  public TaggedToken read(Kryo kryo, Input input, Class<TaggedToken> type) {
    return new TaggedToken(input.readString(), input.readString());
  }

  @Override
  public void write(Kryo kryo, Output output, TaggedToken taggedToken) {
    output.writeString(taggedToken.token);
    output.writeString(taggedToken.tag);
  }

}
