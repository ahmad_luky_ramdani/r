package com.naufal.senti.commons.util.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.database.Mysql;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Bogor Agriculture Univeristy
 * @author ahmadluky
 */
public class GetXMLUtils {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(GetXMLUtils.class);
    private static final boolean LOGGING = Configuration.get("commons.crawler.xml.logging", false);

    public StringBuilder allstringBuilder;
    @SuppressWarnings("rawtypes")
    public List<Map> xmlLists;
    static Mysql  conect;

    public GetXMLUtils() throws ClassNotFoundException, SQLException{
        conect = new Mysql();
        allstringBuilder =  new StringBuilder();
        xmlLists = Configuration.getXmlListUri();
    }
	
    //get and read RSS/XML content from url link
    @SuppressWarnings({"rawtypes", "resource" })
    public String getReadXMLData() throws MalformedURLException, SQLException, IOException 
    {
        BufferedWriter out;
        for (Map xml : xmlLists) 
        {
            // save xml in file
            File xmlFile = new File("resources/datasets/xml/"+ xml.get("name").toString() + ".xml");
            if (LOGGING)
                LOG.info(xml.get("name").toString());
            URL url = new URL((String) xml.get("path"));
            URLConnection con   = url.openConnection();
            BufferedReader in   = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line = in.readLine();
            StringBuilder builder   = new StringBuilder();
            do {
                builder.append(line).append("\n");
            } while ( (line = in.readLine()) != null);
            // write xml content in file             
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(xmlFile)));
            out.write(builder.toString());
            out.flush();
            // save xml content to mysql
            String str = builder.toString().replace("'", "\"");
            String sql = "Insert Into xml_data (`data`, `id`, `from`) "
                        + "values ('"+str+"', NULL, '" + xml.get("name").toString() + "')";
            conect.sqlInsert(sql);
            // add uri xml/rss
            allstringBuilder.append(builder);
        }
        return allstringBuilder.toString();
    }
	
    //get and read RSS/XML content Read XML data From file (example)
    @SuppressWarnings("resource")
	public String getReadXMLData(String directory) throws SQLException, FileNotFoundException, IOException 
    {
        StringBuilder all_builder   = new StringBuilder();
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        BufferedReader in;
        
        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println("Read : "+file.getName());
                //Get file from resources folder
                StringBuilder builder   = new StringBuilder();
                in = new BufferedReader(new FileReader(directory+file.getName()));
                String line             = in.readLine();
                do {
                    builder.append(line).append("\n");
                } while ( (line = in.readLine()) != null);	  

                // save to mysql
                String str = builder.toString().replace("'", "\"");
                String sql = "Insert Into xml_data (`data`, `id`, `from`) "
                              + "values ('"+str+"', NULL, '"+file.getName()+"')";
                conect.sqlInsert(sql);
                all_builder.append(str);
            }
        }
        return all_builder.toString();
    }
    
    //get Link in RSS Content using regex 
    //result link/uri HTML
    public List<String> LinkHtml(String xml) throws SAXException, IOException, ParserConfigurationException, SQLException {
    	List<String> listLink = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\<link>(.*?)\\</link>");
        Matcher matcher = pattern.matcher(xml);
        while (matcher.find()) {
            try {
                String sql = "Insert Into xml_linkHtml (`uri_link`, `id`) "
                            + "values ('"+matcher.group(1)+"', NULL)";
                conect.sqlInsert(sql);
                listLink.add(matcher.group(1));
            } catch (Exception e) {
                    continue;
            }
        }
        return listLink;
    }
    
    /**
     * Main function Test
     * Result : link list to article
     * @param args
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, ClassNotFoundException, SQLException{
		
        GetXMLUtils xml = new GetXMLUtils();
        String xml_string = xml.getReadXMLData();
        // Get Link HTML
    	xml.LinkHtml(xml_string);
    	/**
        ReadXML xml = new ReadXML();
    	String xml_string = xml.getReadXMLData("resources/datasets/xml/");
    	xml.LinkHtml(xml_string);
        **/
    }
    
}
