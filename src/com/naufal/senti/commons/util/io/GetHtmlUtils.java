package com.naufal.senti.commons.util.io;

import com.naufal.senti.commons.Configuration;

import java.io.IOException;
import java.sql.SQLException;

import org.slf4j.LoggerFactory;
import com.naufal.senti.commons.database.Mysql;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author ahmadluky
 * Ipb
 */
public class GetHtmlUtils {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(GetHtmlUtils.class);
    static Mysql  conect;
    @SuppressWarnings("rawtypes")
	static List<Map> HTMlLists ;
    static List<String> AlllinkList;
    
    public GetHtmlUtils() throws ClassNotFoundException, SQLException {
        conect = new Mysql();
        HTMlLists = Configuration.getHtmlListUri();
    }
   
    @SuppressWarnings({ "rawtypes" })
	public void GetHTML_linkInHTML() throws IOException{
        for (Map linkHTML : HTMlLists) 
        {  
			LOG.info((String) linkHTML.get("name")+"===");
            // setiap link html memiliki tag link yg bebeda 
            if (linkHTML.get("name").equals("pdi")) {
                Document doc = Jsoup.connect((String) linkHTML.get("path")).get();
                Elements links = doc.select("a.heading-link"); // define attribute
                for(Element link:links){
                    try {
                        LOG.info(link.attr("abs:href"));
                        String sql1 = "Insert Into xml_linkHtml (`uri_link`, `id`) values ('"+link.attr("abs:href")+"', NULL)";
                        conect.sqlInsert(sql1);
                        //
                        AlllinkList.add(link.attr("abs:href"));
                    } catch (SQLException ex) {
                        Logger.getLogger(GetHtmlUtils.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
            if (linkHTML.get("name").equals("demokrat")) {
                Document doc = Jsoup.connect((String) linkHTML.get("path")).get();
                Elements links = doc.select("h1.entry-title.post-title a"); // define attribute
                for(Element link:links){
                    try {
                        LOG.info(link.attr("abs:href"));
                        String sql2 = "Insert Into xml_linkHtml (`uri_link`, `id`) values ('"+link.attr("abs:href")+"', NULL)";
                        conect.sqlInsert(sql2);
                        //
                        AlllinkList.add(link.attr("abs:href"));
                    } catch (SQLException ex) {
                        Logger.getLogger(GetHtmlUtils.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
            if (linkHTML.get("name").equals("pan")) {
                Document doc = Jsoup.connect((String) linkHTML.get("path")).get();
                Elements links = doc.select(".col-md-9.full-h a.normalfont"); // define attribute
                for(Element link:links){
                    try {
                        LOG.info(link.attr("abs:href"));
                        String sql3 = "Insert Into xml_linkHtml (`uri_link`, `id`) values ('"+link.attr("abs:href")+"', NULL)";
                        conect.sqlInsert(sql3);
                        //
                        AlllinkList.add(link.attr("abs:href"));
                    } catch (SQLException ex) {
                        Logger.getLogger(GetHtmlUtils.class.getName()).log(Level.SEVERE, null, ex);
                    }
                };
            }            
        }
    }
    
    @SuppressWarnings("null")
	public List<String> GetHTML_jsoup(List<String> linkList) throws IOException, SQLException{
        List<String> ArtikelList = null;
        AlllinkList = linkList;
        for (int i = 0; i < AlllinkList.size(); i++) {
            String link = AlllinkList.get(i);
            if (!link.equals("http://partaigerindra.or.id") 
                   && !link.equals("http://www.fraksipkb.com") 
                   && !link.equals("http://pks.id/")
                   && !link.equals("http://news.detik.com/")
                   && !link.equals("http://www.republika.co.id")
                   && !link.equals("http://rss.viva.co.id")
                   && !link.equals("http://www.antaranews.com")) 
            {
                try {
                    LOG.info("Fetching ..."+link);
                    Document doc = Jsoup.connect(link).timeout(0).get();
                    String text = doc.body().text(); 
                    String sql = "Insert Into html_data (`data`, `id`) values ('"+text+"', NULL)";
                    conect.sqlInsert(sql);
                    //
                    ArtikelList.add(text);
                } catch (IOException e) {
                    continue;
                }
            } else {
                LOG.info("No Fetching "+ link);
            }
        }
        return ArtikelList;
        /**
        String selectLink = "SELECT * FROM xml_linkHtml";
        try (ResultSet rs = conect.sqlSelect(selectLink)) 
        {
            while(rs.next())
            {
                String url = rs.getString("uri_link");
                if (!url.equals("http://partaigerindra.or.id") 
                    && !url.equals("http://www.fraksipkb.com") 
                    && !url.equals("http://pks.id/")
                    && !url.equals("http://news.detik.com/")
                    && !url.equals("http://www.republika.co.id")
                    && !url.equals("http://rss.viva.co.id")
                    && !url.equals("http://www.antaranews.com")) 
                {
                    try {
                        LOG.info("Fetching ..."+url);
                        Document doc = Jsoup.connect(url).timeout(0).get();
                        String text = doc.body().text(); 
                        String sql = "Insert Into html_data (`data`, `id`) values ('"+text+"', NULL)";
                        conect.sqlInsert(sql);
                    } catch (IOException | SQLException e) {
                        continue;
                    }
                } else {
                    LOG.info("No Fetching "+url);
                }
                        
            }   
        }
        return null;
        */
    }
    
    /**
     * Main Function Test
     * Get HTML code dari Uri
     * @param args
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException 
    {  
    	//GetHtmlUtils read  = new GetHtmlUtils();
        //get Link HTML
        //read.GetHTML_linkInHTML();
        //read.GetHTML_jsoup();

    }
}
