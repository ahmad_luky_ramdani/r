package com.naufal.senti.commons.knn;

import com.naufal.senti.commons.FeaturedTweet;
import com.naufal.senti.commons.Tweet;
import com.naufal.senti.commons.featurevector.CombinedFeatureVectorGeneratorBahasa;
import com.naufal.senti.commons.featurevector.FeatureVectorGeneratorBahasa;
import com.naufal.senti.commons.featurevector.SentimentFeatureVectorGeneratorBahasa;
import com.naufal.senti.components.POSTagHMM;
import com.naufal.senti.components.PreprocessorBahasa;
import com.naufal.senti.components.Tokenizer;
import com.naufal.senti.cs.ir.kbbi.IOException_Exception;
import com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.sf.javaml.classification.Classifier;
import net.sf.javaml.classification.evaluation.CrossValidation;
import net.sf.javaml.classification.evaluation.PerformanceMeasure;
import net.sf.javaml.core.Dataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KNNBahasa {
    public static final String KNN_PROBLEM_FILE = "./resources/models/knn_problem.txt";
    public static final String KNN_MODEL_FILE_SER = "./resources/models/knn_model.ser";
    private static final Logger LOG = LoggerFactory.getLogger(KNNBahasa.class);
    
    public static List<List<Double>> generateProblem(List<FeaturedTweet> featuredTweets) {
        List<List<Double>> nodes = new ArrayList<>();
        for (FeaturedTweet tweet : featuredTweets) {
            List<Double> once = new ArrayList<>();
            Map<Integer, Double> featureVector = tweet.getFeatureVector();
            // set feature nodes
            for (Map.Entry<Integer, Double> feature : featureVector.entrySet()) 
            {
                once.add((double)feature.getValue());
            }
            // set class / label
            once.add((double)tweet.getScore());
            nodes.add(once);
        }
        return nodes;
    }
    public static void saveProblem(List<List<Double>> knnProb, String file) {
        try (BufferedWriter br = new BufferedWriter(new FileWriter(file))) {
            for (List<Double> list : knnProb) {
                for (int i = 0; i < list.size(); i++) {
                    if (i==(list.size()-1)) {
                        br.write(Double.toString(list.get(i)));
                    }else{
                        br.write(Double.toString(list.get(i))+",");
                    }
                }
                br.newLine();
                br.flush();
            }
        } catch (IOException e) {
          LOG.error("IOException: " + e.getMessage());
        }
        LOG.info("saved svm_problem in " + file);
    }
    public static Map<Object, PerformanceMeasure> crossValidate(Classifier knn, Dataset data) {
        /* Construct new cross validation instance with the KNN classifier */
        CrossValidation cv = new CrossValidation(knn);
        /* Perform 5-fold cross-validation on the data set */
        Map<Object, PerformanceMeasure> p = cv.crossValidation(data);
        return p;
    }
    public static void knn(
            List<Tweet> trainTweets,
            Class<? extends FeatureVectorGeneratorBahasa> featureVectorGeneratorBahasa,
            int nFoldCrossValidation, 
            boolean parameterSearch,
            boolean useSerialization) throws SQLException, IOException, InterruptedException, IOException_Exception, MalformedURLException_Exception 
    {   
        FeatureVectorGeneratorBahasa fvg = null;
        PreprocessorBahasa preprocessor = null;
        POSTagHMM posTaggerhmm = null;
        // Prepare data tweets
        LOG.info("Prepare Train data...");
        List<FeaturedTweet> featuredTrainTweets = null;
        if (featuredTrainTweets == null) {
            // Read train tweets
            com.naufal.senti.commons.Dataset.printTweetStats(trainTweets);
            // Tokenize
            LOG.info("Tokenize train tweets...");
            List<List<String>> tokenizedTweets = Tokenizer.tokenizeTweets(trainTweets);
            // Preprocess
            preprocessor = PreprocessorBahasa.getInstance();
            LOG.info("Preprocess train tweets...");
            List<List<String>> preprocessedTweets = preprocessor.preprocessTweets(tokenizedTweets);
            // POS Tagging
            posTaggerhmm = POSTagHMM.getInstance();
            LOG.info("POS Tagging of train tweets...");
            List<List<String>> taggedTweets = posTaggerhmm.tagTweets(preprocessedTweets);
            // Create Feature Vector Generator
            if (featureVectorGeneratorBahasa.equals(SentimentFeatureVectorGeneratorBahasa.class)) {
                LOG.info("Load SentimentFeatureVectorGeneratorBahasa...");
                fvg = new SentimentFeatureVectorGeneratorBahasa();
            } else {
                throw new UnsupportedOperationException("FeatureVectorGenerator '"+ featureVectorGeneratorBahasa.getName() + "' is not supported!");
            }
            // Feature Vector Generation
            LOG.info("Generate Feature Vectors for train tweets...");
            featuredTrainTweets = new ArrayList<>();
            for (int i = 0; i < taggedTweets.size(); i++) {
                List<String> taggedTweet = taggedTweets.get(i);
                Map<Integer, Double> featureVector = fvg.generateFeatureVector(taggedTweet);
                featuredTrainTweets.add(new FeaturedTweet(trainTweets.get(i), tokenizedTweets.get(i), preprocessedTweets.get(i), taggedTweet, featureVector));
            }
        }
        // generate problem
        LOG.info("Generate problem...");
        List<List<Double>> knnProb = generateProblem(featuredTrainTweets);
        // save svm problem in libSVM format
        LOG.info("Save problem...");
        saveProblem(knnProb, KNN_PROBLEM_FILE);
        // Load a data set
        // LOG.info("Load KNN problem...");
        // Dataset data = FileHandler.loadDataset(new File(KNN_PROBLEM_FILE), 4, ",");
        // Contruct a KNN classifier that uses 5 neighbors to make a decision.
        // Classifier knn = new KNearestNeighbors(5);
        // knn.buildClassifier(data);
        // Map<Object, PerformanceMeasure> rst = crossValidate(knn,data);
        // Print result 
        // System.out.print(rst);
    }
    public static void main(String[] args) throws IOException, SQLException, InterruptedException, IOException_Exception, MalformedURLException_Exception{
        List<Tweet> tweet = Tweet.getTestTweets();
        boolean parameterSearch = false;
        boolean useSerialization = false;
        int nFoldCrossValidation = 10;
        int featureVectorLevel = 0;
        switch (featureVectorLevel) {
            case 0:
                KNNBahasa.knn(  tweet, 
                                SentimentFeatureVectorGeneratorBahasa.class,
                                nFoldCrossValidation, 
                                parameterSearch, 
                                useSerialization);
                break;
            default:
                KNNBahasa.knn(  tweet, 
                                CombinedFeatureVectorGeneratorBahasa.class,
                                nFoldCrossValidation, 
                                parameterSearch, 
                                useSerialization);
                break;
      }
    }   
}
