package com.naufal.senti.commons.wordnet;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.database.Mysql;
import com.naufal.senti.commons.util.io.IOUtils;
import com.naufal.senti.commons.util.io.SerializationUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author ahmadluky
 * using wordnet for bahasa indonesia 
 * source : http://wn-msa.sourceforge.net/index.eng.html
 * source : http://globalwordnet.org/wordnets-in-the-world/
 */
public final class WordNetBahasa {
    
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WordNetBahasa.class);
    private static final boolean LOGGING = Configuration.get("commons.wordnetbahasa.logging", false);
    private static final WordNetBahasa INSTANCE = new WordNetBahasa();
    Mysql mysql ;
    Map<String, Double> wordNetList = null;
    @SuppressWarnings("rawtypes")
	private final List<Map> wordnetBahasa;
    private String delimiter;
    private String path;
    
    @SuppressWarnings("rawtypes")
	public WordNetBahasa(){
        LOG.info("Load configuration wordnet bahasa connection .....");
        wordnetBahasa = Configuration.getWordNetBahasaDict();
        for(Map wordnet : wordnetBahasa){
            path = (String) wordnet.get("path");
            String serializationFile = path + ".ser";
            if (IOUtils.exists(serializationFile)) {
                if (LOGGING)
                LOG.info("Deserialize Wordnet Bahsa from: " + serializationFile);
                wordNetList = SerializationUtils.deserialize(serializationFile);
            } else {
                delimiter = (String) wordnet.get("delimiter");  
                if (LOGGING)
                LOG.info("Load Wordnet Bahsa from: " + path);
                wordNetList = readWordNetBahasa(path, delimiter);
                SerializationUtils.serializeMap(wordNetList, serializationFile);
            }
        }
    }
    
    public static WordNetBahasa getInstance() {
        return INSTANCE;
    }
    
    public Map<String, Double> readWordNetBahasa(String file, String sparator){
        Map<String, Double> map = new HashMap<>();
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            isr = new InputStreamReader(IOUtils.getInputStream(file), "UTF-8");
            br = new BufferedReader(isr);
            String line = "";
            Double tmppos = 0.0;
            Double tmpneg = 0.0;
            while ((line = br.readLine()) != null) {
                if (line.trim().length() == 0) {
                    continue;
                }
                String[] values = line.split(sparator);
                if (map.get(values[1].trim()+"#"+values[2].trim()+"#pos")!= null) {
                    tmppos = map.get(values[1].trim()+"#"+values[2].trim()+"#pos");
                    map.put(values[1].trim()+"#"+values[2].trim()+"#pos", Double.parseDouble(values[3])+tmppos);
                } else {
                    map.put(values[1].trim()+"#"+values[2].trim()+"#pos", Double.parseDouble(values[3]));
                }
                if (map.get(values[1].trim()+"#"+values[2].trim()+"#neg")!= null) {
                    tmpneg = map.get(values[1].trim()+"#"+values[2].trim()+"#neg");
                    map.put(values[1].trim()+"#"+values[2].trim()+"#neg", Double.parseDouble(values[4])+tmpneg);
                } else {
                    map.put(values[1].trim()+"#"+values[2].trim()+"#neg", Double.parseDouble(values[4]));
                }
            }
        } catch (IOException e) {}
        LOG.info("Loaded total " + map.size() + " entries");
        return map;
    }
    public Double findStems(String posTag, String word, String label) {
        if (LOGGING)
        LOG.info(posTag+"#"+word+"#"+label);
        if (wordNetList.get(posTag+"#"+word+"#"+label)!= null) {
            return wordNetList.get(posTag+"#"+word+"#"+label);
        } else {
            return null;
        }
    }
    // 1 if positif soce > negatif score, 0 otherwis
    public Map<Integer, Double> sentimentScores(String posTag, String word){
        Map<Integer, Double> val = new HashMap<>();
        if (findStems(posTag, word, "pos") > findStems(posTag, word, "neg")) {
            val.put(1, 1.0);// if null value tidak terdapat pada kamus
            return val;
        } else {
            val.put(1, 0.0);// if null value tidak terdapat pada kamus
            return val; 
        }
    }
    // SentiWordNet Insert
    public void insertSentiword_ID() throws SQLException{
        try {
            InputStreamReader isr = new InputStreamReader(IOUtils.getInputStream(path), "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (line.length() == 0)
                    continue;
                String[] word = line.split(",");
                String sqlSentiwordNet = "INSERT INTO sentiwordnet_id (`tag`, `index`, `pos`, `neg`, `word_en`, `word_id`) "
                                          + "values ('"+ word[0] +"', '"+ word[1] +"', '"+ word[2] +"', '"+ word[3] +"', '"+  word[4].replace("'", "") +"', '"+ word[5].replace("'", "") +"')";
                mysql.sqlInsert(sqlSentiwordNet);
                LOG.info("Add entry: '" + line + "'");
            }
        } catch (IOException e) {}
    }
    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException, SAXException, ParserConfigurationException{  
        WordNetBahasa WNB = new WordNetBahasa();
        System.out.println(WNB.findStems("n","kelemahan","pos"));
        System.out.println(WNB.findStems("n","kelemahan","neg"));
    }
}
