package com.naufal.senti.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/**
 * 
 * @author @ahmadluky
 *
 */
public class Tweet implements Serializable {
    private static final long serialVersionUID = 3778428208873277421L;
    private final Long m_id;
    private final String m_text;
    private final Double m_score;

    public Tweet(Long id, String text, Double score) {
        this.m_id = id;
        this.m_text = text;
        this.m_score = score;
    }

    public Tweet(Long id, String text) {
        this(id, text, null);
    }

    public Long getId() {
        return m_id;
    }

    public String getText() {
        return m_text;
    }

    public Double getScore() {
        return m_score;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tweet other = (Tweet) obj;
        // check if id is matching
        if (!Objects.equals(this.m_id, other.getId())) {
            return false;
        }
        // check if text is matching
        return this.m_text.equals(other.getText());
    }

    @Override
    public String toString() {
        return "Tweet [id=" + m_id + ", text=" + m_text + ((m_score != null) ? ", score=" + m_score : "") + "]";
    }
    
    public static List<Tweet> getTestTweets() {
        List<Tweet> tweets = new ArrayList<>();
        tweets.add(new Tweet(1L, "kita benci jokowi :)", 1.0));
        tweets.add(new Tweet(2L, "jokowi tetapi malas :)", 2.0));
        tweets.add(new Tweet(3L, "4ku suka jokowi :(", 0.0));
        return tweets;
    }
    
    public static List<Tweet> getTrainTweets() {
        List<Tweet> tweets = new ArrayList<>();
        tweets.add(new Tweet(1L, "kita benci jokowi :)", 1.0));
        tweets.add(new Tweet(2L, "jokowi tetapi malas :)", 2.0));
        tweets.add(new Tweet(3L, "4ku suka jokowi :(", 0.0));
        return tweets;
    }
     public static List<Tweet> getTestTweet() {
        List<Tweet> tweet = new ArrayList<>();
        tweet.add(new Tweet(1L, "@jokowi semangattt p4k presiden, terima masukan ekonom yg pro kesejahteraan r4ky4t moga berjalan lancar 4ku", 2.0));
        return tweet;
    }
}
