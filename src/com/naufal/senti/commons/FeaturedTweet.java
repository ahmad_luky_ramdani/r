package com.naufal.senti.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.naufal.senti.commons.util.io.SerializationUtils;
import com.naufal.senti.components.Tokenizer;
import com.naufal.senti.commons.featurevector.FeatureVectorGeneratorBahasa;
import com.naufal.senti.commons.featurevector.SentimentFeatureVectorGeneratorBahasa;
import com.naufal.senti.components.POSTagHMM;
import com.naufal.senti.components.PreprocessorBahasa;
import com.naufal.senti.cs.ir.kbbi.IOException_Exception;
import com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class FeaturedTweet extends Tweet implements Serializable {
    
    private static final Logger LOG = LoggerFactory.getLogger(FeaturedTweet.class);
    private static final long serialVersionUID = -1917356433934166756L;
    private final List<String> m_tokens;
    private final List<String> m_preprocessedTokens;
    private final List<String> m_taggedTokens;
    private final Map<Integer, Double> m_featureVector; // dense vector

    public FeaturedTweet(
            long id, 
            String text, 
            double score, 
            List<String> tokens,
            List<String> preprocessedTokens, 
            List<String> taggedTokens,
            Map<Integer, Double> featureVector) 
    {
        super(id, text, score);
        m_tokens = tokens;
        m_preprocessedTokens = preprocessedTokens;
        m_taggedTokens = taggedTokens;
        m_featureVector = featureVector;
    }

    public FeaturedTweet(Tweet tweet, 
                         List<String> tokens,
                         List<String> preprocessedTokens, 
                         List<String> taggedTokens,
                         Map<Integer, Double> featureVector) 
    {
            this(tweet.getId(), tweet.getText(), tweet.getScore(), tokens, preprocessedTokens, taggedTokens, featureVector);
    }

    public List<String> getTokens() {
        return m_tokens;
    }

    public List<String> getPreprocessedTokens() {
        return m_preprocessedTokens;
    }

    public List<String> getTaggedTokens() {
        return m_taggedTokens;
    }

    public Map<Integer, Double> getFeatureVector() {
        return m_featureVector;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public static final List<List<String>> getTaggedTokensFromTweets(List<FeaturedTweet> featuredTweets) {
        List<List<String>> taggedTweets = new ArrayList<>();
        for (FeaturedTweet tweet : featuredTweets) {
            taggedTweets.add(tweet.getTaggedTokens());
        }
        return taggedTweets;
    }

    public static final List<FeaturedTweet> generateFeatureTweets(List<Tweet> tweets) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException {
        final PreprocessorBahasa preprocessorBahasa = PreprocessorBahasa.getInstance();
        final POSTagHMM postTagger = POSTagHMM.getInstance();
        // Tokenize
        List<List<String>> tokenizedTweets = Tokenizer.tokenizeTweets(tweets);
        // Preprocess
        List<List<String>> preprocessedTweets = preprocessorBahasa.preprocessTweets(tokenizedTweets);
        // POS Tagging
        List<List<String>> taggedTweets = postTagger.tagTweets(preprocessedTweets);
        // Load Feature Vector Generator
        FeatureVectorGeneratorBahasa fvg = new SentimentFeatureVectorGeneratorBahasa();
        // Feature Vector Generation
        List<FeaturedTweet> featuredTweets = new ArrayList<>();
        for (int i = 0; i < tweets.size(); i++) {
            List<String> taggedTweet = taggedTweets.get(i);
            LOG.info("taggedTweet : "+taggedTweet.toString());
            Map<Integer, Double> featureVector = fvg.generateFeatureVector(taggedTweet);
            LOG.info("featureVector : "+featureVector.toString());
            featuredTweets.add(new FeaturedTweet(tweets.get(i), tokenizedTweets.get(i), preprocessedTweets.get(i), taggedTweet, featureVector));
        }
        return featuredTweets;
    }

  public static void main(String[] args) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException {
        Dataset dataset = Configuration.getDataSet();
        // Generate features of train tweets
        List<FeaturedTweet> featuredTrainTweets = generateFeatureTweets(dataset.getTrainTweets());
        SerializationUtils.serializeCollection(featuredTrainTweets,dataset.getTrainDataSerializationFile());
        // Generate features of test tweets
//         List<FeaturedTweet> featuredTestTweets = generateFeatureTweets(dataset.getTestTweets());
//         SerializationUtils.serializeCollection(featuredTestTweets,dataset.getTestDataSerializationFile());
  }

}
