package com.naufal.senti.commons;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import com.naufal.senti.commons.util.io.IOUtils;
/**
 * @author ahmadluky
 */
public class Configuration {

    private static final Logger LOG = LoggerFactory.getLogger(Configuration.class);
    public static final boolean RUNNING_WITHIN_JAR = Configuration.class.getResource("Configuration.class").toString().startsWith("jar:");
    public static final String WORKING_DIR_PATH = (RUNNING_WITHIN_JAR) ? "": System.getProperty("user.dir") + File.separator;
    public static final String TEMP_DIR_PATH = System.getProperty("java.io.tmpdir");
    public static final String GLOBAL_RESOURCES_DATASETS = "global.resources.datasets";
    public static final String GLOBAL_RESOURCES_DICT = "global.resources.dict";
    public static final String GLOBAL_RESOURCES_DICT_SENTIMENT = "global.resources.dict.sentiment";
    public static final String GLOBAL_RESOURCES_DICT_SLANG = "global.resources.dict.slang";
    public static final String GLOBAL_RESOURCES_DICT_WORDNET_PATH = "global.resources.dict.wordnet.path";
    public static final String GLOBAL_RESOURCES_DICT_WORDNETBAHASA_PATH = "global.resources.dict.wordnet.bahasa.path";
    public static final String GLOBAL_RESOURCES_DICT_SENTIWORDNET_PATH = "global.resources.dict.sentiwordnet.path";
    public static final String GLOBAL_RESOURCES_XML = "global.resource.data.xml";
    public static final String GLOBAL_RESOURCES_HTML = "global.resource.data.html";
    public static final String MYSQL_RESOURCES = "global.connection.mysql";
    public static final String GLOBAL_RESOURCES_POSTAG = "global.resources.postag";
    public static final String TWITTER = "twitter.key";

    @SuppressWarnings("rawtypes")
    public static final Map CONFIG = readConfig();

    @SuppressWarnings({ "rawtypes" })
    public static Map readConfig() {
        Map conf = readConfigFile(WORKING_DIR_PATH + "conf/defaults.yaml", true);
        return conf;
    }
    
    @SuppressWarnings("rawtypes")
    public static Map readConfigFile(String file, boolean mustExist) {
        Yaml yaml = new Yaml(new SafeConstructor());
        Map ret = null;
        InputStream input = IOUtils.getInputStream(file);
        if (input != null) {
            ret = (Map) yaml.load(new InputStreamReader(input));
            LOG.info("Loaded " + file);
            try {
                input.close();
            } catch (IOException e) {
                LOG.error("IOException: " + e.getMessage());
            }
        } else if (mustExist) {
            LOG.error("Config file " + file + " was not found!");
        }
        if ((ret == null) && (mustExist)) {
            throw new RuntimeException("Config file " + file + " was not found!");
        }
        return ret;
    }

    public static <K, V> V get(K key) {
        return get(key, null);
    }

    @SuppressWarnings("unchecked")
    public static <K, V> V get(K key, V defaultValue) {
        return get((Map<K, V>) CONFIG, key, defaultValue);
    }

    public static <K, V> V get(Map<K, V> map, K key, V defaultValue) {
        V value = map.get(key);
        if (value == null) {
            value = defaultValue;
        }
        return value;
    }

    @SuppressWarnings("rawtypes")
	public static Dataset getDataSet() {
        return Dataset.readFromYaml((Map) ((Map) CONFIG.get(GLOBAL_RESOURCES_DATASETS)).get("2016"));
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<Map> getPOSTag() {
        return (List<Map>) ((Map) CONFIG.get(GLOBAL_RESOURCES_POSTAG)).get("HMM");
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<Map> getSymbol() {
        return (List<Map>) ((Map) CONFIG.get(GLOBAL_RESOURCES_DICT)).get("Symbol");
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<String> getNewsaccount(){
        return (List<String>) ((Map) CONFIG.get(GLOBAL_RESOURCES_DICT)).get("Newsaccount");
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<String> getStopWords() {
        return (List<String>) ((Map) CONFIG.get(GLOBAL_RESOURCES_DICT)).get("StopWords");
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Map> getSentimentWordlists() {
        return (List<Map>) CONFIG.get(GLOBAL_RESOURCES_DICT_SENTIMENT);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Map> getSlangWordlists() {
        return (List<Map>) CONFIG.get(GLOBAL_RESOURCES_DICT_SLANG);
    }

    public static String getWordNetDict() {
        return (String) CONFIG.get(GLOBAL_RESOURCES_DICT_WORDNET_PATH);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Map> getWordNetBahasaDict() {
        return (List<Map>) CONFIG.get(GLOBAL_RESOURCES_DICT_WORDNETBAHASA_PATH);
    }

    public static String getSentiwordnetDict(){
        return (String) CONFIG.get(GLOBAL_RESOURCES_DICT_SENTIWORDNET_PATH);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<Map> getXmlListUri() {
        return (List<Map>) CONFIG.get(GLOBAL_RESOURCES_XML);
    }
  
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<Map> getHtmlListUri() {
        return (List<Map>) CONFIG.get(GLOBAL_RESOURCES_HTML);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<Map> getMysqlResource(){
        return (List<Map>) CONFIG.get(MYSQL_RESOURCES);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<Map> getTwitterResource(){
        return (List<Map>) CONFIG.get(TWITTER);
    }
}
