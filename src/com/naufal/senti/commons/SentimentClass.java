package com.naufal.senti.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 * @author ahmadluky
 *
 */
public enum SentimentClass {
    POSITIVE, NEGATIVE, NEUTRAL;
    private static final Logger LOG = LoggerFactory.getLogger(SentimentClass.class);
    public static SentimentClass fromScore(Dataset dataset, int score) {
        if (score == dataset.getPositiveValue()) {
            return POSITIVE;
        } else if (score == dataset.getNegativeValue()) {
            return NEGATIVE;
        } else if (score == dataset.getNeutralValue()) {
            return NEUTRAL;
        } else {
            LOG.error("Score: '" + score + "' is not valid!");
        }
        return null;
    }
}
