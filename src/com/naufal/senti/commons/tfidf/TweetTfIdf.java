package com.naufal.senti.commons.tfidf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.dict.StopWords;
import com.naufal.senti.commons.util.StringUtils;

public class TweetTfIdf {
    private static final Logger LOG = LoggerFactory.getLogger(TweetTfIdf.class);

    private final TfType m_tfType;
    private final TfIdfNormalization m_tfIdfNormalization;
    private List<Map<String, Double>> m_termFreqs;
    private Map<String, Double> m_inverseDocFreq;
    private Map<String, Integer> m_termIds;
    private final boolean m_usePOSTags;

    private TweetTfIdf(TfType type, TfIdfNormalization normalization,boolean usePOSTags) {
      this.m_tfType = type;
      this.m_tfIdfNormalization = normalization;
      this.m_usePOSTags = usePOSTags;
    }

    public TfType getTfType() {
      return m_tfType;
    }

    public TfIdfNormalization getTfIdfNormalization() {
      return m_tfIdfNormalization;
    }

    public List<Map<String, Double>> getTermFreqs() {
      return m_termFreqs;
    }

    public Map<String, Double> getInverseDocFreq() {
      return m_inverseDocFreq;
    }

    public Map<String, Integer> getTermIds() {
      return m_termIds;
    }
    public Map<String, Double> tfIdfFromTaggedTokens(List<String> tweet) {
    	return TfIdf.tfIdf(tfFromTaggedTokens(tweet, m_tfType, m_usePOSTags), m_inverseDocFreq, m_tfIdfNormalization);
    }
    public static TweetTfIdf createFromTaggedTokens(List<List<String>> tweets, boolean usePOSTags) {
    	return createFromTaggedTokens(tweets, TfType.RAW, TfIdfNormalization.NONE,usePOSTags);
    }
    public static TweetTfIdf createFromTaggedTokens(List<List<String>> tweets, TfType type,TfIdfNormalization normalization, boolean usePOSTags) {
    	TweetTfIdf tweetTfIdf = new TweetTfIdf(type, normalization, usePOSTags);
    	tweetTfIdf.m_termFreqs = tfTaggedTokenTweets(tweets, type, usePOSTags);
    	tweetTfIdf.m_inverseDocFreq = idf(tweetTfIdf.m_termFreqs);
    	tweetTfIdf.m_termIds = new HashMap<String, Integer>();
    	int i = 0;
    	for (String key : tweetTfIdf.m_inverseDocFreq.keySet()) {
    		tweetTfIdf.m_termIds.put(key, i);
    		i++;
    	}
    	LOG.info("Found " + tweetTfIdf.m_inverseDocFreq.size() + " terms");
    	// Debug
    	// print("Term Frequency", m_termFreqs, m_inverseDocFreq);
    	// print("Inverse Document Frequency", m_inverseDocFreq);
    	return tweetTfIdf;
    }
    public static List<Map<String, Double>> tfTaggedTokenTweets(List<List<String>> tweets, TfType type, boolean usePOSTags) {
	    List<Map<String, Double>> termFreqs = new ArrayList<Map<String, Double>>();
	    for (List<String> tweet : tweets) {
	        termFreqs.add(tfFromTaggedTokens(tweet, type, usePOSTags));
	    }
	    return termFreqs;
    }
    public static Map<String, Double> tfFromTaggedTokens(List<String> tweet,TfType type, boolean usePOSTags) {
    	Map<String, Double> termFreq = new LinkedHashMap<String, Double>();
    	StopWords stopWords = StopWords.getInstance();
    	List<String> words = new ArrayList<String>();
    	for (String taggedToken : tweet) {
			String[] taggString = taggedToken.split("/");
			String word = taggString[0].toLowerCase();
			String Tag = taggString[1];
			if ((!Tag.equals(",")) && (!Tag.equals("$"))
					&& (!Tag.equals("G")) && (!Tag.equals("@"))
					&& (!Tag.equals("~")) && (!Tag.equals("U"))
					&& (!stopWords.isStopWord(word))) {
			
				          // Remove hashtag
				          if (Tag.equals("#")) {
				            word = word.substring(1);
				          }
				          // Check if word starts with an alphabet
				          if (!StringUtils.startsWithAlphabeticChar(word)) {
				            continue;
				          }
				          // add word to term frequency
					      words.add(word);
		    		}
	    }
    	termFreq = TfIdf.tf(termFreq, words);
    	termFreq = TfIdf.normalizeTf(termFreq, type);
    	return termFreq;
    }

    public static Map<String, Double> idf(List<Map<String, Double>> termFreq) {
      return TfIdf.idf(termFreq);
    }

    public static List<Map<String, Double>> tfIdf(
        List<Map<String, Double>> termFreqs, Map<String, Double> inverseDocFreq,
        TfIdfNormalization normalization) {

      List<Map<String, Double>> tfIdf = new ArrayList<Map<String, Double>>();
      // compute tfIdf for each document
      for (Map<String, Double> doc : termFreqs) {
        tfIdf.add(TfIdf.tfIdf(doc, inverseDocFreq, normalization));
      }

      return tfIdf;
    }

}
