package com.naufal.senti.commons.database;

import com.naufal.senti.commons.util.io.IOUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
/**
 * 
 * @author @ahmadluky
 * # Wordnet Bahasa	
 * ind	
 * http://wn-msa.sourceforge.net/	
 * MIT 
 *
 */
public class Import_WordnetBahasa {
    
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(Import_WordnetBahasa.class);
    public static BufferedReader readFile(String file, String splitRegex) {
        return readFile(IOUtils.getInputStream(file), splitRegex, true);
    }
    public static BufferedReader readFile(InputStream is, String splitRegex, boolean logging) {
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            isr = new InputStreamReader(is, "UTF-8");
            br = new BufferedReader(isr);
            return br;
        } catch (IOException e) {
            return null;
        } 
    }
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, SQLException, ClassNotFoundException{
        Mysql  conect = new Mysql();
        String File_wordnetBahasa = "./resources/dictionaries/sentiment/sentiwordnet-wordnetbahasa/wordnetbahasa/SentiWordNet_index_score.txt";
        BufferedReader buffer_File = readFile(File_wordnetBahasa, ";");
        String line = "";
        while ((line = buffer_File.readLine()) != null) {
            if (line.trim().length() == 0) {
                continue;
            }
            String[] values = line.split(";");
            String sql = "Insert Into sentiwordnet_index_score (`index`, `pos`, `neg`) values ('"+values[0].trim()+"', '"+values[1].trim()+"', "+values[2].trim()+" )";
            LOG.info(sql);
            conect.sqlInsert(sql);
        }
        conect.closex();
    }
}
