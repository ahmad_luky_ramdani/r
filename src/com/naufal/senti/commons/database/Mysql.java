package com.naufal.senti.commons.database;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.mysql.jdbc.Connection;
import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.Tweet;

import java.util.HashMap;

public class Mysql {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(Mysql.class);
    public static Connection connect = null;
    @SuppressWarnings("rawtypes")
	public List<Map> confMysql;
    public String user;
    public String password;
    public String database;

    @SuppressWarnings("rawtypes")
	public Mysql() throws ClassNotFoundException, SQLException{
        LOG.info("Load configuration connection .....");
        confMysql = Configuration.getMysqlResource();
        for(Map conf : confMysql){
            user = (String) conf.get("user");
            password = (String) conf.get("password");
            database = (String) conf.get("database");
        }
        LOG.info("Connecting.....");
        Class.forName("com.mysql.jdbc.Driver");
        connect = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/"+database, user, password);
    }
    public static ResultSet sql(String sqlString) throws SQLException{
        Statement statement = connect.createStatement();
        return  statement.executeQuery(sqlString);
    }
    public void sqlDelete(String table, String whereField, String whereVal) throws SQLException{
        PreparedStatement preparedStatement = connect.prepareStatement("delete from "+table+" where "+whereField+"= ? ; ");
        preparedStatement.setString(1, whereVal);
        preparedStatement.executeUpdate();
    }
    public void sqlInsert(String sqlInsert) throws SQLException{
        PreparedStatement preparedStatement = connect.prepareStatement(sqlInsert);
        preparedStatement.executeUpdate();
    }
    public void sqlUpdate(String sql) throws SQLException{
		Statement statement = connect.createStatement();
		System.out.println(sql);
		statement.execute(sql);
    }
    public void closex() throws SQLException{
        connect.close();
    }
    /**
     * Mysql IO
     * @return 
     * @throws java.sql.SQLException
     */
    public Map<String, String> contextual() throws SQLException{
        Map<String, String> context = new HashMap<>();
        String sql = "SELECT * FROM contextual";
        ResultSet result = sql(sql);
        while(result.next()){
            String key = result.getString("contextual");
            String value = result.getString("name");
            context.put(key, value);
        }   
        return context;
    }
    
    public List<Tweet> readTweetsDB(String DB, String field) throws SQLException{
    	System.out.println("From DB "+DB);
        List<Tweet> tweets = new ArrayList<>();
        String sql = "SELECT "+field+" FROM "+DB+"";
        ResultSet result = sql(sql);
        while(result.next())
        {
        	long id = 0;
            try {
                id = Long.parseLong(result.getString("id_str"));
            } catch (NumberFormatException e) {
                id = 0;
            }
            String text = result.getString("text");
            String label = result.getString("kelas");
            double score = -1;
            if (label=="negatif") {
                score = 1.0;
            } else if (label=="netral") {
                score = 2.0;
            } else if (label=="positif") {
                score = 0.0;
            }
            System.out.println(id+"/"+text+"/"+score);
            tweets.add(new Tweet(id, text, score));
        }   
        return tweets;
    }
    
    /**
     * Main function Test
     * @param args
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
     public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, SQLException, ClassNotFoundException{
            Mysql  conect = new Mysql();
            LOG.info(conect.toString());
            String sql = "Insert Into data_xml (`xml_data`, `id`, `name`) values ('data_xml_example', NULL, 'test')";
            conect.sqlInsert(sql);
     }
         
}
