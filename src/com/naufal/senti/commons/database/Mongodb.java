package com.naufal.senti.commons.database;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

import org.slf4j.LoggerFactory;

import java.net.UnknownHostException;
import java.util.Set;

import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 * @author ahmadluky
 */
public class Mongodb {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(Mongodb.class);
    public MongoClient connect = null;
    public DB db = null;
    DBCollection collection = null;
    public static Set<String> collections_name = null;
    DBCursor cursor =null;

    public Mongodb() throws UnknownHostException {
        LOG.info("Connecting.....");
        connect = new MongoClient( "localhost" , 27017 ); // connection in java
    }
    public void SelectDb(String dbName){
        LOG.info("Selected DB.....");
        db = connect.getDB(dbName); //name databases
    }
    public void GetCollectionALL(){
        collections_name = db.getCollectionNames(); // get all list collection name
    }
    public Set<String> GetCollectionALLColl(){
        return db.getCollectionNames(); // get all list collection name
    }
    public DBCursor findAll(String collection_name){
        collection = db.getCollection(collection_name); //name collection
        cursor = collection.find(); // run query
        return cursor;
    }
    public DBCursor findQuery(String collection_name, String field, String value){
        collection = db.getCollection(collection_name); //name collection
        BasicDBObject obj_query = new BasicDBObject(field,value); // query
        cursor = collection.find(obj_query);
        return cursor;
    }
	public DBCursor findQueryRegex(String collName, String string,String string2) {
		// TODO Auto-generated method stub
		return null;
	}
	public void insert(String collName, BasicDBObject doc) {
		// TODO Auto-generated method stub
		
	}
    public void MongoClose(){
        connect.close();
    }
    /**
     * Main function Test
     * @param args
     * @throws java.net.UnknownHostException
     * @throws twitter4j.JSONException
     */
     public static void main(String[] args) throws UnknownHostException, JSONException {
        Mongodb  conect = new Mongodb();
        conect.SelectDb("research");
        DBCursor rst = conect.findAll("data_ahok_20150813");
        while(rst.hasNext()) {
            DBObject dbObject = rst.next();
            String jsonString = JSON.serialize(dbObject);       
            JSONObject jsonObject = new JSONObject(jsonString);
            if(jsonObject.isNull("id_str")){
                continue;
            }else{
                String id = (String) jsonObject.get("id_str");
                JSONObject users = jsonObject.getJSONObject("user");
                String id_user = users.getString("id_str");
                String url = "https://twitter.com/"+id_user+"/status/"+id;
                LOG.info(url);
            }
    	}
     }
}
