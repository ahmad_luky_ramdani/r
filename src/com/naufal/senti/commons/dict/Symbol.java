package com.naufal.senti.commons.dict;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.util.io.FileUtils;

import java.util.List;
import java.util.Map;

public class Symbol {
    private static final Logger LOG = LoggerFactory.getLogger(Symbol.class);
    private static final Symbol INSTANCE = new Symbol();
    private Map<String, String> m_Symbol;
    
    @SuppressWarnings("rawtypes")
	private Symbol() {
        List<Map> symLists = Configuration.getSymbol();
        for(Map symbolListEntry:symLists){
            String file = (String) symbolListEntry.get("path");
            boolean isEnabled = (Boolean) symbolListEntry.get("enabled");
            if (isEnabled) {
                // membaca file symbol
                String separator = (String) symbolListEntry.get("delimiter");
                LOG.info("Load SymbolLookupTable from: " + file);
                m_Symbol = FileUtils.readFile(file, separator);
            }	
        }
    }
    public static Symbol getInstance() {
        return INSTANCE;
    }
    public String getCorrection(String term) {
    	// check range date / year / int
    	String[] part = term.split("-");
    	if (part.length > 1) {
            try {
                int part1 = Integer.parseInt(part[0]);
                int part2 = Integer.parseInt(part[1]);
                if (part1 > 0 && part2 > 0) {
                    return Integer.toString(part1)+"-"+Integer.toString(part2);
                }
            } catch (Exception e) {
                return term;
            }
        }
    	// check is numaric and year
    	try {
            int angka = Integer.parseInt(term);
            if (angka > 0) {
                return Integer.toString(angka);
            }
        } catch (Exception e) {
            // convert symbil in word
            term = term + " ";
            int l_str = term.length();
            String result_str = "";
            char ch;
            for(int i=0; i<l_str-1; i++){
                ch=term.charAt(i);
                    String ch_ = m_Symbol.get(String.valueOf(ch));
                    if (ch_ != null) {
                        result_str = result_str + ch_;
                    }else{
                        result_str = result_str + ch;
                    }
            }
            return result_str;
        }
        return term;
    }
    public static void main(String[] args) {
        Symbol symbol = Symbol.getInstance();
        // Test Symbol
        String[] testFirstNames = new String[] {"4ku", "Ok3", "S3mang4t", "Gw3" };
        for (String s : testFirstNames) {
          System.out.println("isSymbolCorection(" + s + "): " + symbol.getCorrection(s));
        }
    }
}
