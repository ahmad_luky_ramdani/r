package com.naufal.senti.commons.dict;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.util.io.FileUtils;
import com.naufal.senti.commons.util.io.IOUtils;
import com.naufal.senti.commons.util.io.SerializationUtils;

public class SlangCorrection {
    private static final Logger LOG = LoggerFactory.getLogger(SlangCorrection.class);
    private static final SlangCorrection INSTANCE = new SlangCorrection();
    private final Map<String, String[]> m_slangWordList = new HashMap<>();
    
    
    @SuppressWarnings("rawtypes")
	private SlangCorrection() {
        List<Map> slangWordLists = Configuration.getSlangWordlists();
        for (Map slangWordListEntry : slangWordLists) {
            String file = (String) slangWordListEntry.get("path");
            boolean isEnabled = (Boolean) slangWordListEntry.get("enabled");
            if (isEnabled) {
                Map<String, String> slangWordList = null;
                String serializationFile = file + ".ser";
                if (IOUtils.exists(serializationFile)) {
                    LOG.info("Deserialize SlangLookupTable from: " + serializationFile);
                    slangWordList = SerializationUtils.deserialize(serializationFile);
                } else {
                    String separator = (String) slangWordListEntry.get("delimiter");
                    LOG.info("Load SlangLookupTable from: " + file);
                    slangWordList = FileUtils.readFile(file, separator);
                    SerializationUtils.serializeMap(slangWordList, serializationFile);
                }
                // Insert new entries
                for (Map.Entry<String, String> entry : slangWordList.entrySet()) {
                    if (!m_slangWordList.containsKey(entry.getKey())) {
                        m_slangWordList.put(entry.getKey(), entry.getValue().split(" "));
                    }
                }
            }
        }
    }

    public static SlangCorrection getInstance() {
        return INSTANCE;
    }

    public String[] getCorrection(String token) {
        return m_slangWordList.get(token);
    }

    public static void main(String[] args) {
        SlangCorrection slangCorrection = SlangCorrection.getInstance();
        // Test SlangCorrection
        String[] testSlangCorrection = new String[] { "omg", "afk", "yg", "yudeh" };
        for (String s : testSlangCorrection) {
            System.out.println("getCorrection(" + s + "): " + Arrays.toString(slangCorrection.getCorrection(s)));
        }
    }
}
