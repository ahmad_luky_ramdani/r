package com.naufal.senti.commons.dict;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.util.StringUtils;
import com.naufal.senti.commons.util.io.FileUtils;
import com.naufal.senti.commons.util.io.IOUtils;
import com.naufal.senti.commons.util.io.SerializationUtils;
import com.naufal.senti.commons.wordnet.POSTag;
import com.naufal.senti.commons.Tweet;
import com.naufal.senti.commons.featurevector.SentimentFeatureVectorGeneratorBahasa;
import com.naufal.senti.components.POSTagHMM;
import com.naufal.senti.components.PreprocessorBahasa;
import com.naufal.senti.components.Tokenizer;
import com.naufal.senti.cs.ir.kbbi.IOException_Exception;
import com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception;

import java.io.IOException;
import java.sql.SQLException;

public class SentimentDictionary {
	
    private static final Logger LOG = LoggerFactory.getLogger(SentimentDictionary.class);
    private static final boolean LOGGING = Configuration.get("commons.sentimentdictionary.logging", false);
    private static final SentimentDictionary INSTANCE = new SentimentDictionary();
    //private WordNetBahasa m_wordnetbahasa=null;
    private List<Map<String, Double>> m_wordLists = null;

    @SuppressWarnings({ "rawtypes", "unchecked" })
	private SentimentDictionary() {
        //m_wordnetbahasa = WordNetBahasa.getInstance();
        m_wordLists = new ArrayList<>();
        List<Map> wordLists = Configuration.getSentimentWordlists();
        for (Map wordListEntry:wordLists){
            String file = (String) wordListEntry.get("path");
            String separator = (String) wordListEntry.get("delimiter");
            boolean containsPOSTags = (Boolean) wordListEntry.get("containsPOSTags");
            boolean featureScaling = (Boolean) wordListEntry.get("featureScaling");
            double minValue = (Double) wordListEntry.get("minValue");
            double maxValue = (Double) wordListEntry.get("maxValue");
            boolean isEnabled = (Boolean) wordListEntry.get("enabled");
            if (isEnabled) 
            {
                String serializationFile = file + ".ser";
                if (IOUtils.exists(serializationFile)) 
                {
                    LOG.info("Deserialize WordList from: " + serializationFile);
                    m_wordLists.add((Map<String, Double>) SerializationUtils.deserialize(serializationFile));
                } else {
                    LOG.info("Load WordList from: " + file);
                    Map<String, Double> wordList = FileUtils.readFile(file, separator,containsPOSTags, featureScaling, minValue, maxValue);
                    SerializationUtils.serializeMap(wordList, serializationFile);
                    m_wordLists.add(wordList);
                }
            }
        }
    }
    public static SentimentDictionary getInstance() {
        return INSTANCE;
    }
    public int getSentimentWordListCount() { // get count lexicon iusing
        return m_wordLists.size();
    }
    public Map<Integer, Double> getWordSentiments(String word) {
        Map<Integer, Double> sentimentScores = new HashMap<>();
        for (int i = 0; i < m_wordLists.size(); i++) {
            Double sentimentScore = m_wordLists.get(i).get(word);
            if (sentimentScore != null) {
                sentimentScores.put(i, sentimentScore);
            }
        }
        if (LOGGING) {
            LOG.info("getWordSentiment('" + word + "'): " + sentimentScores);
        }
        return (sentimentScores.size() > 0) ? sentimentScores : null;
    }
    private Map<Integer, Double> getWordSentiment(String word, String tag) {
        String posTagHmm = null;
        posTagHmm = POSTag.convertHMM(tag);
        boolean wordIsHashtag = tag.equals("#") || tag.equals("HT");
        boolean wordIsEmoticon = tag.equals("E") || tag.equals("UH");
        if (wordIsHashtag && (word.length() > 1)) {
            if (word.indexOf('@') == 1) {
                word = word.substring(2); // check for #@ HASHTAG_USER
            } else {
                word = word.substring(1);
            }
        } else if ((!wordIsEmoticon) && StringUtils.consitsOfPunctuations(word)) {
            return null;
        } else if (StringUtils.consitsOfUnderscores(word)) {
            return null;
        }
        if (!wordIsEmoticon) {
            word = word.toLowerCase();
        }
        
        Map<Integer, Double> sentimentScores = getWordSentiments(word);
        
        //wordnet in indonesia lang not defined
        //if (sentimentScores == null) {
        //    if (LOGGING) {
        //        LOG.info("findStems for (" + word + "," + posTagHmm + ") in wordnet bahasa");
        //    }
        //    sentimentScores = m_wordnetbahasa.sentimentScores(posTagHmm,word);
        //}
        
        if (LOGGING) {
            LOG.info("getWordSentiment ('" + word + "'\'" + posTagHmm+ "'): " + sentimentScores);
        }
        return sentimentScores;
    }
    public Map<Integer, SentimentResult> getSentenceSentiment( List<String> sentence) {
        Map<Integer, SentimentResult> sentenceSentiments = new HashMap<>();
        if (LOGGING) {
            LOG.info("Sentence Tag List : " + sentence.toString());
        }
        for (String word : sentence) {
            String[] value = word.split("/");
            Map<Integer, Double> wordSentiments = getWordSentiment(value[0].trim(),value[1]); // [word/tag]
            if (wordSentiments != null) {
                if (LOGGING) {
                    LOG.info("getWordSentiment (): "+word+" "+wordSentiments.toString());
                }
                for (Map.Entry<Integer, Double> wordSentiment : wordSentiments.entrySet()) {
                    int key = wordSentiment.getKey();
                    double sentimentScore = wordSentiment.getValue();
                    SentimentResult sentimentResult = sentenceSentiments.get(key);
                    if (sentimentResult == null) {
                        sentimentResult = new SentimentResult();
                    }
                    // add score value
                    sentimentResult.addScore(sentimentScore);
                    // update sentimentResult
                    sentenceSentiments.put(key, sentimentResult);
                }
            }
        }
        if (LOGGING) {
            LOG.info("Sentiment: " + sentenceSentiments);
        }
        return (sentenceSentiments.size() > 0) ? sentenceSentiments : null;
    }
    public List<Map<Integer, SentimentResult>> getSentiment(List<List<String>> tweets) {
        List<Map<Integer, SentimentResult>> tweetSentiments = new ArrayList<>();
        for (List<String> tweet : tweets) {
            tweetSentiments.add(getSentenceSentiment(tweet));
        }
        return tweetSentiments;
    }
    
    // main test
    @SuppressWarnings("unused")
	public static void main(String[] args) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException, SQLException {
        SentimentDictionary sentimentDictionary = SentimentDictionary.getInstance();
        
        // Test
        List<String> data = new ArrayList<>();
        data.add("semangat/NN");
        data.add("pak/NN");
        data.add("presiden/NN"); 
        data.add("terima/VBT"); 
        data.add("masukan/NN"); 
        data.add("ekonom/NN");
        data.add("kesejahteraan/NN"); 
        data.add("rakyat/NN");
        data.add("senyum/NN");
        data.add("moga/RB");
        data.add("berjalan/VBI"); 
        data.add("lancar/NN");
        data.add(":)/E]");
        
        
        List<Tweet> tweet = Tweet.getTestTweets();
        // Read train tweets
        com.naufal.senti.commons.Dataset.printTweetStats(tweet);
        // Tokenize
        LOG.info("Tokenize train tweets...");
        List<List<String>> tokenizedTweets = Tokenizer.tokenizeTweets(tweet);
        // Preprocess
        PreprocessorBahasa preprocessor = PreprocessorBahasa.getInstance();
        LOG.info("Preprocess train tweets...");
        List<List<String>> preprocessedTweets = preprocessor.preprocessTweets(tokenizedTweets);
        // POS Tagging
        POSTagHMM posTaggerhmm = POSTagHMM.getInstance();
        LOG.info("POS Tagging of train tweets...");
        List<List<String>> taggedTweets = posTaggerhmm.tagTweets(preprocessedTweets);
        // Create Feature Vector Generator
        LOG.info("Load SentimentFeatureVectorGeneratorBahasa...");
        SentimentFeatureVectorGeneratorBahasa fvg = new SentimentFeatureVectorGeneratorBahasa();
        
        //Feature Vector Generation
//        LOG.info("Generate Feature Vectors for train tweets...");
//        List<FeaturedTweet> featuredTrainTweets = new ArrayList<>();
//        for (int i = 0; i < taggedTweets.size(); i++) {
//            List<String> taggedTweet = taggedTweets.get(i);
//            Map<Integer, Double> featureVector = fvg.generateFeatureVector(taggedTweet);
//            featuredTrainTweets.add(new FeaturedTweet(tweet.get(i), tokenizedTweets.get(i), preprocessedTweets.get(i), taggedTweet, featureVector));
//        }
//          
//
//        List<Map<Integer, SentimentResult>>  rst = sentimentDictionary.getSentiment(taggedTweets);
//        for (Map<Integer, SentimentResult> key : rst) {
//            for(Integer i:key.keySet()){
//                LOG.info(key.get(i).toString());
//            }
//        }

        Map<Integer, SentimentResult>  rst = sentimentDictionary.getSentenceSentiment(data);
        for(Integer key : rst.keySet()){
           SentimentResult a = rst.get(key);
           LOG.info("Key :"+key.toString());
           LOG.info("ResultSentiment :"+a.toString());
           LOG.info("==================");
        }
    }

}
