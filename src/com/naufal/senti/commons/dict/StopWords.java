package com.naufal.senti.commons.dict;

import java.util.List;
import java.util.Set;

import org.apache.storm.guava.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.util.io.FileUtils;
import com.naufal.senti.commons.util.io.IOUtils;
import com.naufal.senti.commons.util.io.SerializationUtils;

public class StopWords {
    // default stop words from nltk.corpus
    public static final String[] STOP_WORDS = new String[] { "ada","biasanya","adalah","bila","adanya","bilamana",
        "adapun","buat","aduh","bukan","agar","dalam","ah","dan","akan","dapat","aku","dari","alih-alih","daripada",
        "anda","dekat","andai","demi","antar","demikian","antara","dengan","apa","depan","apakah","di","apalagi",
        "dia","asalkan","dikatakan","atas","dilakukan","atau","dkk","ataupun","dll","bagai","dsb","bagaikan","engkau",
        "bagaimana","hal","bagaimanakah","hamper","bagaimanapun","hanya","bagi","harus","bahkan","hingga","bahwa",
        "ia","balik","ialah","banyak","ini","barangkali","itu","bawah","iya","beberapa","jadi","begini","jangan",
        "begitu","jarang","belakang","jauh","belum","jika","berapa","jikalau","berbagai","juga","bersama","jumlah",
        "rempong","saiko","peres","kalau","kalian","kami","kamu","karena","kata","katanya","kau","ke","kebanyakan",
        "kecuali","kemanakah","kemudian","kenapa","kenapakah","kepada","ketika","ketimbang","kini","kita","lagi",
        "lain","lain-lain","lainnya","lalu","lebih","lepas","lewat","maka","makin","manakala","masih","masing-masing",
        "masing-masingnya","maupun","melainkan","melakukan","melalui","memang","mengatakan","mengenai","repot",
        "gila","palsu","menunjukkan","menurut","mereka","merupakan","meski","meskipun","misalnya","mungkin",
        "namun","nanti","nyaris","oleh","pada","padahal","para","pasti","pelbagai","per","peri","perihal",
        "pinggir","pula","pun","saat","saja","sambil","sampai","samping","sang","sangat","sangatlah","saya",
        "seakan","seakan-akan","seantero","sebab","sebabnya","sebagai","sebagaimana","sebagainya","sebelum",
        "beserta","betapa","biar","sedang","sedangkan","sedikit","segera","sehabis","sehingga","sehubungan",
        "sejak","sejumlah","sekarang","sekeliling","seketika","sekitar","sekonyong-konyong","selagi","selain",
        "selalu","selama","serta","sesuai","sesuatu","tersebut","justru","kadang","kadang-kadang","selanjutnya",
        "selesai","seluruh","seluruhnya","semakin","semenjak","sementara","semua","semuanya","seorang","sepanjang",
        "seperti","sepertinya","seputar","seraya","sering","seringkali","tentunya","tergolong","terhadap","terjadi",
        "sesudah","menjadi","menjelang","menuju","seusai","sewaktu","si","siapa","siapakah","siapapun",
        "suatu","sudah","supaya","tanpa","tapi","tatkala","telah","tengah","tentang","tentu",
        "sesungguhnya","setelah","seterusnya","setiap","sesudahnya","sebelumnya","sebuah","secara",
        "tertentu","tetap","tetapi","tiap","tiba-tiba","ujar","ujarnya","umumnya","untuk","walau",
        "walaupun","ya","yaitu","yakni","yang","terkadang","terlalu","terlebih","termasuk","ternyata" };

    private static final Logger LOG = LoggerFactory.getLogger(StopWords.class);
    private static final StopWords INSTANCE = new StopWords();
    private Set<String> m_stopwords = null;

    @SuppressWarnings("unchecked")
	private StopWords() {
        m_stopwords = Sets.newHashSet(STOP_WORDS);
        List<String> files = Configuration.getStopWords();
        if (files != null) {
            for (String file : files) {
                // Try deserialization of file
                String serializationFile = file + ".ser";
                if (IOUtils.exists(serializationFile)) {
                    LOG.info("Deserialize FirstNames from: " + serializationFile);
                    m_stopwords.addAll((Set<String>) SerializationUtils.deserialize(serializationFile));
                } else {
                    LOG.info("Load StopWords from: " + file);
                    Set<String> stopWords = FileUtils.readFile(file, true);
                    SerializationUtils.serializeCollection(stopWords, serializationFile);
                    m_stopwords.addAll(stopWords);
                }
            }
        }
    }

    public static StopWords getInstance() {
        return INSTANCE;
    }

    public boolean isStopWord(String value) {
        return m_stopwords.contains(value.toLowerCase());
    }

    public static void main(String[] args) {
        StopWords stopWords = StopWords.getInstance();
        // Test StopWords
        String[] testStopWords = new String[] { "saya", "aku", };
        for (String s : testStopWords) {
            System.out.println("isStopWord(" + s + "): " + stopWords.isStopWord(s));
        }
    }

}
