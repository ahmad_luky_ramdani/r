package com.naufal.senti.commons.dict;

import com.naufal.senti.commons.Configuration;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.util.io.FileUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Calculation Sentiment Score in sentence by Kerstin Denecke 2008 [http://ieeexplore.ieee.org/xpl/freeabs_all.jsp?arnumber=4498370&abstractAccess=no&userType=inst]
 */
public class SentiWordNetCalculation {
    
    private static final Logger LOG = LoggerFactory.getLogger(SentiWordNetCalculation.class);
    private static final SentiWordNetCalculation INSTANCE = new SentiWordNetCalculation();
    private static Map<String, Double> rst;
    private final Map<String, Double> wordlist;

    private SentiWordNetCalculation(){
        String sentiwordnet = Configuration.getSentiwordnetDict();
        LOG.info("Load WordList from: " + sentiwordnet);
        wordlist = FileUtils.redFileSWN(sentiwordnet, ",");
    }
    public static SentiWordNetCalculation getInstance() {
        return INSTANCE;
    }
    public Map <String, Double> getSentiSentence (List<String>  sentence){
        double sumPos = 0;
        double sumNeg = 0;
        double sumObj = 0;
        for (String token : sentence) {
            String value[] = token.split("/");
            if (wordlist.containsKey(value[0]+"/"+value[1]+"/POS"))
            sumPos += wordlist.get(value[0]+"/"+value[1]+"/POS");
            if (wordlist.containsKey(value[0]+"/"+value[1]+"/NEG"))
            sumNeg += wordlist.get(value[0]+"/"+value[1]+"/NEG");
            if (wordlist.containsKey(value[0]+"/"+value[1]+"/OBJ"))
            sumObj += wordlist.get(value[0]+"/"+value[1]+"/OBJ");
        }
        Map<String, Double> sentiScore = new HashMap<>();
        sentiScore.put("pos", sumPos);
        sentiScore.put("neg", sumNeg);
        sentiScore.put("obj", sumObj);
        return sentiScore;
    }
	public static void main (String[] args) {
        List<String> sentence = new ArrayList<String>();
        sentence.add("jelek/n");
        SentiWordNetCalculation sentiWordNetCalculation = SentiWordNetCalculation.getInstance();
        rst = sentiWordNetCalculation.getSentiSentence(sentence);
        LOG.info("positive :" + rst.get("pos") + " negative : " + rst.get("neg") + " objective : "+ rst.get("obj"));
    }

}
