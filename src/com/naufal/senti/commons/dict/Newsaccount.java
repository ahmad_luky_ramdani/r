package com.naufal.senti.commons.dict;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.util.io.FileUtils;
import com.naufal.senti.commons.util.io.IOUtils;
import com.naufal.senti.commons.util.io.SerializationUtils;

public class Newsaccount {
    private static final Logger LOG = LoggerFactory.getLogger(Newsaccount.class);
    private static final Newsaccount INSTANCE = new Newsaccount();
    private Set<String> m_Newsaccount = null;

    @SuppressWarnings("unchecked")
	private Newsaccount() {
        for (String file : Configuration.getNewsaccount()) {
            // Try deserialization of file
            String serializationFile = file + ".ser";
            if (IOUtils.exists(serializationFile)) {
                LOG.info("Deserialize Symbol from: " + serializationFile);
                if (m_Newsaccount == null) {
                    m_Newsaccount = SerializationUtils.deserialize(serializationFile);
                } else {
                    m_Newsaccount.addAll((Set<String>) SerializationUtils.deserialize(serializationFile));
                }
            } else {
                LOG.info("Load Symbol from: " + file);
                if (m_Newsaccount == null) {
                    m_Newsaccount = FileUtils.readFile(file, true);
                    SerializationUtils.serializeCollection(m_Newsaccount,serializationFile);
                } else {
                    Set<String> firstNames = FileUtils.readFile(file, true);
                    SerializationUtils.serializeCollection(firstNames, serializationFile);
                    m_Newsaccount.addAll(firstNames);
                }
            }
        }
    }

    public static Newsaccount getInstance() {
        return INSTANCE;
    }

    public boolean isNewsaccount(String value) {
        return m_Newsaccount.contains(value);
    }

    public static void main(String[] args) {
        Newsaccount newsaccount = Newsaccount.getInstance();
        // Test Symbol
        String[] testNewsaccount = new String[] { "@detikcom", "infobdgnews", "PemumBdg" };
        for (String s : testNewsaccount) {
          System.out.println("isNewsaccount(" + s + "): " + newsaccount.isNewsaccount(s));
        }
    }

}
