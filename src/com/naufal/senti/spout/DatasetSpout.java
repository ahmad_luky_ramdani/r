package com.naufal.senti.spout;

import java.util.List;
import java.util.Map;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.Dataset;
import com.naufal.senti.commons.Tweet;
import com.naufal.senti.commons.util.TimeUtils;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class DatasetSpout extends BaseRichSpout {
  public static final String ID = "dataset-spout";
  public static final String CONF_STARTUP_SLEEP_MS = ID + ".startup.sleep.ms";
  public static final String CONF_TUPLE_SLEEP_MS = ID + ".tuple.sleep.ms";
  public static final String CONF_TUPLE_SLEEP_NS = ID + ".spout.tuple.sleep.ns";
  private static final long serialVersionUID = 3028853846518561027L;
  private Dataset m_dataset;
  private SpoutOutputCollector m_collector;
  private List<Tweet> m_tweets;
  private long m_messageId = 0;
  private int m_index = 0;
  private long m_tupleSleepMs = 0;
  private long m_tupleSleepNs = 0;

  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    // key of output tuples
    declarer.declare(new Fields("id", "score", "text"));
  }

  @SuppressWarnings("rawtypes")
  public void open(Map config, TopologyContext context,
    SpoutOutputCollector collector) {
        this.m_collector = collector;
        this.m_dataset = Configuration.getDataSet();
        this.m_tweets = m_dataset.getTestTweets();
        // Optional sleep between tuples emitting
        if (config.get(CONF_TUPLE_SLEEP_MS) != null) {
          m_tupleSleepMs = (Long) config.get(CONF_TUPLE_SLEEP_MS);
        } else {
          m_tupleSleepMs = 0;
        }
        if (config.get(CONF_TUPLE_SLEEP_NS) != null) {
          m_tupleSleepNs = (Long) config.get(CONF_TUPLE_SLEEP_NS);
        } else {
          m_tupleSleepNs = 0;
        }
        // Optional startup sleep to finish bolt preparation
        // before spout starts emitting
        if (config.get(CONF_STARTUP_SLEEP_MS) != null) {
          long startupSleepMillis = (Long) config.get(CONF_STARTUP_SLEEP_MS);
          TimeUtils.sleepMillis(startupSleepMillis);
        }
    }

  public void nextTuple() {
    Tweet tweet = m_tweets.get(m_index);

    // infinite loop
    m_index++;
    if (m_index >= m_tweets.size()) {
      m_index = 0;
    }
    m_messageId++; // accept possible overflow

    // Emit tweet
    m_collector.emit(
        new Values(tweet.getId(), tweet.getScore(), tweet.getText()),
        m_messageId);

    // Optional sleep
    if (m_tupleSleepMs != 0) {
      TimeUtils.sleepMillis(m_tupleSleepMs);
    }
    if (m_tupleSleepNs != 0) {
      TimeUtils.sleepNanos(m_tupleSleepNs);
    }
  }
}
