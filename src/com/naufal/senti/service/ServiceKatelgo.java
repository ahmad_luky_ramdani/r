package com.naufal.senti.service;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

//import org.json.JSONObject;
//import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ahmadluky
 */
class Katelgo{
    public String relation;
}

public class ServiceKatelgo {
    private static final Logger LOG = LoggerFactory.getLogger(ServiceKatelgo.class);
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	}

	public static String readJsonFromUrl(String word) throws IOException {
	    try (InputStream apiKatelgo = new URL("http://kateglo.com/api.php?format=json&phrase="+word).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(apiKatelgo, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            char a_char = jsonText.charAt(0);
            if (a_char == '<') {
                return "false";
            } else {
                Gson obj = new Gson();
                Katelgo katelgo = obj.fromJson(jsonText, Katelgo.class);
                if(katelgo.relation != null){
                    return "true";
                } else {
                    return "false";
                }
            }
	    }
	}
	/**
	 * Word Exsis
	 * @param word
	 * @return
	 * @throws IOException
	 */
	public static int Word_ex(String word) throws IOException{
        String rst = readJsonFromUrl(word);
     	LOG.info(rst);
		if (rst.equals("false")) {
		    return -1;
		} else{
			int Count_sim = rst.length();
		    return Count_sim;
		}
	}
	/**
	 * get All syninim word 
	 * @param word
	 * @return 
	 * @throws IOException
	 * 
	 * return List
	 */
    public static ArrayList<String> Word_list(String word) throws IOException{
        ArrayList<String> List_synonim 	= new ArrayList<>();
        String rst = readJsonFromUrl(word);
        int Count_sim = rst.length();
        for (int i = 0; i < Count_sim-2; i++) {
//            JSONObject x = (JSONObject) rst.get(i);
//            List_synonim.add((String) x.get("related_phrase"));
        }
        return List_synonim;
    }
    /**
     * main function for testing
     * @param args
     * @throws IOException 
     * @throws JSONException 
     */
    public static void main(String[] args) throws IOException {
    	ArrayList<String> rst = Word_list("semangat");
    	LOG.info(rst.toString());
    }
}

