package com.naufal.senti.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.naufal.senti.commons.database.Mysql;
import com.naufal.senti.cs.ir.kbbi.IOException_Exception;
import com.naufal.senti.cs.ir.kbbi.Kbbi_Service;
import com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception;
/**
 * 
 * @author ahmadluky
 *
 */
public class ServiceKbbi {
    private static final ServiceKbbi INSTANCE = new ServiceKbbi();
    public ServiceKbbi(){}
    public static ServiceKbbi getInstance() {
        return INSTANCE;
    }
    // Check KBBI
    public boolean contains(String value) throws IOException_Exception, MalformedURLException_Exception{
        Kbbi_Service kbbi_s = new Kbbi_Service();
        String mean = kbbi_s.getKbbiPort().getMean(value);
        // check main word is exsis return FALSE
        return mean.equals("<span>kata yang anda cari tidak ditemukan!</span>");
    }
    // Test main validasi term dan frasa
    @SuppressWarnings("static-access")
	public static void main(String[] args) throws IOException_Exception, MalformedURLException_Exception, InterruptedException, ClassNotFoundException, SQLException{ 
    	Mysql  conect = new Mysql();
        ServiceKbbi service = new ServiceKbbi();
        for (int i = 7723; i <=8223; i++) {
        	// selected from database
        	String sql = "SELECT * FROM lexicon.lexicon__mpqa_subjclueslen WHERE id="+i+"";
        	ResultSet result = conect.sql(sql);
        	while (result.next()){
	            String id = result.getString("id");
	            String word = result.getString("word");
	            if (!service.contains(word)) {
	                String sql_update = "Update lexicon.lexicon__mpqa_subjclueslen SET status='TRUE' WHERE id="+id+"";
	                conect.sqlUpdate(sql_update);
	    	        System.out.println("KBBI : "+word+"/"+service.contains(word));
				}
		        Thread.sleep(1000);
        	}
		}   
        /**
         * 	Testing KBBI service
	        String[] testFirstNames = new String[] { "oke", "semangat", "noh", "Tresiden", "dan", "Nilai", "mereka" };
	        for (String s : testFirstNames) {
		        System.out.println("KBBI : "+service.contains(s));
		        Thread.sleep(1000);
        	}
        **/
        
    }
}
