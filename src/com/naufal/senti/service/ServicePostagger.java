package com.naufal.senti.service;

import com.naufal.senti.cs.ir.postagger.Exception_Exception;
import com.naufal.senti.cs.ir.postagger.IOException_Exception;
import com.naufal.senti.cs.ir.postagger.POSTagger_Service;

public class ServicePostagger {
    private static final ServicePostagger INSTANCE = new ServicePostagger();
    public ServicePostagger(){}
    public static ServicePostagger getInstance() {
        return INSTANCE;
    }
    public String getPosTag(String sentence) throws Exception_Exception, IOException_Exception {
        POSTagger_Service postag = new POSTagger_Service();
        String tag = postag.getPOSTaggerPort().getPOSTag(sentence);
        // check main word is exsis return FALSE
        return tag;
    }
    // Test main
    public static void main(String[] args) throws IOException_Exception, InterruptedException, Exception_Exception{ 
        ServicePostagger service = new ServicePostagger();
//        String[] sentence = new String[] { "oke semangat ya pak Presiden Jokowi"};
//        for (String s : sentence) {
        System.out.println("PosTag : "+service.getPosTag("oke semangat ya pak Presiden Jokowi"));
//        Thread.sleep(1000);
//        }
        
    }
}
