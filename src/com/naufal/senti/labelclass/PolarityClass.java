package com.naufal.senti.labelclass;

import java.io.IOException;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.naufal.senti.commons.util.io.FileUtils;

/**
 * PLARITY CLASS USING SENTIWORDNET LEXICON
 * Classification rule polarity by frangky [https://ufal.mff.cuni.cz/pbml/103/art-franky-bojar-veselovska.pdf]
 * @author ahmadluky
 */
public class PolarityClass {

	private static final Logger LOG = LoggerFactory.getLogger(PolarityClass.class);
	
	public static String polarity(Double sentimenScorePos, Double sentimenScoreNeg, Double sentimenScoreObj){
		
		if (sentimenScorePos > sentimenScoreNeg) {
			return "positive";
		} else if (sentimenScorePos < sentimenScoreNeg) {
			return"negative";
		} else if (sentimenScorePos.equals(sentimenScoreNeg)) {
			return "netral";
		} else{
			return "null";
		}
		
	}
	/**
	 * main function
	 * @param args null
	 */
	public static void main (String[] args) {
		
		String data_fitur = "resources/datasets/data/data-example.txt";
		String f = "resources/datasets/data/data-example-result.txt";
		
		LOG.info("Load Data from: " + data_fitur);
		// data yang berupa list fitur pos/neg/objectif sentence
        Set<String> tweet = FileUtils.readFile(data_fitur); 
        for (Object object: tweet) {	
        	String tw = (String) object;
        	String[] polar = tw.split(",");
			String polatity = polarity(Double.parseDouble(polar[0]), Double.parseDouble(polar[1]), Double.parseDouble(polar[2]));
			LOG.info(""+polar[0]+"/"+polar[1]+"/"+polar[2]+"/"+polatity);
			
			try {
				FileUtils.writeClassPlarity(""+polar[0]+","+polar[1]+","+polar[2]+","+polatity+"", f);
			} catch (IOException e) {
				LOG.info("Error : Cannot to write file csv");
			}
		}
   }
}
