
package com.naufal.senti.cs.ir.kbbi;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cs.ir.kbbi package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetMeanResponse_QNAME = new QName("http://kbbi.ir.cs/", "getMeanResponse");
    private final static QName _MalformedURLException_QNAME = new QName("http://kbbi.ir.cs/", "MalformedURLException");
    private final static QName _GetMean_QNAME = new QName("http://kbbi.ir.cs/", "getMean");
    private final static QName _IOException_QNAME = new QName("http://kbbi.ir.cs/", "IOException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cs.ir.kbbi
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetMeanResponse }
     * 
     */
    public GetMeanResponse createGetMeanResponse() {
        return new GetMeanResponse();
    }

    /**
     * Create an instance of {@link MalformedURLException }
     * 
     */
    public MalformedURLException createMalformedURLException() {
        return new MalformedURLException();
    }

    /**
     * Create an instance of {@link GetMean }
     * 
     */
    public GetMean createGetMean() {
        return new GetMean();
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMeanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://kbbi.ir.cs/", name = "getMeanResponse")
    public JAXBElement<GetMeanResponse> createGetMeanResponse(GetMeanResponse value) {
        return new JAXBElement<GetMeanResponse>(_GetMeanResponse_QNAME, GetMeanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MalformedURLException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://kbbi.ir.cs/", name = "MalformedURLException")
    public JAXBElement<MalformedURLException> createMalformedURLException(MalformedURLException value) {
        return new JAXBElement<MalformedURLException>(_MalformedURLException_QNAME, MalformedURLException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://kbbi.ir.cs/", name = "getMean")
    public JAXBElement<GetMean> createGetMean(GetMean value) {
        return new JAXBElement<GetMean>(_GetMean_QNAME, GetMean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://kbbi.ir.cs/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

}
