package com.naufal.senti.crawler;
import com.naufal.senti.commons.database.Mysql;
import com.naufal.senti.commons.util.RegexUtils;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
/**
 * @author @ahmadluky
 */
public class NEntityRecognition {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NEntityRecognition.class);
    Mysql mysql ;
    Map<String, String> m_contextual;
    public static List<String> ListLink;
    public NEntityRecognition() throws SQLException, ClassNotFoundException{
        NEntityRecognition.ListLink = new ArrayList<String>();
        mysql = new Mysql();
        m_contextual = mysql.contextual();
    }
    public List<String> tokenZing(String artikel){
        artikel = artikel.replace(".", "");
        List<String> tokens = new ArrayList<>();
        Matcher m = RegexUtils.TOKENIZER_PATTERN.matcher(artikel);
        while (m.find())
            tokens.add(m.group());
        return tokens;
    }
    public void Entity(Map<Integer, List<String>> artikel){
        for (int i = 0; i < artikel.size(); i++) {
            List<String> row = artikel.get(i);
            // setiap kata dlm artikel
            for (int j = 0; j < row.size(); j++) {
                // contextual value
                String str = row.get(j);
                
                // # RULE 1 #
                // IF Token[i].LL and Token[i+1].TitleCase 
                // THEN Token[i+1].NE = "Token[i+1] (NAMA ORANG)"
                // # RULE 2 #
                // IF Token[i].PO and Token[i+1].TitleCase 
                // THEN Token[i+1].NE = "Token[i+1] (NAMA ORANG)"
                // # RULE 3 #
                // IF Token[i].PP and Token[i+1].TitleCase 
                // THEN Token[i+1].NE = "Token[i+1] (NAMA ORANG)"
                // # RULE 4 #
                // IF Token[i].PS and Token[i-1].TitleCase 
                // THEN Token[i+1].NE = "Token[i+1] (NAMA ORANG)"
                
                if( m_contextual.containsKey(str.toLowerCase()) ){
                    String str_context = m_contextual.get(str.toLowerCase()); // string to lowercase word
                    if ("ll".equals(str_context) && Morphological.TitleCase(str)) {
                        LOG.info(row.get(j+1));
                    }
                    if ("po".equals(str_context) && Morphological.TitleCase(str)) {
                        LOG.info(row.get(j+1));
                    }
                    if ("pp".equals(str_context) && Morphological.TitleCase(str)) {
                        LOG.info(row.get(j+1));
                    }
                    if ("ps".equals(str_context) && Morphological.TitleCase(str)) {
                        LOG.info(row.get(j+1));
                    }
                }
                // END
            }
        }
    }
    public Map<Integer, List<String>> getArticleTokenizing(List<String> artikelList) throws SQLException{
        int key=0;
        Map<Integer, List<String>> articleToken = new HashMap<>();
        for (int i = 0; i < artikelList.size(); i++) {
            List<String> dat = tokenZing(artikelList.get(i));
            articleToken.put(key, dat);
            key++;
        }
        return articleToken;
    }
    @SuppressWarnings("static-access")
	public List<String> getArtikelDB() throws SQLException{
        List<String> articleList = new ArrayList<>();
        String articleHTML = "SELECT * FROM html_data";
        ResultSet rs = mysql.sql(articleHTML);
        while(rs.next())
        {
            String article = rs.getString("data");
            articleList.add(article);
        }  
        return articleList;
    }
    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException, SAXException, ParserConfigurationException{  
        NEntityRecognition ner             = new NEntityRecognition();
        List<String> article = ner.getArtikelDB();
        Map<Integer, List<String>> token    = ner.getArticleTokenizing(article);
        ner.Entity(token);
    }

}
