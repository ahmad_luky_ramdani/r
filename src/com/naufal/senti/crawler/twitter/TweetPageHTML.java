package com.naufal.senti.crawler.twitter;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.naufal.senti.commons.database.Mongodb;
import com.naufal.senti.commons.util.io.FileUtils;
import com.naufal.senti.commons.util.io.IOUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.LoggerFactory;

import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 * @author ahmadluky
 */
public class TweetPageHTML {
    
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetPageHTML.class);
    public TweetPageHTML(){}
    
    public static void GetReply(String tweetID, String url) throws IOException, InterruptedException{
        //using JSOUP
        Document doc = Jsoup.connect(url).get();
        Elements replys = doc.select("p.js-tweet-text"); // define attribute
        Elements at = doc.select("span.username.js-action-profile-name b");
        Elements link_reply = doc.select("small.time a");
        
        int i=0;
        String parentID = null;
        
        for(Element reply:replys){
            String[] link =link_reply.get(i).attr("href").split("/");
            if (i==0) {
                FileUtils.writeClassPlarity(tweetID + "//" + at.get(i).text() + "//" + reply.text(), "./resources/datasets/2016/logTwitter/data_latih_id_tweet_ALL-8.log");
                parentID=tweetID;
            }else{
                FileUtils.writeClassPlarity(parentID+"||"+link[3] + "//" + at.get(i).text() + "//" + reply.text(), "./resources/datasets/2016/logTwitter/data_latih_id_tweet_reply_ALL-8.log");
            }
            LOG.info("Reply \t :" + link[3] + "||" + at.get(i).text()+" --> "+"//"+reply.text());
            i++;
        }
    }
    /**
     * Main Function Test
     * @param args
     * @throws java.net.UnknownHostException
     * @throws twitter4j.JSONException
     */
    public static void main(String[] args) throws UnknownHostException, JSONException, IOException, InterruptedException {  
        
        boolean file = true;
        if (file) {
            String file_name = "./resources/datasets/2016/tweetreply.csv";
            InputStreamReader isr = null;
            BufferedReader br = null;
            try {
                isr = new InputStreamReader(IOUtils.getInputStream(file_name), "UTF-8");
                br = new BufferedReader(isr);
                String line = "";
                while ((line = br.readLine()) != null) {
                    if (line.trim().length() == 0) {
                        continue;
                    }
                    String[] values = line.split(";", 2);
                    String url = "https://twitter.com/"+values[1]+"/status/"+values[0]+"";
                    if (true) {
                        LOG.info("Get URL \t: '"+url+"'");
                    }
                    GetReply(values[0], url);
                }
            } catch (IOException e) {
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException ignore) {}
                }
                if (isr != null) {
                    try {
                        isr.close();
                    } catch (IOException ignore) {}
                }
            }
        } else {
            Mongodb  conect = new Mongodb();
            conect.SelectDb("research");
            DBCursor rst = conect.findAll("data_ahok_20150813");
            while(rst.hasNext()) {
                
                DBObject dbObject = rst.next();
                String jsonString = JSON.serialize(dbObject);       
                JSONObject jsonObject = new JSONObject(jsonString);
                
                if(!jsonObject.isNull("id_str")){
                    String id = (String) jsonObject.get("id_str");
                    JSONObject users = jsonObject.getJSONObject("user");
                    String id_user = users.getString("id_str");
                    String url = "https://twitter.com/"+id_user+"/status/"+id;
                    GetReply(id, url);
                }
            }
        }
    }
}
