package com.naufal.senti.crawler.twitter;

import com.naufal.senti.commons.Configuration;

import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.naufal.senti.commons.util.io.FileUtils;
import java.io.IOException;
/**
 * 
 * @author ahmadluky
 *
 */
public class TweetStream {
    /**
     * get data streaming from twitter data
     */
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetStream.class);
    @SuppressWarnings("rawtypes")
	public TweetStream() throws ClassNotFoundException, SQLException{
        String TWITTER_CONSUMER_KEY = null;
        String TWITTER_CONSUMER_SECRET = null;
        String OAUTH_TOKEN = null;
        String OAUTH_SECRET = null;
        List<Map> confTwitter = Configuration.getTwitterResource();
        for (Map conf : confTwitter) {
            TWITTER_CONSUMER_KEY = (String) conf.get("TWITTER_CONSUMER_KEY");
            TWITTER_CONSUMER_SECRET = (String) conf.get("TWITTER_CONSUMER_SECRET");
            OAUTH_TOKEN = (String) conf.get("OAUTH_TOKEN");
            OAUTH_SECRET = (String) conf.get("OAUTH_SECRET");
        }
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true);
        cb.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
        cb.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
        cb.setOAuthAccessToken(OAUTH_TOKEN);
        cb.setOAuthAccessTokenSecret(OAUTH_SECRET);
        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        
        StatusListener listener = new StatusListener() {
            @Override
            public void onException(Exception arg0) {}
            @Override
            public void onDeletionNotice(StatusDeletionNotice arg0) {}
            @Override
            public void onScrubGeo(long arg0, long arg1) {}
            @Override
            public void onStatus(Status status) {
                if (status.getText().toLowerCase().contains(":)") || 
                    status.getText().toLowerCase().contains(":-)") ||
                    status.getText().toLowerCase().contains(": )") ||
                    status.getText().toLowerCase().contains(":D") ||
                    status.getText().toLowerCase().contains("=)") ||
                    status.getText().toLowerCase().contains(":))")) 
                    {
                        //Positive
                        User user = status.getUser();
                        Date date = status.getCreatedAt();
                        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String createDate = formatter.format(date);
                        
                        String username = status.getUser().getScreenName();
                        String profileLocation = user.getLocation();
                        long tweetId = status.getId(); 
                        String content = status.getText();
                        boolean is_retweet= status.isRetweet();
                        String is_reply = status.getInReplyToScreenName();
                        
                        String data = ""+username+"|"+createDate+"|"+profileLocation+"|"+tweetId+"|"+content+"";
                        if (is_retweet || is_reply != null) {
                        	//String data
    	                    try {
    	                        FileUtils.writeClassPlarity(data, "./resources/datasets/2016/dataUji/PositifRTRPY-jk.log");
    	                    } catch (IOException ex) {
    	                        Logger.getLogger(TweetStream.class.getName()).log(Level.SEVERE, null, ex);
    	                    }
						} else {
							//String data
		                    try {
		                        FileUtils.writeClassPlarity(data, "./resources/datasets/2016/dataUji/PositifTWEET-jk.log");
		                    } catch (IOException ex) {
		                        Logger.getLogger(TweetStream.class.getName()).log(Level.SEVERE, null, ex);
		                    }
						}
                        //log
                        LOG.info(createDate);
                        LOG.info(username);
                        LOG.info(profileLocation);
                        LOG.info(Long.toString(tweetId));
                        LOG.info(content +"\n");
                        //save
                        /**String sql = "INSERT INTO data_train_dummy ("
                                + "`username`,`create_date`,`profileLocation`,`tweetId`,`text`, `other`) "
                                + "VALUES ("
                                + "'"+username+"','"+createDate+"','"+profileLocation+"','"+tweetId+"','"+content+"','')";**/
                        
                        
                    } else if (status.getText().toLowerCase().contains(":(") || 
                    status.getText().toLowerCase().contains(":-(") ||
                    status.getText().toLowerCase().contains(": (") ||
                    status.getText().toLowerCase().contains(":((") ||
                    status.getText().toLowerCase().contains("=(") ||
                    status.getText().toLowerCase().contains(":P")) 
                    {
                        //Negative
                        User user = status.getUser();
                        Date date = status.getCreatedAt();
                        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String createDate = formatter.format(date);
                        String username = status.getUser().getScreenName();
                        String profileLocation = user.getLocation();
                        long tweetId = status.getId(); 
                        String content = status.getText();
                        boolean is_retweet= status.isRetweet();
                        String is_reply = status.getInReplyToScreenName();

                        String data = ""+username+"|"+createDate+"|"+profileLocation+"|"+tweetId+"|"+content+"";
                        if (is_retweet || is_reply != null) {
                        	//String data
    	                    try {
    	                        FileUtils.writeClassPlarity(data, "./resources/datasets/2016/dataUji/NegatifRTRPY-jk-0.log");
    	                    } catch (IOException ex) {
    	                        Logger.getLogger(TweetStream.class.getName()).log(Level.SEVERE, null, ex);
    	                    }
						} else {
							//String data
		                    try {
		                        FileUtils.writeClassPlarity(data, "./resources/datasets/2016/dataUji/NegatifTWEET-jk-0.log");
		                    } catch (IOException ex) {
		                        Logger.getLogger(TweetStream.class.getName()).log(Level.SEVERE, null, ex);
		                    }
						}
                        //log
                        LOG.info(createDate);
                        LOG.info(username);
                        LOG.info(profileLocation);
                        LOG.info(Long.toString(tweetId));
                        LOG.info(content +"\n");
                        //save
                        /**String sql = "INSERT INTO data_train_dummy ("
                                + "`username`,`create_date`,`profileLocation`,`tweetId`,`text`, `other`) "
                                + "VALUES ("
                                + "'"+username+"','"+createDate+"','"+profileLocation+"','"+tweetId+"','"+content+"','')";**/
                        

                    }else {
                         //other
                        User user = status.getUser();
                        Date date = status.getCreatedAt();
                        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String createDate = formatter.format(date);
                        String username = status.getUser().getScreenName();
                        String profileLocation = user.getLocation();
                        long tweetId = status.getId(); 
                        String content = status.getText();
                        boolean is_retweet= status.isRetweet();
                        String is_reply = status.getInReplyToScreenName();
                        

                        String data = ""+username+"|"+createDate+"|"+profileLocation+"|"+tweetId+"|"+content+"";
                        if (is_retweet || (is_reply != null) ) {
                        	//String data
    	                    try {
    	                        FileUtils.writeClassPlarity(data, "./resources/datasets/2016/dataUji/NetRTRPY-jk-0.log");
    	                    } catch (IOException ex) {
    	                        Logger.getLogger(TweetStream.class.getName()).log(Level.SEVERE, null, ex);
    	                    }
						} else {
							//String data
		                    try {
		                        FileUtils.writeClassPlarity(data, "./resources/datasets/2016/dataUji/NetTWEET-jk-0.log");
		                    } catch (IOException ex) {
		                        Logger.getLogger(TweetStream.class.getName()).log(Level.SEVERE, null, ex);
		                    }
						}
                        //log
                        LOG.info(createDate);
                        LOG.info(username);
                        LOG.info(profileLocation);
                        LOG.info(Long.toString(tweetId));
                        LOG.info(content +"\n");
                        //save
                        /**String sql = "INSERT INTO data_train_dummy ("
                                + "`username`,`create_date`,`profileLocation`,`tweetId`,`text`, `other`) "
                                + "VALUES ("
                                + "'"+username+"','"+createDate+"','"+profileLocation+"','"+tweetId+"','"+content+"','')";**/
                        
                    }
                
            }
            @Override
            public void onTrackLimitationNotice(int arg0) {}
            @Override
            public void onStallWarning(StallWarning sw) {
                throw new UnsupportedOperationException("Not supported yet."); 
            }
        };
        
        FilterQuery fq = new FilterQuery();
        String keywords[] = {"pak_jk"}; //example key
        fq.track(keywords);
        fq.language(new String[]{"in"});
        twitterStream.addListener(listener);
        twitterStream.filter(fq);  
    }
    
    /**
     * @param args
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    @SuppressWarnings("unused")
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
        TweetStream twitter   = new TweetStream();
    }
}
