package com.naufal.senti.crawler.twitter;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.naufal.senti.commons.database.Mongodb;

import java.net.UnknownHostException;

import org.slf4j.LoggerFactory;

import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 * @author ahmadluky
 */
public class GetTweetReply {
    
    @SuppressWarnings("unused")
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(GetTweetReply.class);
    public GetTweetReply(){}
    public static void GetReply(String url){}
    /**
     * Main Function Test
     * @param args
     * @throws java.net.UnknownHostException
     * @throws twitter4j.JSONException
     */
    public static void main(String[] args) throws UnknownHostException, JSONException {  
        Mongodb  conect = new Mongodb();
        conect.SelectDb("research");
        DBCursor rst = conect.findAll("data_ahok_20150813");
        while(rst.hasNext()) {
            DBObject dbObject = rst.next();
            String jsonString = JSON.serialize(dbObject);       
            JSONObject jsonObject = new JSONObject(jsonString);
            if(jsonObject.isNull("id_str")){
                continue;
            }else{
                String id = (String) jsonObject.get("id_str");
                JSONObject users = jsonObject.getJSONObject("user");
                String id_user = users.getString("id_str");
                String url = "https://twitter.com/"+id_user+"/status/"+id;
                GetReply(url);
            }
    	}
    }
}
