package com.naufal.senti.crawler.twitter;

import com.naufal.senti.commons.Configuration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

public class TweetReply {
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(TweetReply.class);
    private static String TWITTER_CONSUMER_KEY = null;
    private static String TWITTER_CONSUMER_SECRET = null;
    private static String OAUTH_TOKEN = null;
    private static String OAUTH_SECRET = null;
    private final Twitter twitter; 
    @SuppressWarnings("rawtypes")
	private final List<Map> confMysql;
    @SuppressWarnings("rawtypes")
	public TweetReply(){
        LOG.info("Load configuration connection .....");
        confMysql = Configuration.getTwitterResource();
        for(Map conf : confMysql){
            TWITTER_CONSUMER_KEY = (String) conf.get("TWITTER_CONSUMER_KEY");
            TWITTER_CONSUMER_SECRET = (String) conf.get("TWITTER_CONSUMER_SECRET");
            OAUTH_TOKEN = (String) conf.get("OAUTH_TOKEN");
            OAUTH_SECRET = (String) conf.get("OAUTH_SECRET");
        }
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setJSONStoreEnabled(true)
          .setOAuthConsumerKey(TWITTER_CONSUMER_KEY)
          .setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET)
          .setOAuthAccessToken(OAUTH_TOKEN)
          .setOAuthAccessTokenSecret(OAUTH_SECRET); 
        TwitterFactory tf   = new TwitterFactory(cb.build());
        twitter             = tf.getInstance();
    }
    public void searchReply(String id_strTweet){
    	Query query = new Query(id_strTweet);
        List<Status> tweets = new ArrayList<>();
        try {
            QueryResult result;
            do {
                result = twitter.search(query);
                for (Status tweet : result.getTweets()) {
                    if (tweet.getInReplyToStatusId() > 0) {
                        tweets.add(tweet);
                    }
                }               
            } while ((query = result.nextQuery()) != null);
        } catch (TwitterException te) {
            System.out.println("Failed to search tweets: " + te.getMessage());
        }
        System.out.println(tweets);
    }
    public static void main(String[] args){
        String ID_STR= "716974507722825728";
        TweetReply replay  = new TweetReply();
        replay.searchReply(ID_STR);
    }
}
