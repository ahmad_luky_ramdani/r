package com.naufal.senti.crawler.twitter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import org.slf4j.LoggerFactory;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.database.Mysql;
import com.naufal.senti.commons.util.io.FileUtils;
import com.naufal.senti.commons.util.io.IOUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

public class TweetData {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TweetData.class);
    private static String TWITTER_CONSUMER_KEY = null;
    private static String TWITTER_CONSUMER_SECRET = null;
    private static String OAUTH_TOKEN = null;
    private static String OAUTH_SECRET = null;
    private final Twitter twitter; 
    @SuppressWarnings("rawtypes")
	private final List<Map> confMysql;
    private final Mysql mysql=null;
    
    @SuppressWarnings("rawtypes")
	public TweetData() throws ClassNotFoundException, SQLException{
    	// this.mysql = new Mysql();
        LOG.info("Load configuration connection .....");
        confMysql = Configuration.getTwitterResource();
        for(Map conf : confMysql){
            TWITTER_CONSUMER_KEY = (String) conf.get("TWITTER_CONSUMER_KEY");
            TWITTER_CONSUMER_SECRET = (String) conf.get("TWITTER_CONSUMER_SECRET");
            OAUTH_TOKEN = (String) conf.get("OAUTH_TOKEN");
            OAUTH_SECRET = (String) conf.get("OAUTH_SECRET");
        }
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setJSONStoreEnabled(true)
          .setOAuthConsumerKey(TWITTER_CONSUMER_KEY)
          .setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET)
          .setOAuthAccessToken(OAUTH_TOKEN)
          .setOAuthAccessTokenSecret(OAUTH_SECRET); 
        TwitterFactory tf = new TwitterFactory(cb.build());
        twitter = tf.getInstance();
    }
    
    public ArrayList<Status> datasearch(String str_query){
        try {
            Query query = new Query(str_query);
            query.setSince("2016-01-01");
            query.setUntil("2016-01-07");
            query.setLang("id");
            
            int numberOfTweets = 200;
            long lastID = Long.MAX_VALUE;
            ArrayList<Status> tweetL = new ArrayList<>();
            QueryResult result;
            while(tweetL.size () < numberOfTweets){
                if (numberOfTweets - tweetL.size() > 100)
                    query.setCount(100);
                else 
                    query.setCount(numberOfTweets - tweetL.size());
            
                result 	= twitter.search(query);
                tweetL.addAll(result.getTweets());
                
                for (Status t: tweetL) 
                    if(t.getId() < lastID) lastID = t.getId();
                query.setMaxId(lastID-1);
            }
            return tweetL;
        } catch (TwitterException te) {
            if (te.getStatusCode() == 429) {}
            System.out.println("Failed to search tweets: " + te.getMessage());
            return null;
        }
    }
    
    public void datatimeline(String user) throws IOException{
        try {
            List<Status> statuses;
            Paging paging   = new Paging(2, 100);
            statuses = twitter.getUserTimeline(user,paging);
            System.out.println("Showing @" + user + "'s user timeline. Count :"+ statuses.size());
            for (Status status : statuses) {
                System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());
                if (status.getLang().equals("in")) {
                    LOG.info(status.getLang());
                    LOG.info(String.valueOf(status.getId()));
                    LOG.info(status.getUser().getScreenName());
                    LOG.info(status.getText().replaceAll("\n", ""));
                    LOG.info(status.getCreatedAt().toString());
                    LOG.info(status.getGeoLocation().toString());    
                }
            }
        } catch (TwitterException te) {
            System.out.println("Failed to get timeline: " + te.getMessage());
            System.exit(-1);
        }
    }
    public void InfoUser (String at_user) throws IOException{
        try {
            User user = twitter.showUser(at_user); 
            if (user.getStatus() != null) {
                System.out.println("@" + user.getScreenName() + " - " + user.getDescription() +
                                   "\n Location : " + user.getLocation() +
                                   "\n Lang :" + user.getLang()  +
                                   "\n Flolower :" + user.getFollowersCount() +
                                   "\n Following :" + user.getFriendsCount() +
                                   "\n Image :"   + user.getBiggerProfileImageURLHttps()
                                    );
            } else {
                System.out.println("@" + user.getScreenName());
            }
        } catch (TwitterException te) {
            System.out.println("Failed to get Info User: " + te.getMessage());
            System.exit(-1);
        }
    }
    
    public void OnceTweet(String tweetID) throws IOException{
        System.out.println(tweetID);
        try {
            Status status = twitter.showStatus(Long.parseLong(tweetID));
            if (status == null) {
                System.out.println("Status Tweet Not Found");
            } else {
                String data = ""+ tweetID +"|"
                                + status.getCreatedAt() +"|"
                                + status.getUser().getScreenName() +" | "
                                + status.getText() + " | " 
                                + status.getFavoriteCount() +" | "
                                + status.getRetweetCount() +" | "
                                + status.getUser().getMiniProfileImageURL() +" | "
                                + status.getUser().getFollowersCount() + " | " 
                                + status.getUser().getFriendsCount() + " | " 
                                + status.getUser().getListedCount() + " | " 
                                +"http://www.twitter.com/"+status.getUser().getScreenName()+"/status/"+tweetID+"";
                System.out.println(data);
                
                //if bukan reply ada -1
                if (status.getInReplyToStatusId()== -1) {
                   FileUtils.writeClassPlarity(tweetID, "./resources/datasets/2016/logTwitter/data_latih_id_tweet_18.log");
                   //FileUtils.writeClassPlarity(data, "./resources/datasets/2016/logTwitter/tweet_tweet.log");
                } else {
                   FileUtils.writeClassPlarity(tweetID, "./resources/datasets/2016/logTwitter/data_latih_id_tweet_reply_18.log");
                   //FileUtils.writeClassPlarity(data, "./resources/datasets/2016/logTwitter/tweet_tweetreplay.log");
                }
            }
        } catch (TwitterException e) {
            System.err.print("Failed to search tweets: " + e.getMessage());
        }
    }
    
    public void saveResult(List<Status> tweets){
        for (Status tweet : tweets) {
            LOG.info(tweet.getText());
            Date date = tweet.getCreatedAt();
            // gets Username
            User user = tweet.getUser();
            // change format date
            Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String createDate = formatter.format(date);
            String username = tweet.getUser().getScreenName();
            String profileLocation = user.getLocation();
            long tweetId = tweet.getId(); 
            String content = tweet.getText();
            System.out.println(createDate);
            System.out.println(username);
            System.out.println(profileLocation);
            System.out.println(tweetId);
            System.out.println(content +"\n");
            String sql = "INSERT INTO data_train_dummy (`username`,`create_date`,`profileLocation`,"
                        + "`tweetId`,`text`, "
                        + "`other`) VALUES ('"+username+"','"+createDate+"','"+profileLocation+"',"
                        + "'"+tweetId+"',"
                        + "'"+content+"',"
                        + "'')";
            try {
                mysql.sqlInsert(sql);
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(TweetStream.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Main Test
     * @param args
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException, SQLException {
        boolean file = true;
        TweetData curentSearch      = new TweetData();
        if(file){
            String file_name = "./resources/datasets/2016/data_latih_id.txt";
            InputStreamReader isr = null;
            BufferedReader br = null;
            try {
                isr = new InputStreamReader(IOUtils.getInputStream(file_name), "UTF-8");
                br = new BufferedReader(isr);
                String line = "";
                int counter = 1;
                while ((line = br.readLine()) != null) {
                    if (line.trim().length() == 0) {
                        continue;
                    }
                    if (counter%180==0) {
                        Thread.sleep(60000);
                    }
                    //String[] values = line.split(";");
                    curentSearch.OnceTweet(line);
                    counter++;
                }
            } catch (IOException e) {
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException ignore) {}
                }
                if (isr != null) {
                    try {
                        isr.close();
                    } catch (IOException ignore) {}
                }
            }
        }else{
            //id sample
            String id_tweet ="710281844944556000";
            curentSearch.OnceTweet(id_tweet);
        }
        
//        ArrayList<Status> rst = curentSearch.datasearch("jokowi, :("); //contoh keyword
//        curentSearch.saveResult(rst);
    }

}
