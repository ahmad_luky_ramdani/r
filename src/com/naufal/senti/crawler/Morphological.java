package com.naufal.senti.crawler;
/**
 * @author ahmadluky
 */
public class Morphological {
    public static char ch1;
    public static char ch2;
    /**
     * TitleCase 
     * @param token
     * @return 
     */
    public static boolean TitleCase (String token){
        ch1 = token.charAt(0);
        ch2 = token.charAt(1);
        return Character.isUpperCase(ch1) && Character.isLowerCase(ch2);
    }
}
