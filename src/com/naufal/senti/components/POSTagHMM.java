package com.naufal.senti.components;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import NLP_ITB.POSTagger.HMM.Decoder.MainTagger;
import NLP_ITB.POSTagger.HMM.Train.MainTrainer;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.Dataset;
import com.naufal.senti.commons.Tweet;
import com.naufal.senti.commons.util.io.FileUtils;
import com.naufal.senti.commons.wordnet.POSTag;
import com.naufal.senti.cs.ir.kbbi.IOException_Exception;
import com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception;
import com.naufal.senti.opinionFilter.Rule;

import java.io.IOException;
/**
 * @author ahmadluky
 * POSTaging using HMM  by research http://mail.informatika.org/~ayu/2010postagger.pdf
 */
public class POSTagHMM {
    private int lm =0;
    private int affix=0;
    private int pass2=0;
    private int lex=0;
    private String lexicon_trn=null;
    private String ngram_trn=null;
    private String corpus_trn=null;
    MainTagger mt;
    private static final Logger LOG = LoggerFactory.getLogger(POSTagHMM.class);
    private static final POSTagHMM INSTANCE = new POSTagHMM();
    public String data_tweet;
    
    @SuppressWarnings("rawtypes")
	public POSTagHMM(){
        List<Map> postag = Configuration.getPOSTag(); 
        for(Map postagEntry: postag){
        	this.lexicon_trn = (String) postagEntry.get("lexicon_trn");
            this.ngram_trn = (String) postagEntry.get("ngram_trn");
            this.corpus_trn = (String) postagEntry.get("corpus_trn");
            this.lm = (int) postagEntry.get("lm");
            this.affix = (int) postagEntry.get("affix");
            this.pass2 = (int) postagEntry.get("pass2");
            this.lex = (int) postagEntry.get("lex");
        }
        // training
        MainTrainer.Train(this.corpus_trn);
        // set from config best of parameter
        mt = new MainTagger(this.lexicon_trn, this.ngram_trn, this.lm, 3, 3, 0, this.affix, false, 0.2, this.pass2, 500.0, this.lex);
    }
    public static POSTagHMM getInstance() {
        return INSTANCE;
    }
    public List<String> tag(String tweet){
        try {
            return mt.taggingFile(tweet);
        } catch (Exception e) {
            return null;
        }
    }	
    public List<List<String>> tagTweets(List<List<String>> tweets) throws IOException {
        List<List<String>> taggedTweets = new ArrayList<>();
        for (List<String> tweet : tweets) {
          taggedTweets.add(tag(tweet));
        }
        return taggedTweets;
    }
    public List<String> tag(List<String> tweet) throws IOException{
        List<String> mtTag = new ArrayList<>();
        for (String t : tweet) {
            // convert Tagging
            String[] val = mt.taggingFile(t).get(0).split("/");
            String tag = POSTag.convertHMM(val[1]);
            mtTag.add(val[0]+"/"+tag);
            //mtTag.add(mt.taggingFile(t).get(0));
        }
        return mtTag;
    }	
    public List<String> getResultConverTag(List<String> str_tag){
        List<String> tagSWN = new ArrayList<>();
        for (int i = 0; i < str_tag.size(); i++){
            String[] val = str_tag.get(i).split("/");
            String tag = POSTag.convertHMM(val[1]);
            tagSWN.add(val[0]+"/"+tag);
        }
        return tagSWN;
    }
    public static void main (String[] args) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException, SQLException, ClassNotFoundException {

        long startTime = System.currentTimeMillis();
        //Data Testing
    	//List<Tweet> tweets = Tweet.getTestTweets();
    	//Data Taining
        //Dataset dataSet = Configuration.getDataSet();
        //List<Tweet> tweets = dataSet.getTrainTweets();
        //Data From DB mysql
        Dataset dataSet = Configuration.getDataSet();
        List<Tweet> tweets = dataSet.getTrainTweetsDB("data_json");
        LOG.info("Read train tweets from " + dataSet.getTrainDataFile());
        Dataset.printTweetStats(tweets);
        POSTagHMM posTagHMM = POSTagHMM.getInstance();
        // process tweets
        int opinion_count = 0;
        int non_opinion_count = 0;
        for (Tweet tweet : tweets) {
            // Tokenize
            List<String> tokens = Tokenizer.tokenize(tweet.getText());
            // Preprocess
            List<String> preprocessedTokens = PreprocessorBahasa.preprocess(tokens);
            // POS Tagging
            List<String> taggedTokens = posTagHMM.tag(preprocessedTokens);
            // Filter Opinion
            String r_sentence = Rule.getPosTaggingSentence(taggedTokens);
            boolean is_opinion = Rule.isOpinion(r_sentence);
            if (is_opinion) {
				opinion_count++;
				FileUtils.writeClassPlarity(""+tweet.getId()+";"+tweet.getText()+"","resources/datasets/2016/opinion/data-tabel-data_json.log");
			} else {
				non_opinion_count++;
				FileUtils.writeClassPlarity(""+tweet.getId()+";"+tweet.getText()+"","resources/datasets/2016/non-opinion/data-tabel-data_json.log");
			}
            LOG.info("Tweet: '" + tweet + "'");
            LOG.info("Token: '" + tokens + "'");
            LOG.info("Preproses: '" + preprocessedTokens + "'");
            LOG.info("TaggingTweet: " + taggedTokens);
            LOG.info("is Opinion :" + is_opinion);
            LOG.info("Opinion :" + opinion_count);
            LOG.info("Non_opinion :" + non_opinion_count);
            
        }
        long elapsedTime = System.currentTimeMillis() - startTime;
        LOG.info("POSTagger finished after " + elapsedTime + " ms");
        LOG.info("Total tweets: " + tweets.size());
        LOG.info((elapsedTime / (double) tweets.size()) + " ms per Tweet");
    }
}
