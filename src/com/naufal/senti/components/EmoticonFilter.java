package com.naufal.senti.components;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.naufal.senti.commons.database.Mongodb;
import java.net.UnknownHostException;
import java.util.Set;
import org.slf4j.LoggerFactory;
import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 *
 * @author ahmadluky
 */
public class EmoticonFilter {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(Mongodb.class);
    public static boolean isRetweet(String tweet){
        String[] arr = tweet.split(" ");
        return arr[0].equals("RT");
    }
    public static void main(String[] args) throws UnknownHostException, JSONException {
        Mongodb  conect = new Mongodb();
        conect.SelectDb("research");
        conect.GetCollectionALL();
        Set<String> coll = conect.GetCollectionALLColl();
        for (String collName : coll) {
            DBCursor rst = conect.findQueryRegex(collName,
            									"text",
            									"\\s+(\\:\\)|\\:\\(|<3|\\:\\/|\\:-\\/|\\:\\||\\:p)\\s");
            while(rst.hasNext()) {
                DBObject dbObject = rst.next();
                String jsonString = JSON.serialize(dbObject);       
                JSONObject jsonObject = new JSONObject(jsonString);
                if(!jsonObject.isNull("id_str")){
                    if(!jsonObject.isNull("in_reply_to_status_id")){
                        Object in_reply_to_status_id = jsonObject.get("in_reply_to_status_id");
                        Long id_tweet = jsonObject.getLong("id");
                        String text = (String) jsonObject.get("text");
                        String lang = (String) jsonObject.get("lang");
                        if (in_reply_to_status_id.toString().equals("null") && lang.equals("in")) {  // bukan merupakan reply
                            if(!isRetweet(text)){
                                // get attribute tweet
                                String created_at = (String) jsonObject.get("created_at");
                                String id_tweet_str = (String) jsonObject.get("id_str");
                                int retweet_count_user = jsonObject.getInt("retweet_count");
                                int favorite_count_user = jsonObject.getInt("favorite_count");
                                // user
                                JSONObject users = jsonObject.getJSONObject("user");
                                Long id_user = users.getLong("id");
                                String id_str_user = users.getString("id_str");
                                String screen_name_user = users.getString("screen_name");
                                int followers_count_user = users.getInt("followers_count");
                                int friends_count_user = users.getInt("friends_count");
                                int listed_count_user = users.getInt("listed_count");
                                int favourites_count_user = users.getInt("favourites_count");
                                int statuses_count_user = users.getInt("statuses_count");
                                String profile_image_url = users.getString("profile_image_url");
                                // document insert
                                BasicDBObject doc = new BasicDBObject("created_at", created_at).
	                                append("id", id_tweet).
	                                append("id_str", id_tweet_str).
	                                append("text", text).
	                                append("retweet_count_user", retweet_count_user).
	                                append("favorite_count_user", favorite_count_user).
	                                append("id_user", id_user).
	                                append("id_str_user", id_str_user).
	                                append("screen_name_user", screen_name_user).
	                                append("followers_count_user", followers_count_user).
	                                append("friends_count_user", friends_count_user).
	                                append("listed_count_user", listed_count_user).
	                                append("favourites_count_user", favourites_count_user).
	                                append("statuses_count_user", statuses_count_user).
	                                append("profile_image_url", profile_image_url);
                                conect.insert(collName, doc);
                                LOG.info(collName+": yes");
                            }else{
                                LOG.info("no");
                            }
                        }
                    }else{
                        continue;
                    }
                }else{
                    continue;
                }
            }
        }
    }
}
