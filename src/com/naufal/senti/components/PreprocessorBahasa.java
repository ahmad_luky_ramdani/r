package com.naufal.senti.components;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.Tweet;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.naufal.senti.commons.dict.SlangCorrection;
import com.naufal.senti.commons.dict.StopWords;
import com.naufal.senti.commons.dict.Symbol;
import com.naufal.senti.commons.util.RegexUtils;
import com.naufal.senti.commons.util.StringUtils;
import com.naufal.senti.cs.ir.kbbi.IOException_Exception;
import com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception;
import com.naufal.senti.service.ServiceKbbi;
import java.io.IOException;
/**
 * 
 * @author ahmadluky
 *
 */
public class PreprocessorBahasa {
    private static final Logger LOG = LoggerFactory.getLogger(PreprocessorBahasa.class);
    private static final PreprocessorBahasa INSTANCE = new PreprocessorBahasa();
    private static final boolean EX_KBBI = Configuration.get("commons.kbbi.execution", false);
    private static SlangCorrection m_slangCorrection;
    private static Symbol m_symbolCorrection;
    private static StopWords m_stopword;
    private static ServiceKbbi m_kbbi;
    private PreprocessorBahasa() {
        m_slangCorrection = SlangCorrection.getInstance();
        m_symbolCorrection = Symbol.getInstance();
        m_stopword = StopWords.getInstance();
        m_kbbi = ServiceKbbi.getInstance();
    }
    public static PreprocessorBahasa getInstance() {
    	return INSTANCE;
    }
    public static List<String> preprocess(List<String> tokens) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException {
    	List<String> preprocessedTokens = new ArrayList<>();
    	for (String token : tokens) {
            
            // identify token
            boolean berisiPunctuation = StringUtils.consitsOfPunctuations(token);
            boolean berisiUnderscores = StringUtils.consitsOfUnderscores(token);
            boolean berisiEmoticon = StringUtils.isEmoticon(token);
            boolean berisiURL = StringUtils.isURL(token);
            boolean berisiNumeric = StringUtils.isNumeric(token);
            
            // Unify Emoticons remove repeating chars
            if ((berisiEmoticon) && (!berisiURL) && (!berisiNumeric)) {
                Matcher m = RegexUtils.TWO_OR_MORE_REPEATING_CHARS_PATTERN.matcher(token);
                if (m.find()) {
                      boolean isSpecialEmoticon = m.group(1).equals("^");
                      String reducedToken = m.replaceAll("$1");
                      if (isSpecialEmoticon) { // keep ^^
                          reducedToken += "^";
                      }
                      preprocessedTokens.add(reducedToken);
                      continue;
                }
            } else if ((berisiPunctuation) || (berisiUnderscores)) {
                // If token is no Emoticon then there is no further
                // preprocessing for punctuations or underscores
                // preprocessedTokens.add(token);
                continue;
            }
            
            // identify token further
            boolean tokenIsUser = StringUtils.isUser(token);
            boolean tokenIsHashTag  = StringUtils.isHashTag(token);
            boolean tokenIsEmail = StringUtils.isEmail(token);
            boolean tokenIsPhone = StringUtils.isPhone(token);
            boolean tokenIsSpecialNumeric = StringUtils.isSpecialNumeric(token);
            boolean tokenIsSeparatedNumeric = StringUtils.isSeparatedNumeric(token);
            
            // Slang Correction
            if ((!berisiEmoticon) && (!tokenIsUser) && (!tokenIsHashTag)
                && (!berisiURL) && (!berisiNumeric) && (!tokenIsSpecialNumeric)
                && (!tokenIsSeparatedNumeric) && (!tokenIsEmail) && (!tokenIsPhone)) {
                    String[] slangCorrection = m_slangCorrection.getCorrection(token.toLowerCase());
                    if (slangCorrection != null) {
                        for (String slangCorrection1 : slangCorrection) {
                            if (!m_stopword.isStopWord(slangCorrection1)) {
                                preprocessedTokens.add(slangCorrection1);
                            }
                        }
                        continue;
                    }
            }
            
            // Symbol correction
            token = m_symbolCorrection.getCorrection(token);
            
            
            // Check if there are punctuations between chart
            // e.g., A.K.U
            if ((!berisiEmoticon) && (!tokenIsUser) && (!tokenIsHashTag)
                && (!berisiURL) && (!berisiNumeric) && (!tokenIsSpecialNumeric)
                && (!tokenIsSeparatedNumeric) && (!tokenIsEmail) && (!tokenIsPhone)) {
                    Matcher m = RegexUtils.ALTERNATING_LETTER_DOT_PATTERN.matcher(token);
                    if (m.matches()) {
                        String newToken = token.replaceAll("\\.", "");
                        // check to KBBI using WSDL
                        if (EX_KBBI) {
                            if (m_kbbi.contains(newToken)) {
                                if (!m_stopword.isStopWord(newToken)){
                                    preprocessedTokens.add(newToken);
                                    continue;
                                }
                            }
                        } else {
                            if (!m_stopword.isStopWord(newToken)){
                                preprocessedTokens.add(newToken);
                                continue;
                            }
                        }
                    }
            }
            
            // Step 5) Remove elongations of characters (suuuper)
            // 'lollll' to 'loll' because 'loll' is found in dict
            // TODO 'AHHHHH' to 'AH'
            if ((!berisiEmoticon) && (!tokenIsUser) && (!tokenIsHashTag)
                && (!berisiURL) && (!berisiNumeric) && (!tokenIsSpecialNumeric)
                && (!tokenIsSeparatedNumeric) && (!tokenIsEmail) && (!tokenIsPhone)) {
                    // remove repeating chars
                    token = removeRepeatingChars2(token);
                    // Step 5b) Try Slang Correction again
                    String[] slangCorrection = m_slangCorrection.getCorrection(token.toLowerCase());
                    if (slangCorrection != null) {
                        for (String slangCorrection1 : slangCorrection) {
                            if (!m_stopword.isStopWord(slangCorrection1)) {
                                preprocessedTokens.add(slangCorrection1);
                            }
                        }
                        continue;
                    }
            }
            
            // URI
            if (berisiURL) {
                continue;
            }
            
            // User
            if (tokenIsUser) {
                continue;
            }
            
            // Email
            if (tokenIsEmail) {
                continue;
            }
            
            // Hastag
            if (tokenIsHashTag) {
                continue;
            }
            
            // Check KBBI
            if (EX_KBBI) {
                if (m_kbbi.contains(token) && !berisiEmoticon) {
                    continue;
                }
            }
            
            // default action add token
            preprocessedTokens.add(token);
            
            // delay
            if (EX_KBBI) {
                Thread.sleep(1000);
            }
    	}
        return preprocessedTokens;
    }
    private static String removeRepeatingChars2(String value){
        /* Remove vocal/consonan repeat */
        value 				= value + " ";
        int l_str 			= value.length();
        String result_str 	= "";
        char ch1,ch2;
        for(int i=0; i<l_str-1; i++){
            ch1	= value.charAt(i);
            ch2	= value.charAt(i+1);
            if(ch1!=ch2) {
                result_str = result_str + ch1;
            }
        }
        return result_str;
    } 
    public List<List<String>> preprocessTweets(List<List<String>> tweets) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException {
        List<List<String>> preprocessedTweets = new ArrayList<>();
        for (List<String> tweet : tweets) {
                preprocessedTweets.add(preprocess(tweet));
        }
        return preprocessedTweets;
    }
    /**
     * Main Function For Test praproses
     * @param args
     * @throws java.io.IOException
     * @throws com.naufal.senti.cs.ir.kbbi.IOException_Exception
     * @throws com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception
     * @throws java.lang.InterruptedException
     */
    public static void main(String args[]) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException{
    	// Load tweets
        List<Tweet> tweets = Tweet.getTestTweet();
        for (Tweet tweet : tweets) {
            LOG.info("Kalimat :"+tweet.getText());
            
            //Tokennizing 
            List<String> tokens = Tokenizer.tokenize(tweet.getText());
            
            //Praproses 
            List<String> praproses = PreprocessorBahasa.preprocess(tokens);
            String rst_praproses = StringUtils.listTostring(praproses);
            LOG.info("Kalimat Praporses: "+rst_praproses);
            
            // delay
            Thread.sleep(1000);
        }
    }
}
