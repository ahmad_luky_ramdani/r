package com.naufal.senti.components;

import java.util.ArrayList;
import java.util.List;
import com.naufal.senti.commons.Tweet;
import com.naufal.senti.commons.util.HtmlUtils;
import com.naufal.senti.commons.util.UnicodeUtils;

import static com.naufal.senti.ngram.NGram.getBigrams;
import static com.naufal.senti.ngram.NGram.getTrigrams;
import static com.naufal.senti.ngram.NGram.getUnigrams;
/**
 * 
 * @author ahmadluky
 *
 */
public class Tokenizer {
    public static List<List<String>> tokenizeTweets(List<Tweet> tweets) {
        List<List<String>> tokenizedTweets = new ArrayList<>();
        for(Tweet tweet:tweets){
                tokenizedTweets.add(tokenize(tweet.getText()));
        }
        return tokenizedTweets;
    }
    public static String[] preTokenize(String str){
        // Trim text
        str = str.trim();
        // Replace Unicode symbols \u0000
        if (UnicodeUtils.containsUnicode(str)) {
            str = UnicodeUtils.replaceUnicodeSymbols(str);
        }
        // Replace HTML symbols &#[0-9];
        if (HtmlUtils.containsHtml(str)) {
            str = HtmlUtils.replaceHtmlSymbols(str);
        }
        return str.split(" ");
    }
    public static List<String[]> biTokenize(String str) {
        // Tokenize bigram
        String[] preTokenize = preTokenize(str);
        List<String[]> getBigrams = getBigrams(preTokenize);
        return getBigrams;
    }
    public static List<String[]> triTokenize(String str) {
        // Tokenize trigram
        String[] preTokenize = preTokenize(str);
        List<String[]> getTrigrams = getTrigrams(preTokenize);
        return getTrigrams;
    }
    public static List<String> tokenize(String str) {
        // Tokenize unigram
        String[] preTokenize = preTokenize(str);
        List<String> getUnigrams = getUnigrams(String.join(" ", preTokenize));
        return getUnigrams;
    }
}
