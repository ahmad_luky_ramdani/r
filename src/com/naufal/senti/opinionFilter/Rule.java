package com.naufal.senti.opinionFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @ahmadluky
 */
public class Rule {
    private static final Logger LOG = LoggerFactory .getLogger(Rule.class);
    public static final String r_1 = "a r"; // kata keterangan - kata sifat
    public static final String r_2 = "a v"; // kata keterangan - kata kerja
    public static final String r_3 = "n r"; // kata benda - kata sifat
    public static final String r_4 = "n v"; // kata benda - kata kerja
    public static final String r_5 = "r v"; // kata sifat - kata kerja
    public static final String r_6 = "cc r"; // kata sambung setara - kata sifat
    public static final String r_7 = "r r"; // kata sifat -kata sifat
    public static final String r_8 = "v v"; // Kata kerja -kata sifat 
    public static final String r_9 = "r a"; // Kata sifat - Kata keeterangan
    public static final String r_10 = "v r"; // Kata kerja - Kata sifat
    public static final String r_11 = "neg r"; //Negasi - Kata Sifat
    public static final String r_12 = "neg v"; //Negasi - Kata Kerja
    public static final String r_13 = "prp v"; //Kata Ganti Orang - Kata Kerja Intransitif
    public static final String r_14 = "prp v"; //Kata Ganti Orang - Kata Kerja Transitif
    public static final String r_15 = "v n"; //Kata Kerja Transitif - Kata Benda 
    public static final String r_16 = "m v"; //Modal-Kata Kerja Transitif
    public static final String r_17 = "m v"; //Modal-Kata Kerja Intransitif

    public static final Pattern TOKENIZER_PATTERN = Pattern.compile(r_1 + "|" + 
                                                                    r_2 + "|" + 
                                                                    r_3 + "|" + 
                                                                    r_4 + "|" + 
                                                                    r_5 + "|" + 
                                                                    r_6 + "|" + 
                                                                    r_7 + "|" + 
                                                                    r_8 + "|" + 
                                                                    r_9 + "|" + 
                                                                    r_10 + "|" + 
                                                                    r_11 + "|" + 
                                                                    r_12 + "|" + 
                                                                    r_13 + "|" + 
                                                                    r_14 + "|" + 
                                                                    r_15 + "|" + 
                                                                    r_16 + "|" + 
                                                                    r_17);
    public static String getPosTaggingSentence(List<String> sentence){
        List<String> tag_s = new ArrayList<String>();
        for(String word:sentence){
        	String[] tag = word.split("/");
        	tag_s.add(tag[1]);
        }
        return String.join(" ",tag_s);
    }
    public static boolean isOpinion(String r_sentence) {
        Matcher m = TOKENIZER_PATTERN.matcher(r_sentence);
        return m.find();
    }
    /**
     * Main Test
     */
    public static void main(String[] args) {
        Matcher m = TOKENIZER_PATTERN.matcher("n n CDP n n n n : cdp n n v n n n n n .");
        List<String> tokens = new ArrayList<>(); 
        while (m.find()) {
            tokens.add(m.group());
        }
        LOG.info(tokens.toString());
    }

}
