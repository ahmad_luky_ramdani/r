package com.naufal.senti.ngram;

import com.naufal.senti.commons.util.RegexUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

public class NGram {
    public static List<String> getUnigrams(String words){
        List<String> unigrams = new ArrayList<>();
        Matcher m = RegexUtils.TOKENIZER_PATTERN.matcher(words);
        while (m.find()) {
            unigrams.add(m.group());
        }
        return unigrams;
    }
    public static List<String[]> getBigrams(String[] words) {
        List<String[]> bigrams = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            if (i < words.length - 1) {
              bigrams.add(new String[] { words[i], words[i + 1] });
            }
        }
        return bigrams;
    }
    public static List<String[]> getTrigrams(String[] words) {
        List<String[]> trigrams = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            if (i < words.length - 2) {
              trigrams.add(new String[] { words[i], words[i + 1], words[i + 2] });
            }
        }
        return trigrams;
    }
    public static void main(String[] args) {
        String testStr = "Colorless green ideas sleep furiously 4ll";
        System.out.println("TestString: " + testStr);
        
        // Unigram
        List<String> unigram = getUnigrams(testStr);
        for (int i = 0; i < unigram.size(); i++) {
            System.out.println("Unigram : ["+i+"]: "
                    +unigram.get(i));
        }
        
        String[] words = testStr.split(" ");

        // Bigrams
        List<String[]> bigrams = getBigrams(words);
        for (int i = 0; i < bigrams.size(); i++) {
            System.out.println("bigram[" + i + "]: "
                + Arrays.toString(bigrams.get(i)));
        }

        // Trigrams
        List<String[]> trigrams = getTrigrams(words);
        for (int i = 0; i < trigrams.size(); i++) {
            System.out.println("trigrams[" + i + "]: "
                + Arrays.toString(trigrams.get(i)));
        }
    }

}
