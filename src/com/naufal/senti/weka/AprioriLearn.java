package com.naufal.senti.weka;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import weka.core.converters.ArffLoader.ArffReader;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
/**
 * 
 * @author ahmadluky
 * ===================
 * The required number of rules. (default = 10)
 * The metric type by which to rank rules. (default = confidence)
 * The minimum confidence of a rule. (default = 0.9)
 * The delta by which the minimum support is decreased in each iteration. (default = 0.05)
 * Upper bound for minimum support. (default = 1.0)
 * The lower bound for the minimum support. (default = 0.1)
 * If used, rules are tested for significance at the given level. Slower. (default = no significance testing)
 * If set the itemsets found are also output. (default = no)
 * Remove columns that contain all missing values (default = no)
 * Report progress iteratively. (default = no)
 * If set class association rules are mined. (default = no)
 * Treat zero (i.e. first value of nominal attributes) as missing
 * If used, two characters to use as rule delimiters in the result of toString: the first to delimit fields, the second to delimit items within fields.
 * (default = traditional toString result)
 * The class index. (default = last)
 */

public class AprioriLearn {
	
	Instances trainData;
	weka.associations.Apriori apriori;

	public void loadDataset(String fileName) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			ArffReader arff = new ArffReader(reader);
			trainData = arff.getData();
			System.out.println("===== Loaded dataset: " + fileName + " =====");
			reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("Problem found when reading: " + fileName);
		}
	}
	
	public void learn() {
		try {
			trainData.setClassIndex(trainData.numAttributes() - 1);
		    // build associator
		    apriori = new weka.associations.Apriori();
		    apriori.setClassIndex(trainData.classIndex());
		    apriori.buildAssociations(trainData);
		    // output associator
		    System.out.println(apriori);
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("Problem learn found when training");
		}
	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void makeInstance(String csvInstance) throws NumberFormatException, IOException {
		
		/**
		 * @RELATION data
		 * @ATTRIBUTE posscore	{p1,p2,p3}
		 * @ATTRIBUTE negscore 	{n1,n2,n3}
		 * @ATTRIBUTE objscore 	{obj1,obj2,obj3}
		 * @ATTRIBUTE aggregation	{positif,negatif,netral}
		 */
		
		// Atribute "positive"
		ArrayList  positive = new ArrayList(3); 
		positive.add("p1"); 
		positive.add("p2"); 
		positive.add("p3"); 
		Attribute posv = new Attribute("posScore", positive);
		
		// Atribute "negative"
		ArrayList  negative = new ArrayList(3); 
		negative.add("n1"); 
		negative.add("n2"); 
		negative.add("n3"); 
		Attribute negv = new Attribute("negScore", negative);
		
		// Atribute "objective"
		ArrayList  objective = new ArrayList(3); 
		objective.add("obj1"); 
		objective.add("obj2"); 
		objective.add("obj3"); 
		Attribute objv = new Attribute("objScore", objective);
		
		ArrayList  values = new ArrayList(3); 
		values.add("positive"); 
		values.add("negative"); 
		values.add("netral"); 
		Attribute aggv = new Attribute("polarity", values);
		

		ArrayList  attributeList = new ArrayList(4);
		attributeList.add(posv);
		attributeList.add(negv);
		attributeList.add(objv);
		attributeList.add(aggv);
		
		trainData = new Instances("dataTarining",attributeList, 0);
		trainData.setClassIndex(trainData.numAttributes()-1);
		
		DenseInstance instance = new DenseInstance(4);
		instance.setDataset(trainData);

		/**
		 * Read Csv / data testing
		 */
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(new FileReader(csvInstance));
		String line;
		while((line=reader.readLine()) != null){
			String[] stringValues = line.split(",");
			
			/**
			 * Low Positive [0.0 - 2.625 : P1]
			 * Positive [2.626 - 5.250 : P2]
			 * Strength Positive [5.251 - 7.875 : P2]
			 */
			if ( Double.parseDouble(stringValues[0])  >= 0.0 & 
				Double.parseDouble(stringValues[0]) < 2.625) {
				instance.setValue(0, "p1"); // positif
				System.out.print("p1,");
			} else if ( Double.parseDouble(stringValues[0])  >= 2.625 & 
				Double.parseDouble(stringValues[0]) <  5.250) {
				instance.setValue(0, "p2"); //"negatif"
				System.out.print("p2,");
			} else if ( Double.parseDouble(stringValues[0]) >=  5.250 & 
				Double.parseDouble(stringValues[0]) <= 7.875) {
				instance.setValue(0, "p3"); //"netral"
				System.out.print("p3,");
			}
			trainData.add(instance);
			
			/**
			 * Low Negative [0.0 - 2.708 : N1]
			 * Negative [2.709 - 5.417 : N2]
			 * Strength Negative [5.417 - 8.125: N3]
			 */
			if ( Double.parseDouble(stringValues[1])  >= 0.0 & 
				Double.parseDouble(stringValues[1]) < 2.708) {
				instance.setValue(1, "n1"); // positif
				System.out.print("n1,");
			} else if ( Double.parseDouble(stringValues[1])  >= 2.708 & 
				Double.parseDouble(stringValues[1]) <  5.417) {
				instance.setValue(1, "n2"); //"negatif"
				System.out.print("n2,");
			} else if ( Double.parseDouble(stringValues[1]) >=  5.417 & 
				Double.parseDouble(stringValues[1]) <= 8.125) {
				instance.setValue(1, "n3"); //"netral"
				System.out.print("n3,");
			}
			trainData.add(instance);
			
			/**
			 * Low Objective [0.0 - 40.375 : OBJ1]
			 * Objective [40.376 - 80,75 : OBJ2]
			 * Strength Objective [80,76 - 121.125: OBJ3]
			 */
			if ( Double.parseDouble(stringValues[2])  >= 0.0 & 
				Double.parseDouble(stringValues[2]) < 40.375) {
				instance.setValue(2, "obj1"); // positif
				System.out.print("obj1,");
			} else if ( Double.parseDouble(stringValues[2])  >= 40.376 & 
				Double.parseDouble(stringValues[2]) <  80.750) {
				instance.setValue(2, "obj2"); //"negatif"
				System.out.print("obj2,");
			} else if ( Double.parseDouble(stringValues[2]) >=  80.751 & 
				Double.parseDouble(stringValues[2]) <= 121.125) {
				instance.setValue(2, "obj3"); //"netral"
				System.out.print("ob3,");
			}
			trainData.add(instance);
			
			/**
			 * Low Objective [0.0 - 41.333 : positive]
			 * Objective [41.333 - 82.666: negative]
			 * Strength Objective [82,666 - 124: Objective]
			 *
			 *
			if ( Double.parseDouble(stringValues[3])  >= 0.0 & 
				Double.parseDouble(stringValues[3]) < 41.333333333333336) {
				instance.setValue(3, "pos"); // positif
				System.out.print(" pos");
			} else if ( Double.parseDouble(stringValues[3])  >= 41.333333333333336 & 
				Double.parseDouble(stringValues[3]) <  82.66666666666667) {
				instance.setValue(3, "neg"); //"negatif"
				System.out.print(" neg");
			} else if ( Double.parseDouble(stringValues[3]) >=  82.66666666666667 & 
				Double.parseDouble(stringValues[3]) <= 124.0) {
				instance.setValue(3, "obj"); //"netral"
				System.out.print(" obj");
			}
			*/
			
			// type nominal data
			System.out.print(""+stringValues[3]);
			instance.setValue(3, stringValues[3]);
			
			trainData.add(instance);
			System.out.print("\n");
		}
 		System.out.println("===== Instance created with reference dataset =====");
		System.out.println(trainData);
	}
	
	public void saveModel(String fileName) {
		try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(apriori);
            out.close();
 			System.out.println("===== Saved model: " + fileName + " =====");
        } 
		catch (IOException e) {
			System.out.println("Problem found when writing: " + fileName);
		}
	}
	
	
	public static void main (String[] args) throws NumberFormatException, IOException {
//		String csv = "resources/datasets/data/data-example-result.txt";
		String csv = "resources/datasets/data/data-example-result.arff";
		AprioriLearn learner;
		learner = new AprioriLearn();
		learner.loadDataset(csv);
//		learner.makeInstance(csv);
		learner.learn();
//		learner.saveModel("resources/models/data-result.dat");
	}
}
