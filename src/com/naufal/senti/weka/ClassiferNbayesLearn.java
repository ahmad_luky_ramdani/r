package com.naufal.senti.weka;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instances;
import weka.core.converters.DatabaseLoader;
import weka.core.converters.ArffLoader.ArffReader;
import weka.experiment.InstanceQuery;

public class ClassiferNbayesLearn {

    Instances trainData;
    NaiveBayes classifier;
    
    public void loadDataset(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            ArffReader arff = new ArffReader(reader);
            trainData = arff.getData();
            System.out.println("= Loaded dataset: " + fileName + " =");
        }
        catch (IOException e) {
            System.out.println("Problem found when reading: " + fileName);
        }
    }
    public void loadDataset() throws Exception {
		/**
    	 * cretae arff from connect db
    	 */
		InstanceQuery query;
		try {
			DatabaseLoader loader = new DatabaseLoader();
			loader.setSource("jdbc:mysql://localhost:3306/lexicon", "root", "");
			query = new InstanceQuery();
			query.setUsername("root");
			query.setPassword("");
			query.setQuery("select f1, f2, f3, f4, f5, f6, f7 from lexicon.data_ciri limit 10"); // read table
			trainData = query.retrieveInstances();
			trainData.setClassIndex(trainData.numAttributes()-1); // set the numberof classes (creates index)
			System.out.println(trainData.toString());
            System.out.println("Loaded dataset Train: From DB");
            System.exit(-1);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    public void evaluate() {
    	/**
    	 * evaluate model
    	 */
        try {
            trainData.setClassIndex(trainData.numAttributes()-1);
            classifier = new NaiveBayes();
            Evaluation eval = new Evaluation(trainData);
            // best k=8 for cross validation
            eval.crossValidateModel(classifier, trainData, 10, new Random(1));
            System.out.println(eval.toSummaryString());
            System.out.println(eval.toClassDetailsString());
            System.out.println("= Evaluating on filtered (training) dataset done =");
        }
        catch (Exception e) {
            System.out.println("Problem found when evaluating");
        }
    }
    public void learn() {
    	/**
    	 * training data
    	 */
    	try {
            trainData.setClassIndex(trainData.numAttributes()-1);
            classifier = new NaiveBayes();
            classifier.buildClassifier(trainData);
            System.out.println(classifier);
            System.out.println("= Training on filtered (training) dataset done =");
        }
        catch (Exception e) {
            System.out.println("Problem found when training");
        }
    }
    public void saveModel(String fileName) {
    	/**
    	 * save model
    	 */
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(classifier);
            out.close();
            System.out.println("= Saved model: " + fileName + " =");
        } 
        catch (IOException e) {
            System.out.println("Problem found when writing: " + fileName);
        }
    }
	public static void main (String[] args) throws Exception {
    	// Prepare Train tweets
        String fl = "./resources/datasets/2016/arff/Unigram-SE-SW.arff";
        ClassiferNbayesLearn learner;
        learner = new ClassiferNbayesLearn();
        learner.loadDataset(fl); //from file arff
        // learner.loadDataset(); //from database
        learner.evaluate();
        learner.learn();
        learner.saveModel("./resources/models/Unigram-SE-SW.dat");
    }
}
