package com.naufal.senti.weka;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.naufal.senti.commons.Configuration;
import com.naufal.senti.commons.Dataset;
import com.naufal.senti.commons.Tweet;
import com.naufal.senti.commons.database.Mysql;
import com.naufal.senti.commons.featurevector.CombinedFeatureVectorGeneratorBahasa;
import com.naufal.senti.components.POSTagHMM;
import com.naufal.senti.components.PreprocessorBahasa;
import com.naufal.senti.components.Tokenizer;
import com.naufal.senti.cs.ir.kbbi.IOException_Exception;
import com.naufal.senti.cs.ir.kbbi.MalformedURLException_Exception;
/**
 * @author DIDSI-IPB
 */

/**
 * Structur Tabel data ciri untuk digunakan pada proses pembelajaran algoritme WEKA
	 CREATE TABLE `data_ciri` (
	  `id` int(11) NOT NULL,
	  `f1` double DEFAULT NULL,
	  `f2` double DEFAULT NULL,
	  `f3` double DEFAULT NULL,
	  `f4` double DEFAULT NULL,
	  `f5` double DEFAULT NULL,
	  `f6` double DEFAULT NULL,
	  `f7` double DEFAULT NULL,
	  `other` varchar(45) DEFAULT NULL,
	  `class` varchar(45) DEFAULT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 */
public class FeatureVectorWeka {
	 public static void main(String[] args) throws IOException, IOException_Exception, MalformedURLException_Exception, InterruptedException, SQLException, ClassNotFoundException {
		 	Mysql conect = new Mysql(); // Mysql
		 	String dbFrom = "data_json"; //data training
		 	String dbTo = "data_ciri"; //data ciri training
	        PreprocessorBahasa preprocessor = PreprocessorBahasa.getInstance();// Use POS tags in terms
	        POSTagHMM pOSTagHMM = POSTagHMM.getInstance();
	        Dataset dataset = Configuration.getDataSet();
	        // Load tweets
	        List<Tweet> tweet = dataset.getTrainTweetsDB(dbFrom);
	        System.out.println("Read train tweets from DB "+dbFrom+"");
	        // Tokenize
	        List<List<String>> tokenizedTweets = Tokenizer.tokenizeTweets(tweet);
	        // Preprocess
	        List<List<String>> preprocessedTweets = preprocessor.preprocessTweets(tokenizedTweets);
	        // POS Tagging
	        List<List<String>> taggedTweets = pOSTagHMM.tagTweets(preprocessedTweets);
	        // Generate CombinedFeatureVectorGeneratorBahasa
	        CombinedFeatureVectorGeneratorBahasa cfvg = new CombinedFeatureVectorGeneratorBahasa();
	        // Combined Feature Vector Generation
	        for (int i = 0; i < taggedTweets.size(); i++) {
	            List<String> taggedTokens = taggedTweets.get(i);
	            Map<Integer, Double> combinedFeatureVector = cfvg.generateFeatureVector(taggedTokens);
	            // Generate feature vector string
	            String featureVectorStr = "";
	            String Sql1 = "Insert Into "+dbTo+" ("
			            		+ "`f1`,"
			            		+ "`f2`,"
								+ "`f3`,"
								+ "`f4`,"
								+ "`f5`,"
								+ "`f6`,"
								+ "`f7`,"
								+ "`id_str`,"
								+ "`kelas` ) values (";
	            List<Double> valueOnly = new ArrayList<Double>();
	            for (Map.Entry<Integer, Double> feature : combinedFeatureVector.entrySet()) {
	                featureVectorStr += " " + feature.getKey() + ":" + feature.getValue();    
	                valueOnly.add(feature.getValue());
                	if (feature.getValue().isNaN()) {
                		Sql1 += "null, ";
					} else {
	                	Sql1 += ""+feature.getValue()+", ";
					}
	            }
	            Sql1 += ""+tweet.get(i).getId()+", "+tweet.get(i).getScore()+" )";
	            // Save vektor ciri by score word
	            valueOnly.add(tweet.get(i).getScore());
	            System.out.println("Tweet: '" + taggedTokens + "'");
	            System.out.println("FeatureVector: " + featureVectorStr);
	            System.out.println("Class: "+tweet.get(i).getScore());
	            System.out.println("Id: "+tweet.get(i).getId());
	            if (!featureVectorStr.isEmpty()) {
		            System.out.println(Sql1);
		            conect.sqlInsert(Sql1);
				}
	        }
	  }
}
