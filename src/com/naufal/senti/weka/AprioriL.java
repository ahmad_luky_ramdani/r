package com.naufal.senti.weka;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import weka.associations.Apriori;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;

public class AprioriL {
	
	Instances instances;
	weka.associations.Apriori  apriori;
	
	public void loadModel(String fileName) {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
            Object tmp = in.readObject();
            apriori = (Apriori) tmp;
            in.close();
 			System.out.println("===== Loaded model: " + fileName + " =====");
       } 
		catch (Exception e) {
			System.out.println("Problem found when reading: " + fileName);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void makeInstance(String csvInstance) throws NumberFormatException, IOException {
		
		/**
		 * @RELATION iris
		 * @ATTRIBUTE sepallength	REAL
		 * @ATTRIBUTE sepalwidth 	REAL
		 * @ATTRIBUTE petallength 	REAL
		 * @ATTRIBUTE petalwidth	REAL
		 * @ATTRIBUTE class 		{Iris-setosa,Iris-versicolor,Iris-virginica}
		 */
		
		// Create the header Arff
		@SuppressWarnings("rawtypes")
		ArrayList  attributeList = new ArrayList(5);
		
		// Atribute "outlook" - default numeric
		Attribute attribute = new Attribute("outlook");
		@SuppressWarnings("rawtypes")
		ArrayList  outlook = new ArrayList(3); 
		outlook.add("sunny"); 
		outlook.add("overcast"); 
		outlook.add("rainy"); 
		attribute = new Attribute("outlook", outlook);
		attributeList.add(attribute);
		
		// Atribute "temperature" - default real
		attribute = new Attribute("temperature");
		attributeList.add(attribute);
		
		// Atribute "humidity" - default real
		attribute = new Attribute("humidity");
		attributeList.add(attribute);
		
		// Atribute "windy" - default numeric
		@SuppressWarnings("rawtypes")
		ArrayList  windy = new ArrayList(3); 
		windy.add("TRUE"); 
		windy.add("FALSE"); 
		attribute = new Attribute("windy", windy);
		attributeList.add(attribute);
		
		// Atribute "play"
		@SuppressWarnings("rawtypes")
		ArrayList  play = new ArrayList(3); 
		play.add("yes"); 
		play.add("no"); 
		attribute = new Attribute("play", play);
		attributeList.add(attribute);
		
		instances = new Instances("Test clasification", (java.util.ArrayList<Attribute>) attributeList, 1);
		instances.setClassIndex(instances.numAttributes()-1);
		
		DenseInstance instance = new DenseInstance(5);
		instance.setDataset(instances);
		
		/**
		 * Read Csv / data testing model
		 */
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(new FileReader(csvInstance));
		String line;
		while((line=reader.readLine()) != null){
			String[] stringValues = line.split(",");
			instance.setValue(0, Double.parseDouble(stringValues[0]));
			instance.setValue(1, Double.parseDouble(stringValues[1]));
			instance.setValue(2, Double.parseDouble(stringValues[2]));
			instance.setValue(3, Double.parseDouble(stringValues[3]));		
			instances.add(instance);
		}
 		System.out.println("===== Instance created with reference dataset =====");
		System.out.println(instances);
	}
	
	/**
	 * Main method
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	public static void main (String[] args) throws NumberFormatException, IOException {
		AprioriL classifier;
		String datT 	= "resources/models/weather.nominal_testing.csv";
		String modl 	= "resources/models/model-weather.nominal.dat";
		classifier		= new AprioriL();
		classifier.loadModel(modl);
		classifier.makeInstance(datT);
	}
}
