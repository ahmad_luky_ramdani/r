package com.naufal.senti.weka;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.naufal.senti.commons.database.Mysql;

import weka.classifiers.bayes.NaiveBayes;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;

public class ClassiferNbayes {

    Instances instances;
    Instances testData;
    NaiveBayes classifier;
    
    public void loadModel(String fileName) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
            Object tmp = in.readObject();
            classifier = (NaiveBayes) tmp;
        } catch (Exception e) {
            System.out.println("Problem found when reading: " + fileName);
        }
        System.out.println("===== Loaded model: " + fileName + "");
    }
    public void loadTestset(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            ArffReader arff = new ArffReader(reader);
            testData = arff.getData();
            testData.setClassIndex(testData.numAttributes() - 1);
            System.out.println("===== Loaded dataset Testing: " + fileName + "");
        } catch (IOException e) {
            System.out.println("Problem found when reading: " + fileName);
        }
    }
    @SuppressWarnings("unchecked")
    public void makeInstance(String csvInstance) throws NumberFormatException, IOException {
        /**
         * @RELATION iris
         * @ATTRIBUTE sepallength	REAL
         * @ATTRIBUTE sepalwidth 	REAL
         * @ATTRIBUTE petallength 	REAL
         * @ATTRIBUTE petalwidth	REAL
         * @ATTRIBUTE class 		{Iris-setosa,Iris-versicolor,Iris-virginica}
         */
        // Create the header Arff
        @SuppressWarnings("rawtypes")
        ArrayList  attributeList = new ArrayList(5);
        // Atribute "sepallength" - default numeric
        Attribute attribute = new Attribute("sepallength");
        attributeList.add(attribute);
        // Atribute "sepalwidth" - default numeric
        attribute = new Attribute("sepalwidth");
        attributeList.add(attribute);
        // Atribute "petallength" - default numeric
        attribute = new Attribute("petallength");
        attributeList.add(attribute);
        // Atribute "petalwidth" - default numeric
        attribute = new Attribute("petalwidth");
        attributeList.add(attribute);
        // Atribute "class"
        @SuppressWarnings("rawtypes")
        ArrayList  values = new ArrayList(3); 
        values.add("Iris-setosa"); 
        values.add("Iris-versicolor"); 
        values.add("Iris-virginica"); 
        attribute = new Attribute("class", values);
        attributeList.add(attribute);
        instances = new Instances("Test clasification", (java.util.ArrayList<Attribute>) attributeList, 1);
        instances.setClassIndex(instances.numAttributes()-1);
        DenseInstance instance = new DenseInstance(5);
        instance.setDataset(instances);
        /**
         * Read Csv / data testing model
         */
        @SuppressWarnings("resource")
        BufferedReader reader = new BufferedReader(new FileReader(csvInstance));
        String line;
        while((line=reader.readLine()) != null){
                String[] stringValues = line.split(",");
                instance.setValue(0, Double.parseDouble(stringValues[0]));
                instance.setValue(1, Double.parseDouble(stringValues[1]));
                instance.setValue(2, Double.parseDouble(stringValues[2]));
                instance.setValue(3, Double.parseDouble(stringValues[3]));		
                instances.add(instance);
        }
        System.out.println("===== Instance created with reference dataset =====");
        System.out.println(instances);
    }
    
	@SuppressWarnings({ "unchecked", "static-access" })
	private void makeInstance() throws SQLException, ClassNotFoundException {
		/**
        @RELATION train from db
        @ATTRIBUTE attribute_0 REAL
		@ATTRIBUTE attribute_1 REAL
		@ATTRIBUTE attribute_2 REAL
		@ATTRIBUTE attribute_3 REAL
		@ATTRIBUTE attribute_4 REAL
		@ATTRIBUTE attribute_5 REAL
		@ATTRIBUTE attribute_6 REAL
		@ATTRIBUTE attribute_7 {0.0,1.0,2.0}
         */
        // Create the header Arff
        @SuppressWarnings("rawtypes")
        ArrayList  attributeList = new ArrayList(8);
        Attribute attribute = new Attribute("attribute_0");
        attributeList.add(attribute);
        attribute = new Attribute("attribute_1");
        attributeList.add(attribute);
        attribute = new Attribute("attribute_2");
        attributeList.add(attribute);
        attribute = new Attribute("attribute_3");
        attributeList.add(attribute);
        attribute = new Attribute("attribute_4");
        attributeList.add(attribute);
        attribute = new Attribute("attribute_5");
        attributeList.add(attribute);
        attribute = new Attribute("attribute_6");
        attributeList.add(attribute);
        // Atribute "class"
        @SuppressWarnings("rawtypes")
        ArrayList  values = new ArrayList(3); 
        values.add("0.0"); 
        values.add("1.0"); 
        values.add("2.0"); 
        attribute = new Attribute("class", values);
        attributeList.add(attribute);
        instances = new Instances("Test clasification", (java.util.ArrayList<Attribute>) attributeList, 1);
        instances.setClassIndex(instances.numAttributes()-1);
        DenseInstance instance = new DenseInstance(8);
        instance.setDataset(instances);
        /**
         * Read database testing model
         */
        Mysql conect = new Mysql();
        String str_sql = "SELECT f1, f2, f3, f4, f5, f6, f7 FROM data_ciri";
		ResultSet result = conect.sql(str_sql);
        while(result.next()){
        	String f1 = result.getString("f1");
        	if (result.wasNull()){
	            instance.setValue(0, Double.NaN);
        	} else {
        		instance.setValue(0, Double.parseDouble(f1));
        	}
            String f2 = result.getString("f2");
            if (result.wasNull()){
	            instance.setValue(1, Double.NaN);
        	} else {
        		instance.setValue(1, Double.parseDouble(f2));
        	};
            String f3 = result.getString("f3");
            if (result.wasNull()){
	            instance.setValue(2, Double.NaN);
        	} else {
        		instance.setValue(2, Double.parseDouble(f3));
        	}
            String f4 = result.getString("f4");
            if (result.wasNull()){
	            instance.setValue(3, Double.NaN);
        	} else {
        		instance.setValue(3, Double.parseDouble(f4));
        	}
            String f5 = result.getString("f5");
            if (result.wasNull()){
	            instance.setValue(4, Double.NaN);
        	} else {
        		instance.setValue(4, Double.parseDouble(f5));
        	}
            String f6 = result.getString("f6");
            if (result.wasNull()){
	            instance.setValue(5, Double.NaN);
        	} else {
        		instance.setValue(5, Double.parseDouble(f6));
        	}
            String f7 = result.getString("f7");
            if (result.wasNull()){
	            instance.setValue(6, Double.NaN);
        	} else {
        		instance.setValue(6, Double.parseDouble(f7));
        	}	
            instances.add(instance);
        }
        System.out.println("===== Instance created with reference dataset =====");
        System.out.println(instances);
	}
    public void classify(Integer count) {
        try {
            Mysql conect = new Mysql();
        	System.out.println("===== Classified instance");
            for(int i = 1; i<=count;i++){
                double pred = classifier.classifyInstance(instances.instance(i));
                System.out.println("Class predicted: " + instances.classAttribute().value((int) pred));
                String str_sql = "Update data_ciri set kelas="+pred+" where id="+ i +"";
        		conect.sqlUpdate(str_sql);
        		System.out.println(i);
            }
        }catch (Exception e) {
            System.out.println("Problem found when classifying the example");
        }		
    }
    /**
     * Main method
    * @param args
     * @throws IOException 
     * @throws NumberFormatException 
     * @throws SQLException 
     * @throws ClassNotFoundException 
     */
    public static void main (String[] args) throws NumberFormatException, IOException, ClassNotFoundException, SQLException {
    	ClassiferNbayes classifier;
        // String datT 	= "./resources/datasets/datatesting/iris.arff";
        String modl 	= "./resources/models/Unigram-SE-SW.dat";
        int count_datT 	= 4686;
        classifier = new ClassiferNbayes();
        classifier.loadModel(modl);
        // classifier.makeInstance(datT); // load from CSV file
        classifier.makeInstance(); // load from DB
        //classifier.loadTestset(datT); //load from arff file
        classifier.classify(count_datT);

    }
}
